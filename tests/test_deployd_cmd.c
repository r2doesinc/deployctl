/*
 test_deployd_cmd.c
 Created by Danny Goossen, Gioxa Ltd on 26/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

#include <check.h>

#include "../src/deployd.h"

//#include "utils.h"


// TODO add to make file when ready WIP

// struct to set return value of stub functions
typedef struct test_stubs_s
{
   int url_check;
   int verify_http_config;
   int reload_http_config;
   int letsencrypt;
   int delete_http_config;
   int write_http_config;
   int check_namespace;
   char * make_html_release;

   cJSON * read_relase_json;
   cJSON * create_thisrelease_json;
	char * read_release_json_filepath;
    char * sub_path;
	char * base_path;
	char *tagPATH;
	char *tagHREF;
	char * check_namespace_filepath;
   int write_namespace;
   int validate_project_path_slug;

   char * create_releases_json;
   char * create_release_json_js;
  cJSON * get_release;
   int is_clean_tag;
   char * check_last_tag;
   } test_stubs_t;


// stubfunctions:
int url_check(void * opaque, char * basePATH,char * baseHREF)
{
   return(( (test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->url_check);
}
int verify_http_config(void * opaque)
{
   return(( (test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->verify_http_config);
}

int reload_http_config(void * opaque)
{
   return(( (test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->reload_http_config);
}

int letsencrypt(void * opaque, char * domain,char * email)
{
   return(  ((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->letsencrypt);
}

int delete_http_config(void * opaque,char * basePATH)
{
		ck_assert_str_eq(basePATH, ((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->base_path);
   return( ((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->delete_http_config);
}

int write_http_config(void * opaque, int is_https,char * basePATH, char * server_name)
{
   ck_assert_str_eq(basePATH, ((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->base_path);
   return( ((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->write_http_config);
}

int check_namespace(void * opaque,char * filepath)
{
   ck_assert_str_eq(filepath, ((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->base_path);
   return( ((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->check_namespace);
}
int write_namespace(void * opaque, char * filepath )
{
   ck_assert_str_eq(filepath, ((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->base_path);
   return( ((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->write_namespace);
}

int validate_project_path_slug(cJSON* env_vars, struct trace_Struct *trace, int suggest)
{
   return( ((test_stubs_t *)(trace->tests))->validate_project_path_slug );
}
/* no longer used used
char * make_html_release(void * opaque, cJSON * release_tag, const char * filesPATH,  const char * filesHREF, int deploy_production )
{
	ck_assert_str_eq(filesPATH, ((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->tagPATH);
	ck_assert_str_eq(filesHREF, ((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->tagHREF);
   return( strdup(((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->make_html_release));
}
 */
char * create_releases_json( void * opaque , const cJSON * this_release, cJSON ** releases, int production)
{
   *releases=cJSON_Parse(((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->create_releases_json);
   return( strdup(((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->create_releases_json));
}

cJSON * create_thisrelease_json( void * opaque , cJSON * release_info, int production, const char * base_path,const char * sub_path)
{
   ck_assert_str_eq(sub_path,((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->sub_path);
   ck_assert_str_eq(base_path, ((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->base_path);
   return( cJSON_Duplicate( ((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->create_thisrelease_json, 1));
}

char * create_release_json_js (void * opaque, char * download_url, char * releases_json, int tag, char * last_tag)
{
	return( strdup(((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->create_release_json_js));
}


cJSON * read_release_json(void * opaque, const char * dirpath)
{
	ck_assert_str_eq(dirpath,((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->read_release_json_filepath);
   return( cJSON_Duplicate(((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->read_relase_json,1));
}

int get_release(void * opaque, cJSON ** release)
{
      *release=cJSON_Duplicate(((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->get_release,1);
    if (*release) return(0);
      else  return (1);
}


char * check_last_tag(void * opaque,const char * basePATH)
{
   ck_assert_str_eq(basePATH, ((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->base_path);
   if (((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->check_last_tag)
	return( strdup(((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->check_last_tag));
   else return NULL;
}

int is_clean_tag(void * opaque,const char * tag, const char ** list)
{
	return( ((test_stubs_t *)((data_exchange_t *)opaque)->trace->tests)->is_clean_tag );
}

char* read_file( const char * dirpath, const char * filename)
{
 return NULL;
}
int url_repo_base_path  (char ** basepath,char * baseHREF,struct trace_Struct * trace)
{
return 0;
}

// repos.c
cJSON * create_repo_dirs(void * opaque, char * repo_PATH , cJSON* repos)
{
return NULL;
}
int init_repo_dirs(void * opaque, char * basepath, cJSON* endpoints)
{
return 0;
}

int update_repo_dirs(void * opaque, char * basepath, cJSON* endpoints)
{
return 0;
}

int  add_deploy_info_json(void * opaque,cJSON * project_json)
{
   return 0;
}
char * create_repo_json_js (void * opaque, char * download_url, char * repo_json)
{
   return NULL;
}

int get_repo_list(void * opaque, const char *projectpath, cJSON** repo_info)
{
   return 0;
}
int match_end_points(void * opaque, cJSON* repo_info, cJSON* end_points, cJSON ** matches, cJSON ** skips, cJSON** symlinks)
{
   return 0;
}

cJSON * read_json(void * opaque, const char * base_path,const char * sub_path,const char * filename)
{
   return NULL;
}

// yalm2cjson.c
cJSON * yaml_sting_2_cJSON (void * opaque ,const char * yaml)
{
   return NULL;
}

void repo_print_skips(void * opaque,cJSON * skips)
{
   return;
}
void repo_copy_matches(void * opaque,cJSON * matches,char* basepath)
{
   return;
}

void repo_create_symlinks(void * opaque,cJSON * symlinks,char* basepath)
{
   return;
}

int copy_script_sh(void * opaque, const char * source, const char * destination, char * repo_name, char * repo_url, int PGP_repo, int PGP_pack)
{
   return 0;
}
//
char * substitute_repo_script(char * in, char * repo_name, char * repo_url, int PGP_repo, int PGP_pack)
{
 return in;
}

char * html_body_release(char * buf_in)
{
	return buf_in;
}

//helper function for tests
int write_test_file(const char * base,const char * sub,const char * file, const char * data)
{
   //LCOV_EXCL_START
   char filename[1024];
   sprintf((char *)filename,"%s%s/%s",base,sub,file);
   debug("write test file %s\n",filename);
   FILE *f = fopen(filename, "w");
   if (f == NULL){ debug("Error opening file : %s\n",strerror(errno));return -1;}
   /* print some text */
   int retvalue=0;
   int errvalue=0;
   do{ retvalue=fprintf(f, "%s",data);} while ( retvalue==-1 && (errvalue=errno)== EINTR );
   if (retvalue<0) { debug("Error io-command %s: %s\n", filename,strerror(errvalue)); return -1; }
   if (fclose(f) <0 ) { debug("Error io-command %s: %s\n", filename,strerror(errno));return -1;}
   return retvalue;
   //LCOV_EXCL_STOP
}



int  update_details(void * userp)
{
   return 0;
}


char * user;
char * group;

uid_t thisuid;
gid_t thisgid;

static void setup(void)
{
   struct passwd *pw;
   struct group * grp;

   thisuid = geteuid();
   thisgid= getgid();
   grp = getgrgid(thisgid);
   pw = getpwuid (thisuid);
   user=pw->pw_name;
   group=grp->gr_name;
}



START_TEST(check_delete_domain)
{
   parameter_t parameters;
   data_exchange_t data_exchange;
   void * opaque=&data_exchange;
   char * newarg[4];
   int exitcode=0;
   struct trace_Struct * trace=NULL;
   init_dynamic_trace( &trace,"token","url",1);
   data_exchange.trace=trace;
   data_exchange.needenvp=0;
   data_exchange.gid=getgid();
   data_exchange.uid=geteuid() ;

   cJSON * env_json=  cJSON_CreateObject();
   ck_assert_ptr_ne(env_json,NULL);
   data_exchange.env_json=env_json;

   data_exchange.paramlist=(char **)newarg;
   data_exchange.timeout=1;
   data_exchange.parameters=&parameters;
   test_stubs_t test_stubs;
   trace->tests=&test_stubs;
   // set results for tests

   test_stubs.check_namespace=0;
   test_stubs.validate_project_path_slug=0;
   char base_PATH[1024];
   test_stubs.base_path=base_PATH;


   char cwd[1024];
   ck_assert_ptr_ne(getcwd(cwd, sizeof(cwd)),NULL);
   char prefix[1024];
   sprintf(prefix,"%s/cmd_delete",cwd);
   parameters.prefix=prefix;

   char this_command[256];
   data_exchange.this_command=this_command;
    char temp[1024];
   sprintf(temp,"%s/projectdir",cwd);

    cJSON_AddItemToObject(env_json,"CI_ENVIRONMENT_NAME" , cJSON_CreateString("production" ));

    cJSON_AddItemToObject(env_json,"CI_PROJECT_PATH_SLUG", cJSON_CreateString("testpath-test-project" ));
    cJSON_AddItemToObject(env_json,"CI_PROJECT_PATH"     , cJSON_CreateString("testpath/test-project" ));
   cJSON_AddItemToObject(env_json,"CI_PROJECT_DIR"      , cJSON_CreateString(temp ));
   cJSON_AddItemToObject(env_json,"CI_PROJECT_URL"      , cJSON_CreateString("https://gitlab.example.com/testpath/test-project"));
   cJSON_AddItemToObject(env_json,"CI_COMMIT_SHA"       , cJSON_CreateString("01234567890"));
   cJSON_AddItemToObject(env_json,"DEPLOY_DOMAIN"       , cJSON_CreateString("www.example.com"));
   cJSON_AddItemToObject(env_json,"DEPLOY_DOMAIN_APP"   , cJSON_CreateString("deploy.example.com"));


   clear_dynamic_trace(trace);

   sprintf(base_PATH, "%s/deploy/domain/%s",prefix,cJSON_get_key(env_json, "DEPLOY_DOMAIN"));

   _mkdir(base_PATH, "/public");

   // should fail, missing environment var

   exitcode= cmd_delete(opaque);
   ck_assert_int_ne(exitcode,0);
   clear_dynamic_trace(trace);

   //add missing env
   cJSON_AddItemToObject(env_json,"CI_ENVIRONMENT_SLUG" , cJSON_CreateString("production" ));

   // should be ok!!
   exitcode= cmd_delete(opaque);
   ck_assert_int_eq(exitcode,0);
   clear_dynamic_trace(trace);

// simulate name space taken
   test_stubs.check_namespace=1;
   test_stubs.validate_project_path_slug=0;
   exitcode= cmd_delete(opaque);
   clear_dynamic_trace(trace);
   ck_assert_int_ne(exitcode,0);

   // simulate wrong projectpath slug, should be ok, in production
   test_stubs.check_namespace=0;
   test_stubs.validate_project_path_slug=1;
   exitcode= cmd_delete(opaque);
   clear_dynamic_trace(trace);
   ck_assert_int_eq(exitcode,0);
   exitcode=0;

   sprintf((char *)base_PATH,"%s/deploy/domain/%s.%s.%s", \
         parameters.prefix, \
         cJSON_get_key(env_json, "CI_ENVIRONMENT_SLUG"), \
         cJSON_get_key(env_json, "CI_PROJECT_PATH_SLUG"), \
         cJSON_get_key(env_json, "DEPLOY_DOMAIN_APP")     );

    _mkdir(base_PATH, "/public");
   cJSON_DeleteItemFromObject(env_json, "DEPLOY_DOMAIN");

// production, without deploy domain
   test_stubs.check_namespace=0;
   test_stubs.validate_project_path_slug=0;

   exitcode= cmd_delete(opaque);
   clear_dynamic_trace(trace);
   ck_assert_int_eq(exitcode,0);
   exitcode=0;
   // need to delete again, cmd_delete will produce deploy_domain!!!!
   cJSON_DeleteItemFromObject(env_json, "DEPLOY_DOMAIN");

// Production with toooooo long domain name, NOK
   char toolong[256];
   memset(toolong, 'a', 255);
   cJSON_ReplaceItemInObject(env_json,"CI_ENVIRONMENT_SLUG" , cJSON_CreateString(toolong ));

   exitcode= cmd_delete(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_ne(exitcode,0);
   exitcode=0;

   cJSON_ReplaceItemInObject(env_json,"CI_ENVIRONMENT_SLUG" , cJSON_CreateString("production" ));
   cJSON_DeleteItemFromObject(env_json, "DEPLOY_DOMAIN");

// production, without deploy domain and bad project_path_slug => NOK
   test_stubs.check_namespace=0;
   test_stubs.validate_project_path_slug=1;
   exitcode= cmd_delete(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_ne(exitcode,0);
   exitcode=0;
   cJSON_DeleteItemFromObject(env_json, "DEPLOY_DOMAIN");
      // non production, OK

   cJSON_ReplaceItemInObject(env_json,"CI_ENVIRONMENT_NAME" , cJSON_CreateString("review/master" ));
   cJSON_ReplaceItemInObject(env_json,"CI_ENVIRONMENT_SLUG" , cJSON_CreateString("review-master0989" ));


// make sites dir
   sprintf((char *)base_PATH,"%s/deploy/sites/%s/%s", \
			 parameters.prefix, \
             cJSON_get_key(env_json, "CI_PROJECT_PATH_SLUG") , \
             cJSON_get_key(env_json, "CI_ENVIRONMENT_SLUG") );
    _mkdir(base_PATH, "/public");

   test_stubs.check_namespace=0;
   test_stubs.validate_project_path_slug=0;
   exitcode= cmd_delete(opaque);
   debug("\nDelete...\n%s\n",get_dynamic_trace(trace));
    clear_dynamic_trace(trace);
   ck_assert_int_eq(exitcode,0);
   exitcode=0;
   cJSON_DeleteItemFromObject(env_json, "DEPLOY_DOMAIN");

// non production with missing Deploy domain app should be NOK
   cJSON_DeleteItemFromObject(env_json, "DEPLOY_DOMAIN_APP");

   test_stubs.check_namespace=0;
   test_stubs.validate_project_path_slug=0;
   exitcode= cmd_delete(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_ne(exitcode,0);
   exitcode=0;
   cJSON_DeleteItemFromObject(env_json, "DEPLOY_DOMAIN");

   cJSON_AddItemToObject(env_json,"DEPLOY_DOMAIN_APP"   , cJSON_CreateString("deploy.example.com"));
// non production with invalid project slug should be NOK
   test_stubs.check_namespace=0;
   test_stubs.validate_project_path_slug=1;
   exitcode= cmd_delete(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_ne(exitcode,0);
   exitcode=0;
// cleanup
   cJSON_Delete(env_json);
   _rmdir(prefix,"");
}
END_TEST

//int cmd_static(void * opaque)

START_TEST(check_cmd_static){
   parameter_t parameters;
   data_exchange_t data_exchange;
   void * opaque=&data_exchange;

   int exitcode=0;
   char * newarg[11];

   struct trace_Struct * trace=NULL;
   init_dynamic_trace( &trace,"token","url",1);
   data_exchange.trace=trace;
   data_exchange.needenvp=0;
   data_exchange.gid=getgid();
   data_exchange.uid=geteuid() ;

   cJSON * env_json=  cJSON_CreateObject();
   ck_assert_ptr_ne(env_json,NULL);
   data_exchange.env_json=env_json;

   data_exchange.paramlist=(char **)newarg;
   data_exchange.timeout=1;
   data_exchange.parameters=&parameters;
   test_stubs_t test_stubs;
   trace->tests=&test_stubs;
   // set results for tests
	char base_PATH[1024];
	test_stubs.base_path=base_PATH;

   test_stubs.check_namespace=0;
   test_stubs.validate_project_path_slug=0;
   test_stubs.url_check=0;
   test_stubs.write_http_config=0;
   test_stubs.verify_http_config=0;
   test_stubs.delete_http_config=0;
   test_stubs.reload_http_config=0;
   test_stubs.write_namespace=0;

   char cwd[1024];
   ck_assert_ptr_ne(getcwd(cwd, sizeof(cwd)),NULL);
   char prefix[1024];
   sprintf(prefix,"%s/cmd_static",cwd);
   parameters.prefix=prefix;

   char this_command[256];
   data_exchange.this_command=this_command;
   char projectdir[1024];


   sprintf(projectdir,"%s/projectdir_static",cwd);

    _mkdir( "/public",projectdir);

   //setdebug();

   cJSON_AddItemToObject(env_json,"CI_ENVIRONMENT_NAME" , cJSON_CreateString("production" ));

   cJSON_AddItemToObject(env_json,"CI_PROJECT_PATH_SLUG", cJSON_CreateString("testpath-test-project" ));
   cJSON_AddItemToObject(env_json,"CI_PROJECT_PATH"     , cJSON_CreateString("testpath/test-project" ));
   cJSON_AddItemToObject(env_json,"CI_PROJECT_DIR"      , cJSON_CreateString(projectdir ));
   cJSON_AddItemToObject(env_json,"CI_PROJECT_URL"      , cJSON_CreateString("https://gitlab.example.com/testpath/test-project"));
   cJSON_AddItemToObject(env_json,"CI_COMMIT_SHA"       , cJSON_CreateString("01234567890"));
   cJSON_AddItemToObject(env_json,"DEPLOY_DOMAIN"       , cJSON_CreateString("www.example.com"));
   cJSON_AddItemToObject(env_json,"DEPLOY_DOMAIN_APP"   , cJSON_CreateString("deploy.example.com"));
   cJSON * pubarray=cJSON_CreateArray();
   cJSON * path_obj=cJSON_CreateObject();
   cJSON_AddStringToObject(path_obj, "/","/public" );
   cJSON_AddItemToArray(pubarray, path_obj);
   cJSON_AddItemToObject(env_json,"DEPLOY_PUBLISH_PATH", pubarray);

    clear_dynamic_trace(trace);
   exitcode=0;

   sprintf(base_PATH, "%s/deploy/domain/www.example.com",prefix);

   // should fail, missing environment var

   exitcode= cmd_static(opaque);
   ck_assert_int_ne(exitcode,0);
    clear_dynamic_trace(trace);
   exitcode=0;

   //add missing env
   cJSON_AddItemToObject(env_json,"CI_ENVIRONMENT_SLUG" , cJSON_CreateString("production" ));

   // should be ok!!
   exitcode= cmd_static(opaque);
   fprintf(stderr,"%s\n",get_dynamic_trace(trace));
   ck_assert_int_eq(exitcode,0);

   // set DEPLOY path

   clear_dynamic_trace(trace);
   exitcode=0;

   // and cleanup
   _rmdir(prefix,"");


   // simulate name space taken
   test_stubs.check_namespace=1;

   exitcode= cmd_static(opaque);
    clear_dynamic_trace(trace);
   test_stubs.check_namespace=0;
   ck_assert_int_ne(exitcode,0);

   // production with invalid deploy_domain dns should be NOK
   test_stubs.url_check=1;
   exitcode= cmd_static(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_ne(exitcode,0);
   exitcode=0;
   test_stubs.url_check=0;

   // and cleanup
   _rmdir(prefix,"");

   // production problem writing config
   test_stubs.write_http_config=1;
   exitcode= cmd_static(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_ne(exitcode,0);
   exitcode=0;
   test_stubs.write_http_config=0;

   // and cleanup
    _rmdir(prefix,"");

   // production problem verify config
   test_stubs.verify_http_config=1;
   exitcode= cmd_static(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_ne(exitcode,0);
   exitcode=0;
   test_stubs.verify_http_config=0;

   // and cleanup
    _rmdir(prefix,"");

   // simulate wrong projectpath slug, should be ok, in production w deploy_domain
   test_stubs.validate_project_path_slug=1;
   exitcode= cmd_static(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_eq(exitcode,0);
   exitcode=0;
   test_stubs.validate_project_path_slug=0;

   // and cleanup
    _rmdir(prefix,"");
   exitcode= cmd_delete(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_eq(exitcode,0);
   exitcode=0;


   cJSON_DeleteItemFromObject(env_json, "DEPLOY_DOMAIN");


// ********************************************
// static: production, without deploy domain  *
// ********************************************
   //OK
   sprintf((char *)base_PATH,"%s/deploy/domain/%s.%s.%s", \
			parameters.prefix, \
			cJSON_get_key(env_json, "CI_ENVIRONMENT_SLUG"), \
			cJSON_get_key(env_json, "CI_PROJECT_PATH_SLUG"), \
			cJSON_get_key(env_json, "DEPLOY_DOMAIN_APP")     );

   exitcode= cmd_static(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_eq(exitcode,0);
   exitcode=0;
   cJSON_DeleteItemFromObject(env_json, "DEPLOY_DOMAIN");

   // and cleanup
   _rmdir(prefix,"");

   cJSON_DeleteItemFromObject(env_json, "DEPLOY_DOMAIN");

   // Production with toooooo long domain name, NOK
   char toolong[256];
   memset(toolong, 'a', 255);
   cJSON_ReplaceItemInObject(env_json,"CI_ENVIRONMENT_SLUG" , cJSON_CreateString(toolong ));

   exitcode= cmd_static(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_ne(exitcode,0);
   exitcode=0;

   cJSON_ReplaceItemInObject(env_json,"CI_ENVIRONMENT_SLUG" , cJSON_CreateString("production" ));
   cJSON_DeleteItemFromObject(env_json, "DEPLOY_DOMAIN");

// production, without deploy domain and bad project_path_slug => NOK
   test_stubs.validate_project_path_slug=1;

   exitcode= cmd_static(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_ne(exitcode,0);
   exitcode=0;
   test_stubs.validate_project_path_slug=0;
   cJSON_DeleteItemFromObject(env_json, "DEPLOY_DOMAIN");

// production  without deploy domain and with invalid dns should be NOK
   test_stubs.url_check=1;
   exitcode= cmd_static(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_ne(exitcode,0);
   exitcode=0;
   test_stubs.url_check=0;
   cJSON_DeleteItemFromObject(env_json, "DEPLOY_DOMAIN");

// **************************
//  static: non production  *
// **************************
   cJSON_ReplaceItemInObject(env_json,"CI_ENVIRONMENT_NAME" , cJSON_CreateString("review/master" ));
   cJSON_ReplaceItemInObject(env_json,"CI_ENVIRONMENT_SLUG" , cJSON_CreateString("review-master0989" ));
	// make sites dir
	sprintf((char *)base_PATH,"%s/deploy/sites/%s/%s", \
			parameters.prefix, \
			cJSON_get_key(env_json, "CI_PROJECT_PATH_SLUG") , \
			cJSON_get_key(env_json, "CI_ENVIRONMENT_SLUG") );

    //ok
   exitcode= cmd_static(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_eq(exitcode,0);
   exitcode=0;

   //cleanup
   exitcode= cmd_delete(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_eq(exitcode,0);
   exitcode=0;


   // simulate name space taken
   test_stubs.check_namespace=1;

   exitcode= cmd_static(opaque);
    clear_dynamic_trace(trace);
   test_stubs.check_namespace=0;
   ck_assert_int_ne(exitcode,0);


   // non production with invalid dns should be NOK
   test_stubs.url_check=1;
   exitcode= cmd_static(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_ne(exitcode,0);
   exitcode=0;
   test_stubs.url_check=0;

   // and cleanup
   exitcode= cmd_delete(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_eq(exitcode,0);
   exitcode=0;

   // non production with missing Deploy domain app should be NOK
   cJSON_DeleteItemFromObject(env_json, "DEPLOY_DOMAIN_APP");

   exitcode= cmd_static(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_ne(exitcode,0);
   exitcode=0;
   cJSON_DeleteItemFromObject(env_json, "DEPLOY_DOMAIN");

   cJSON_AddItemToObject(env_json,"DEPLOY_DOMAIN_APP"   , cJSON_CreateString("deploy.example.com"));

   // non production with invalid project slug should be NOK
   test_stubs.validate_project_path_slug=1;
   exitcode= cmd_static(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_ne(exitcode,0);
   exitcode=0;
   test_stubs.validate_project_path_slug=0;

   // cleanup
   _rmdir(projectdir,"");
   _rmdir(prefix,"");
   cJSON_Delete(env_json);
   free_dynamic_trace(&trace);

}
END_TEST

//int cmd_release(void * opaque)
START_TEST(check_cmd_release){
   parameter_t parameters;
   data_exchange_t data_exchange;
   void * opaque=&data_exchange;
   int exitcode=0;
   char * newarg[11];
   struct trace_Struct * trace=NULL;
   init_dynamic_trace( &trace,"token","url",1);
   data_exchange.trace=trace;
   data_exchange.needenvp=0;
   data_exchange.gid=getgid();
   data_exchange.uid=geteuid() ;

   cJSON * env_json=  cJSON_CreateObject();
   ck_assert_ptr_ne(env_json,NULL);
   data_exchange.env_json=env_json;

   data_exchange.paramlist=(char **)newarg;
   data_exchange.timeout=1;
   data_exchange.parameters=&parameters;
   test_stubs_t test_stubs;
   trace->tests=&test_stubs;
   // set results for tests
   test_stubs.check_namespace=0;
   test_stubs.validate_project_path_slug=0;
   test_stubs.url_check=0;
   test_stubs.write_http_config=0;
   test_stubs.verify_http_config=0;
   test_stubs.delete_http_config=0;
   test_stubs.reload_http_config=0;
   test_stubs.write_namespace=0;



   char cwd[1024];
   ck_assert_ptr_ne(getcwd(cwd, sizeof(cwd)),NULL);
   char prefix[1024];
   sprintf(prefix,"%s/cmd_release",cwd);
   parameters.prefix=prefix;

   char this_command[256];
   data_exchange.this_command=this_command;

   char projectdir[1024];
   sprintf(projectdir,"%s/projectdir_release",cwd);


   _mkdir( "/release",projectdir); // create project dir
   write_test_file(projectdir, "/release", "testfile.txt", "test_data");
   //make_dir(opaque, projectdir); // create project dir

   cJSON_AddItemToObject(env_json,"CI_ENVIRONMENT_NAME" , cJSON_CreateString("production" ));

   cJSON_AddItemToObject(env_json,"CI_PROJECT_PATH_SLUG", cJSON_CreateString("testpath-test-project" ));
   cJSON_AddItemToObject(env_json,"CI_PROJECT_PATH"     , cJSON_CreateString("testpath/test-project" ));
   cJSON_AddItemToObject(env_json,"CI_PROJECT_DIR"      , cJSON_CreateString(projectdir ));
   cJSON_AddItemToObject(env_json,"CI_PROJECT_URL"      , cJSON_CreateString("https://gitlab.example.com/testpath/test-project"));
   cJSON_AddItemToObject(env_json,"CI_COMMIT_SHA"       , cJSON_CreateString("01234567890"));
   cJSON_AddItemToObject(env_json,"DEPLOY_DOMAIN"       , cJSON_CreateString("www.example.com"));
   cJSON_AddItemToObject(env_json,"DEPLOY_DOMAIN_APP"   , cJSON_CreateString("deploy.example.com"));

   cJSON_AddItemToObject(env_json,"CI_COMMIT_REF_SLUG"  , cJSON_CreateString("1-1-1"));
   cJSON_AddItemToObject(env_json,"CI_JOB_TOKEN"        , cJSON_CreateString("dkfkdfkdjfjdfjdkjfkdf"));
   cJSON_AddItemToObject(env_json,"CI_PROJECT_ID"       , cJSON_CreateString("123"));
   cJSON * pubarray=cJSON_CreateArray();
   cJSON * path_obj=cJSON_CreateObject();
   cJSON_AddStringToObject(path_obj, "/","/release" );
   cJSON_AddItemToArray(pubarray, path_obj);
   cJSON_AddItemToObject(env_json,"DEPLOY_PUBLISH_PATH", pubarray);

    clear_dynamic_trace(trace);
   exitcode=0;

   char base_PATH[1024];
   char sub_PATH[1024];
   char read_release_json_filepath[1024];
   char tagPATH[1024];
   char tagHREF[1024];
    test_stubs.is_clean_tag=1;
   test_stubs.tagPATH=tagPATH;
   test_stubs.tagHREF=tagHREF;
   test_stubs.base_path=base_PATH;
   test_stubs.sub_path=sub_PATH;
   test_stubs.read_release_json_filepath=read_release_json_filepath;

   test_stubs.check_namespace=0;
   test_stubs.validate_project_path_slug=0;

   test_stubs.get_release=cJSON_Parse("{ \"name\":\"0.1.4\",\"message\":\"release 0.1.4\"," \
                              "\"release\":{\"tag_name\":\"0.1.4\",\"description\":\"# New release\r\n\r\n\r\n\"}}");
   test_stubs.read_relase_json=NULL;
   test_stubs.create_releases_json=strdup( \
     "\"releases\": [{\"tag\": {\"name\": \"0.1.6\",\"link\": \"https://gitlab.gioxa.com/deployctl/test_deploy_release/tags/0.1.6\"}, " \
     "\"project\": { \"name\": \"test_deploy_release\", "\
     "\"link\": \"https://gitlab.gioxa.com/deployctl/test_deploy_release\"}," \
     "\"source\": {\"name\": \"{CI_COMMIT_SHA}\", " \
     "\"link\": \"https://gitlab.gioxa.com/deployctl/test_deploy_release/tree/0.1.6\"}, " \
     "\"commit\": {\"name\": \"86e245ef\", " \
     "\"link\": \"https://gitlab.gioxa.com/deployctl/test_deploy_release/commit/d89a8974b4363b3f65256330ccdd6dec6614df4e\"}, " \
     "\"deployed\": {\"by\": \"danny.goossen@gioxa.com\",\"link\": \"https://gitlab.gioxa.com/dgoo2308\"}, " \
     "\"tag_info\": { " \
     "\"message\": \"test site and api\"," \
     "\"notes_html\": \"<p>test site and api ... etc</p>\"}, " \
     "\"files\": [{\"name\": \"file2.sh\",\"link\": \"0.1.6/files/file2.sh\"}]}]} ");
    test_stubs.create_thisrelease_json=cJSON_Parse( \
                                                   "{\"tag\": {\"name\": \"0.1.6\",\"link\": \"https://gitlab.gioxa.com/deployctl/test_deploy_release/tags/0.1.6\"}, " \
                                                      "\"project\": { \"name\": \"test_deploy_release\", "\
                                                      "\"link\": \"https://gitlab.gioxa.com/deployctl/test_deploy_release\"}," \
                                                      "\"source\": {\"name\": \"{CI_COMMIT_SHA}\", " \
                                                      "\"link\": \"https://gitlab.gioxa.com/deployctl/test_deploy_release/tree/0.1.6\"}, " \
                                                      "\"commit\": {\"name\": \"86e245ef\", " \
                                                      "\"link\": \"https://gitlab.gioxa.com/deployctl/test_deploy_release/commit/d89a8974b4363b3f65256330ccdd6dec6614df4e\"}, " \
                                                      "\"deployed\": {\"by\": \"danny.goossen@gioxa.com\",\"link\": \"https://gitlab.gioxa.com/dgoo2308\"}, " \
                                                      "\"tag_info\": { " \
                                                      "\"message\": \"test site and api\"," \
                                                      "\"notes_html\": \"<p>test site and api ... etc</p>\"}, " \
                                                      "\"files\": [{\"name\": \"file2.sh\",\"link\": \"0.1.6/files/file2.sh\"}]}" );

	test_stubs.create_release_json_js=strdup("var java\n");
   test_stubs.make_html_release=strdup("<html>");

   sprintf(base_PATH, "%s/deploy/domain/www.example.com",prefix);
   sprintf(sub_PATH, "/1-1-1/files");
   sprintf(tagPATH, "%s/public/1-1-1",base_PATH);
   sprintf(tagHREF, "http://www.example.com/1-1-1");
   sprintf(read_release_json_filepath, "%s/public/release.json",base_PATH);

   // ------------------------------------------------------------------------
   // START Release testing
   // should fail, missing environment var
   //
   exitcode= cmd_release(opaque);
   ck_assert_int_ne(exitcode,0);
    clear_dynamic_trace(trace);
   exitcode=0;

   //add missing env
   cJSON_AddItemToObject(env_json,"CI_ENVIRONMENT_SLUG" , cJSON_CreateString("production" ));

   // should fail, missing var "CI_COMMIT_TAG"

   exitcode= cmd_release(opaque);
   ck_assert_int_ne(exitcode,0);
    clear_dynamic_trace(trace);
   exitcode=0;

   //add missing env, should be ok!! make first release

   cJSON_AddItemToObject(env_json,"CI_COMMIT_TAG" , cJSON_CreateString("1.1.1" ));

   exitcode= cmd_release(opaque);
   ck_assert_int_eq(exitcode,0);
    clear_dynamic_trace(trace);
   exitcode=0;

   // make now clean ta (Beta ... etc )
   cJSON_ReplaceItemInObject(env_json,"CI_COMMIT_TAG" , cJSON_CreateString("1.1.1-beta" ));
   test_stubs.check_last_tag=NULL;
   test_stubs.is_clean_tag=0;

   exitcode= cmd_release(opaque);
   ck_assert_int_eq(exitcode,0);
    clear_dynamic_trace(trace);
   exitcode=0;

   cJSON_ReplaceItemInObject(env_json,"CI_COMMIT_TAG" , cJSON_CreateString("1.1.1-test" ));
   test_stubs.check_last_tag=strdup("1.1.1");
   test_stubs.is_clean_tag=0;
   exitcode= cmd_release(opaque);
   ck_assert_int_eq(exitcode,0);
    clear_dynamic_trace(trace);
   exitcode=0;
   test_stubs.is_clean_tag=1;

   // and cleanup
   _rmdir(prefix,"");



   _rmdir(projectdir,"");

   _mkdir( "/release",projectdir);// create project dir with release and content
      write_test_file(projectdir, "/release", "testfile", "test_data");
   // and cleanup
   _rmdir(prefix,"");

   cJSON_Delete(test_stubs.get_release);
   test_stubs.get_release=cJSON_Parse("{ \"name\":\"0.1.4\"," \
                              "\"release\":{\"tag_name\":\"0.1.4\",\"description\":\"# New release\r\n\r\n\r\n\"}}");

   // No message in return from get release should be nok!!, for now OK, no error on that
   exitcode= cmd_release(opaque);
   ck_assert_int_eq(exitcode,0);

    clear_dynamic_trace(trace);
   exitcode=0;
   // and cleanup
   _rmdir(prefix,"");

   // problem getting release: should be NOK, for now OK: Discussion!!!!
   cJSON_Delete(test_stubs.get_release);
   test_stubs.get_release=NULL; // simulate problem getting release

   exitcode= cmd_release(opaque);
   ck_assert_int_eq(exitcode,0);
    clear_dynamic_trace(trace);
   exitcode=0;
   // and cleanup
   _rmdir(prefix,"");

   // back to normal
   test_stubs.get_release=cJSON_Parse("{ \"name\":\"0.1.4\",\"message\":\"release 0.1.4\"," \
                              "\"release\":{\"tag_name\":\"0.1.4\",\"description\":\"# New release\r\n\r\n\r\n\"}}");

   // simulate name space taken
   test_stubs.check_namespace=1;

   exitcode= cmd_release(opaque);
    clear_dynamic_trace(trace);
   test_stubs.check_namespace=0;
   ck_assert_int_ne(exitcode,0);

   // production with invalid deploy_domain dns should be NOK
   test_stubs.url_check=1;
   exitcode= cmd_release(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_ne(exitcode,0);
   exitcode=0;
   test_stubs.url_check=0;

   // and cleanup
      _rmdir(prefix,"");

   // production problem writing config
   test_stubs.write_http_config=1;
   exitcode= cmd_release(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_ne(exitcode,0);
   exitcode=0;
   test_stubs.write_http_config=0;

   // and cleanup
      _rmdir(prefix,"");

   // production problem verify config
   test_stubs.verify_http_config=1;
   exitcode= cmd_release(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_ne(exitcode,0);
   exitcode=0;
   test_stubs.verify_http_config=0;

   // and cleanup
      _rmdir(prefix,"");

   // simulate wrong projectpath slug, should be ok, in production w deploy_domain
   test_stubs.validate_project_path_slug=1;
   exitcode= cmd_release(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_eq(exitcode,0);
   exitcode=0;
   test_stubs.validate_project_path_slug=0;

   // and cleanup
      _rmdir(prefix,"");

   cJSON_DeleteItemFromObject(env_json, "DEPLOY_DOMAIN");

   // ********************************************
   // release: production, without deploy domain  *
   // ********************************************
   //OK
   debug("***** release: production, without deploy domain ******\n");
	sprintf(base_PATH, "%s/deploy/domain/production.testpath-test-project.deploy.example.com",prefix);
	sprintf(sub_PATH, "/1-1-1/files");
	sprintf(read_release_json_filepath, "%s/public/release.json",base_PATH);
	sprintf(tagPATH, "%s/public/1-1-1",base_PATH);
	sprintf(tagHREF, "http://production.testpath-test-project.deploy.example.com/1-1-1");


   exitcode= cmd_release(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_eq(exitcode,0);
   exitcode=0;
   cJSON_DeleteItemFromObject(env_json, "DEPLOY_DOMAIN");

   // cleanup
      _rmdir(prefix,"");

   cJSON_DeleteItemFromObject(env_json, "DEPLOY_DOMAIN");
/* TODO: NOT IMPLEMETED YET, and also for non Production
   // Production with toooooo long domain name, NOK
   char toolong[256];
   memset(toolong, 'a', 255);
   cJSON_ReplaceItemInObject(env_json,"CI_ENVIRONMENT_SLUG" , cJSON_CreateString(toolong ));

   exitcode= cmd_static(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_ne(exitcode,0);
   exitcode=0;
*/
   cJSON_DeleteItemFromObject(env_json, "DEPLOY_DOMAIN");

   // production, without deploy domain and bad project_path_slug => NOK
   test_stubs.validate_project_path_slug=1;

   exitcode= cmd_release(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_ne(exitcode,0);
   exitcode=0;
   test_stubs.validate_project_path_slug=0;
   cJSON_DeleteItemFromObject(env_json, "DEPLOY_DOMAIN");

   // production  without deploy domain and with invalid dns should be NOK
   test_stubs.url_check=1;
   exitcode= cmd_release(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_ne(exitcode,0);
   exitcode=0;
   test_stubs.url_check=0;
   cJSON_DeleteItemFromObject(env_json, "DEPLOY_DOMAIN");

   // ***********************
   // non production, OK

    sprintf(sub_PATH, "/files");
   cJSON_ReplaceItemInObject(env_json,"CI_ENVIRONMENT_NAME" , cJSON_CreateString("review/master" ));
   cJSON_ReplaceItemInObject(env_json,"CI_ENVIRONMENT_SLUG" , cJSON_CreateString("review-master0989" ));

	sprintf(base_PATH, "%s/deploy/sites/testpath-test-project/review-master0989",prefix);
	sprintf(tagPATH, "%s/public",base_PATH);
	sprintf(tagHREF, "http://review-master0989.testpath-test-project.deploy.example.com");

   //ok
   //setdebug();
   exitcode= cmd_release(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_eq(exitcode,0);
   exitcode=0;

   //cleanup
      _rmdir(prefix,"");


   // simulate name space taken
   test_stubs.check_namespace=1;

   exitcode= cmd_release(opaque);
    clear_dynamic_trace(trace);
   test_stubs.check_namespace=0;
   ck_assert_int_ne(exitcode,0);


   // non production with invalid dns should be NOK
   test_stubs.url_check=1;
   exitcode= cmd_release(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_ne(exitcode,0);
   exitcode=0;
   test_stubs.url_check=0;

   // and cleanup
      _rmdir(prefix,"");

   // non production with missing Deploy domain app should be NOK
   cJSON_DeleteItemFromObject(env_json, "DEPLOY_DOMAIN_APP");

   exitcode= cmd_release(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_ne(exitcode,0);
   exitcode=0;
   cJSON_DeleteItemFromObject(env_json, "DEPLOY_DOMAIN");

   cJSON_AddItemToObject(env_json,"DEPLOY_DOMAIN_APP"   , cJSON_CreateString("deploy.example.com"));

   // non production with invalid project slug should be NOK
   test_stubs.validate_project_path_slug=1;
   exitcode= cmd_release(opaque);
    clear_dynamic_trace(trace);
   ck_assert_int_ne(exitcode,0);
   exitcode=0;
   test_stubs.validate_project_path_slug=0;

   // cleanup
   _rmdir(projectdir,"");
   _rmdir(prefix,"");
   cJSON_Delete(env_json);

}
END_TEST

Suite * deployd_cmd_suite(void)
{
   Suite *s;
   TCase *tc_core;
   //TCase *tc_progress;
   s = suite_create("test_deployd_cmd");
   /* Core test case */
   tc_core = tcase_create("Core");
   tcase_add_checked_fixture(tc_core, setup, NULL);
   //tcase_add_unchecked_fixture(tc_core, setup, teardown);
   tcase_set_timeout(tc_core,65);
   tcase_add_test(tc_core, check_delete_domain);
   tcase_add_test(tc_core, check_cmd_static);
   tcase_add_test(tc_core, check_cmd_release);

   suite_add_tcase(s, tc_core);
   return s;
}


int main(void)
{
   int number_failed;
   Suite *s;
   SRunner *sr;

   s = deployd_cmd_suite();
   sr = srunner_create(s);
   srunner_run_all(sr, CK_VERBOSE | CK_NOFORK);
   number_failed = srunner_ntests_failed(sr);
   srunner_free(sr);
   return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
