/*
test_cJSON_deploy.c
 Created by Danny Goossen, Gioxa Ltd on 26/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */



//LCOV_EXCL_START
#include <check.h>
//LCOV_EXCL_STOP

#include "../src/deployd.h"



static void setup(void)
{

}

static void teardown(void)
{

}


START_TEST(check_env_cjson)
{
  struct trace_Struct * trace=NULL;
  init_dynamic_trace( &trace,"token","url",1);

  int result=0;
   // OK situation
   cJSON *environment = cJSON_CreateObject();
   ck_assert_ptr_ne(environment,NULL);
   cJSON_AddItemToObject(environment,"CI_PROJECT_PATH" , cJSON_CreateString("deployctl/test_deploy_release" ));
   cJSON_AddItemToObject(environment,"CI_PROJECT_PATH_SLUG" , cJSON_CreateString("deployctl-test-deploy-release" ));
   cJSON_AddItemToObject(environment,"DEPLOY_PROJECT_PATH_SLUG" , cJSON_CreateString("DEPLOY_PROJECT_PATH_SLUG" ));
   cJSON_AddItemToObject(environment,"GITLAB_PROJECT_PATH_SLUG" , cJSON_CreateString("GITLAB_PROJECT_PATH_SLUG" ));
   cJSON_AddItemToObject(environment,"blabla" , cJSON_CreateString("deployctl-test-deploy-release" ));
   char **newevpp=NULL;

   char * tt=NULL;
   result=get_key(environment, "CI_PROJECT_PATH", &tt);
   ck_assert_int_eq( result,0);
   ck_assert_str_eq(tt,"deployctl/test_deploy_release");

   // if no place to put the output, it's still ok
   result=get_key(environment, "CI_PROJECT_PATH", NULL);
   ck_assert_int_eq( result,0);

   result=parse_env(environment, &newevpp);
   ck_assert_int_eq( result,0);

   cJSON * parsed_environment=cJSON_Create_env_obj(newevpp);

   ck_assert_ptr_ne(parsed_environment,NULL);

   // should only get environment with DEPLOY_ / GITLAB_ / CI_
   tt=NULL;
   result=get_key(parsed_environment, "blabla", &tt);
   ck_assert_int_ne( result,0);
   ck_assert_ptr_eq(tt,NULL);

   result=get_key(parsed_environment, NULL, &tt);
   ck_assert_int_eq( result,-2);
   ck_assert_ptr_eq(tt,NULL);

   // free newevpp
   free_envp(&newevpp);
   ck_assert_ptr_eq(newevpp,NULL);

   // normal ok check no missing item
   result=check_presence(parsed_environment,
                  (char *[])
   {"CI_PROJECT_PATH_SLUG","CI_PROJECT_PATH",NULL}
                  , trace);
   ck_assert_int_eq( result,0);


   // missing item
   result=check_presence(parsed_environment,
                  (char *[])
   {"CI_PROJECT_PATH_SLUG","CI_PROJECT_PATH","mytest",NULL}
                  , trace);
   ck_assert_int_ne( result,0);
   // missing item and no output buffer
   struct trace_Struct * out_null=NULL;
   result=check_presence(parsed_environment,
                 (char *[])
   {"CI_PROJECT_PATH_SLUG","CI_PROJECT_PATH","mytest",NULL}
                 , out_null);
   ck_assert_int_ne( result,0);

   // have no environment
   result=check_presence(NULL,
                 (char *[])
   {"CI_PROJECT_PATH_SLUG","CI_PROJECT_PATH","mytest",NULL}
                 , out_null);
   ck_assert_int_eq( result,-1);

   // have no list
   result=check_presence(parsed_environment, NULL, out_null);
   ck_assert_int_ne( result,0);

   // have no buffer
   result=check_presence(parsed_environment,
                    (char *[])
     {"CI_PROJECT_PATH_SLUG","CI_PROJECT_PATH","mytest","onemore",NULL}
                    , NULL);
   ck_assert_int_eq( result,-1); // two missing
   cJSON_Delete(parsed_environment);
   cJSON_Delete(environment);
   free_dynamicbuf(&trace);
}
END_TEST




START_TEST(check_extra_cJson)
{
   // check strdup_n

   char todup[1024];
   sprintf(todup, "todupstring");
   char * dupped=NULL;
   dupped=cJSON_strdup_n((unsigned char *)todup, 5);
   ck_assert_ptr_ne(dupped,NULL);

   dupped=cJSON_strdup_n(NULL, 5);
   ck_assert_ptr_eq(dupped,NULL);

   // additem
   cJSON * obj;
   cJSON * test =cJSON_CreateObject();
   cJSON * item =cJSON_CreateObject();
   item->string =strdup("xxxx");
   item->valuestring=strdup("item2");

   cJSON_AddItemToObject(test,"ITEM1" , cJSON_CreateString("item1" ));
   cJSON_AddItemToObject_n(test, todup,5 , item);
   obj=cJSON_GetObjectItem(test,"todup");
   ck_assert_ptr_ne(obj,NULL);
   cJSON_AddItemToObject_n(test, todup,5 , cJSON_CreateString("todup"));

   cJSON_AddItemToObject_n(test, todup,6 , NULL);

   obj=cJSON_GetObjectItem(test,"todups");
   ck_assert_ptr_eq(obj,NULL);
   cJSON_Delete(test);

   // cJSON_AddItemToBeginArray
   cJSON * arr = cJSON_CreateArray();
   cJSON_AddItemToBeginArray(arr, cJSON_CreateString("item1" ));
   cJSON_AddItemToBeginArray(arr, cJSON_CreateString("before_item1" ));

   // check NULL item
   cJSON_AddItemToBeginArray(arr, NULL);

   // check if correct order
   dupped=cJSON_GetArrayItem(arr,0)->valuestring;
   ck_assert_str_eq(dupped,"before_item1");
   dupped=cJSON_GetArrayItem(arr,1)->valuestring;
   ck_assert_str_eq(dupped,"item1");
   cJSON_Delete(arr);

   // check if no array
   cJSON_AddItemToBeginArray(NULL, cJSON_CreateString("item1" ));

}
END_TEST


START_TEST(check_in_string_array)
{
   cJSON * list=NULL;
   ck_assert_int_eq(in_string_array(list, "notright"),-3);
   list=cJSON_CreateStringArray((const char *[]){"notright","test","right"}, 3);
   ck_assert_int_eq(in_string_array(list, NULL),0); // if search ==NULL, result if in
   ck_assert_int_eq(in_string_array(list, "notright"),0);
   ck_assert_int_eq(in_string_array(list, "right"),0);
   ck_assert_int_eq(in_string_array(list, "notinlist"),1);
   cJSON_Delete(list);
}
END_TEST





Suite * cJson_deploy_suite(void)
{
   Suite *s;
   TCase *tc_core;
   //TCase *tc_progress;
   s = suite_create("test_cJSON_deploy_suite");
   /* Core test case */
   tc_core = tcase_create("Core");
   tcase_add_checked_fixture(tc_core, setup, teardown);
   //tcase_add_unchecked_fixture(tc_core, setup, teardown);
   tcase_set_timeout(tc_core,15);
   tcase_add_test(tc_core, check_extra_cJson);
   tcase_add_test(tc_core, check_env_cjson);
   tcase_add_test(tc_core, check_in_string_array);
   suite_add_tcase(s, tc_core);
   return s;
}

int main(void)
{
   int number_failed;
   Suite *s;
   SRunner *sr;
   s = cJson_deploy_suite();
   sr = srunner_create(s);
   srunner_run_all(sr, CK_VERBOSE | CK_NOFORK);
   number_failed = srunner_ntests_failed(sr);
   srunner_free(sr);
   return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
