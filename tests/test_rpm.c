/*
 test_rpm.c
 Created by Danny Goossen, Gioxa Ltd on 12/6/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

#include <check.h>

#include "../src/deployd.h"
#include "utils.h"

#include "test-0.1-1.el6.src.rpm.h"
#include "test-0.1-1.el7.centos.x86_64.rpm.h"
#include "test-0.1-1.x86_64.rpm.h"

START_TEST(check_normal)
{

	char basePATH[1024];
	char rpmPATH[1024];
	sprintf(basePATH,"./test_check_rpm");


	//clean
	_rmdir(basePATH,"");
	_mkdir("/projectdir/rpm",basePATH);
	sprintf(rpmPATH,"%s/projectdir/rpm/",basePATH);

	cJSON * g=cJSON_CreateObject();
	cJSON_AddItemToObject(g,"name" , cJSON_CreateString("0.1.4"));
	char * release_print=cJSON_Print(g);
	debug("going to write release.json\n");
	write_test_file(basePATH,"/public", "release.json", release_print);
	free(release_print);
	cJSON_Delete(g);
	cJSON *rpm_info=NULL;

	int result;
	// file is null : 1
	result=get_rpm_fileinfo(NULL,rpmPATH , NULL,&rpm_info);
	ck_assert_int_eq(result, RPM_ERR_BADARG);

	// path is null : 1
	result=get_rpm_fileinfo(NULL,NULL ,"tt",&rpm_info);
	ck_assert_int_eq(result, RPM_ERR_BADARG);

	write_test_file(rpmPATH,"", "test.rpm", "this is no rpm\n");
	// file is no rpm
	result=get_rpm_fileinfo(NULL,rpmPATH , "test.rpm",&rpm_info);
	ck_assert_int_eq(result, RPM_ERR_READ);

	// real rpm tests

	char filename[256];
	const char * rpm_data;
	int filelength;
	int srpm;

	// test-0.1-1.el6.src.rpm, source rpm

	sprintf(filename,"test-0.1-1.el6.src.rpm");
	rpm_data=test_0_1_1_el6_src_rpm;
	filelength=test_0_1_1_el6_src_rpm_len;
	srpm=0;


	result=write_test_file_n(rpmPATH,"",filename , rpm_data,filelength);
	ck_assert_int_eq(result, filelength);

	result=get_rpm_fileinfo(NULL,rpmPATH , filename,&rpm_info);
	ck_assert_int_eq(result, 0);
	ck_assert_ptr_ne(NULL, rpm_info);
	ck_assert_str_eq(cJSON_get_key(rpm_info, "name"),"test");
	ck_assert_str_eq(cJSON_get_key(rpm_info, "version"),"0.1");
	ck_assert_str_eq(cJSON_get_key(rpm_info, "arch"),"x86_64");
	ck_assert_str_eq(cJSON_get_key(rpm_info, "distribution"),"el6");
	ck_assert_str_eq(cJSON_get_key(rpm_info, "filename"),filename);
	ck_assert_str_eq(cJSON_get_key(rpm_info, "filepath"),rpmPATH);
	ck_assert_int_eq(cJSON_GetObjectItem(rpm_info, "SRPM")->valueint, srpm);

	cJSON_Delete(rpm_info);
	rpm_info=NULL;

	// test-0.1-1.el7.centos.x86_64.rpm,

	sprintf(filename,"test-0.1-1.el7.centos.x86_64.rpm");
	filelength=test_0_1_1_el7_centos_x86_64_rpm_len;
	rpm_data=test_0_1_1_el7_centos_x86_64_rpm;
	srpm=0;


	result=write_test_file_n(rpmPATH,"",filename , rpm_data,filelength);
	ck_assert_int_eq(result, filelength);

	result=get_rpm_fileinfo(NULL,rpmPATH , filename,&rpm_info);
	ck_assert_int_eq(result, 0);
	ck_assert_ptr_ne(NULL, rpm_info);
	ck_assert_str_eq(cJSON_get_key(rpm_info, "name"),"test");
	ck_assert_str_eq(cJSON_get_key(rpm_info, "version"),"0.1");
	ck_assert_str_eq(cJSON_get_key(rpm_info, "arch"),"x86_64");
	ck_assert_str_eq(cJSON_get_key(rpm_info, "distribution"),"el7");
	ck_assert_str_eq(cJSON_get_key(rpm_info, "filename"),filename);
	ck_assert_str_eq(cJSON_get_key(rpm_info, "filepath"),rpmPATH);
	ck_assert_int_eq(cJSON_GetObjectItem(rpm_info, "SRPM")->valueint, srpm);

	cJSON_Delete(rpm_info);
	rpm_info=NULL;


	// test-0.1-1.x86_64.rpm,

	sprintf(filename,"test-0.1-1.x86_64.rpm");
	filelength=test_0_1_1_x86_64_rpm_len;
	rpm_data=test_0_1_1_x86_64_rpm;
	srpm=0;


	result=write_test_file_n(rpmPATH,"",filename , rpm_data,filelength);
	ck_assert_int_eq(result, filelength);

	result=get_rpm_fileinfo(NULL,rpmPATH , filename,&rpm_info);
	ck_assert_int_eq(result, 0);
	ck_assert_ptr_ne(NULL, rpm_info);
	ck_assert_str_eq(cJSON_get_key(rpm_info, "name"),"test");
	ck_assert_str_eq(cJSON_get_key(rpm_info, "version"),"0.1");
	ck_assert_str_eq(cJSON_get_key(rpm_info, "arch"),"x86_64");
	ck_assert_ptr_eq(cJSON_get_key(rpm_info, "distribution"),NULL);
	ck_assert_str_eq(cJSON_get_key(rpm_info, "filename"),filename);
	ck_assert_str_eq(cJSON_get_key(rpm_info, "filepath"),rpmPATH);
	ck_assert_int_eq(cJSON_GetObjectItem(rpm_info, "SRPM")->valueint, srpm);

	cJSON_Delete(rpm_info);
	rpm_info=NULL;


	_rmdir(basePATH,"");

}
END_TEST






Suite * test_suite(void)
{
	Suite *s;
	TCase *tc_core;
	//TCase *tc_progress;
	s = suite_create("test_error");
	/* Core test case */
	tc_core = tcase_create("Core");
	//tcase_add_checked_fixture(tc_core, setup, teardown);
	tcase_add_unchecked_fixture(tc_core, NULL,NULL);
	tcase_set_timeout(tc_core,15);
	tcase_add_test(tc_core, check_normal);
	suite_add_tcase(s, tc_core);
	return s;
}


int main(void)
{
	int number_failed;
	Suite *s;
	SRunner *sr;

	s = test_suite();
	sr = srunner_create(s);
	srunner_run_all(sr, CK_NORMAL);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);
	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
