/*
 test_letsencrypt.c
 Created by Danny Goossen, Gioxa Ltd on 28/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

#include <check.h>
#include "../src/deployd.h"

int cmd_exec(void * opaque);

typedef struct test_stubs_s
{
   char * arg0;
   char * arg1;
   char * arg2;
   char * arg3;
   int exitcode;
} test_stubs_t;


// stubfunctions:
int cmd_exec(void * opaque)
{
   test_stubs_t * teststubs= (test_stubs_t *)((data_exchange_t *)opaque)->trace->tests;
   ck_assert_str_eq(teststubs->arg0, ((data_exchange_t *)opaque)->paramlist[0]);
   ck_assert_str_eq(teststubs->arg1, ((data_exchange_t *)opaque)->paramlist[1]);
   ck_assert_str_eq(teststubs->arg2, ((data_exchange_t *)opaque)->paramlist[2]);
   return(teststubs->exitcode);
}

int exec_color(void * opaque)
{
test_stubs_t * teststubs= (test_stubs_t *)((data_exchange_t *)opaque)->trace->tests;
return(teststubs->exitcode);

}

//helper function for tests
int write_test_file(const char * filename, const char * data)
{
   debug("write test file\n");
   FILE *f = fopen(filename, "w");
   if (f == NULL){ debug("Error opening file : %s\n",strerror(errno));return -1;}
   /* print some text */
   int retvalue=0;
   int errvalue=0;
   do{ retvalue=fprintf(f, "%s",data);} while ( retvalue==-1 && (errvalue=errno)== EINTR );
   if (retvalue<0) { debug("Error io-command %s: %s\n", filename,strerror(errvalue)); return -1; }
   if (fclose(f) <0 ) { debug("Error io-command %s: %s\n", filename,strerror(errno));return -1;}
   return retvalue;
}


int  update_details(void * userp)
{
   return 0;
}

START_TEST(check_lets_encrypt)
{

   data_exchange_t data_exchange;
   parameter_t parameters;
   void * opaque=&data_exchange;
   char * newarg[4];

   struct trace_Struct * trace=NULL;
   init_dynamic_trace( &trace,"token","url",1);
   data_exchange.trace=trace;

   data_exchange.needenvp=0;
   //data_exchange.gid=getgid();
   //data_exchange.uid=geteuid() ;
   //setdebug();
   cJSON * env_json=  cJSON_CreateObject();
   ck_assert_ptr_ne(env_json,NULL);
   data_exchange.env_json=env_json;

   data_exchange.paramlist=(char **)newarg;
   data_exchange.timeout=1;
   data_exchange.parameters=&parameters;
   parameters.testprefix="./test_letsencrypt_d";
   test_stubs_t test_stubs;
   trace->tests=&test_stubs;
   // set results for tests
   test_stubs.exitcode=0;
   test_stubs.arg0="/bin/sh";
   test_stubs.arg1="-c";
   test_stubs.arg2="/opt/deploy/.acme.sh/acme.sh --issue --keylength 4096 --home /opt/deploy/.acme.sh -w /opt/deploy/var -d test.com"; //--email test@test.com
   test_stubs.arg3=NULL;
   const char * domain_dir="./test_letsencrypt_d/opt/deploy/.acme.sh/test.com/";
   const char * cert_filename="./test_letsencrypt_d/opt/deploy/.acme.sh/test.com/fullchain.cer";
    const char * key_filename="./test_letsencrypt_d/opt/deploy/.acme.sh/test.com/test.com.key";

   _rmdir("./test_letsencrypt_d","");


   int result=letsencrypt(opaque, "test.com", "test@test.com");
   ck_assert_int_ne(result, 0);
   //const char * cert_missing="+ /bin/sh -c /usr/bin/certbot certonly --no-self-upgrade --non-interactive --agree-tos --rsa-key-size 4096 --webroot -w /opt/deploy/var --email test@test.com -d test.com \nERROR: certificate missing\n";
   //ck_assert_str_eq(output_buf,cert_missing);
   debug("%s\n",get_dynamic_trace(trace));
   clear_dynamic_trace(trace);

   _mkdir("/opt/deploy/.acme.sh/test.com/","./test_letsencrypt_d");

   // add cert, should report missing key
   write_test_file(key_filename, "key");
   write_test_file(cert_filename, "cert");

   // add cert and key so should return OK

   result=letsencrypt(opaque, "test.com", "test@test.com");
   ck_assert_int_eq(result, 0);
   //const char * result_ok="+ /bin/sh -c /usr/bin/certbot certonly --no-self-upgrade --non-interactive --agree-tos --rsa-key-size 4096 --webroot -w /opt/deploy/var --email test@test.com -d test.com \n";
   //ck_assert_str_eq(output_buf,result_ok);

   debug("%s\n",get_dynamic_trace(trace));
   clear_dynamic_trace(trace);
   // now set prefix and make dir with exec mkdir -p ..... and create cert
   // and then key
   _rmdir("./test_letsencrypt_d","");
   free_dynamic_trace(&trace);
}
END_TEST

Suite * externals_suite(void)
{
   Suite *s;
   TCase *tc_core;
   //TCase *tc_progress;
   s = suite_create("test_externals");
   /* Core test case */
   tc_core = tcase_create("Core");
   tcase_add_checked_fixture(tc_core, NULL,NULL);
   //tcase_add_unchecked_fixture(tc_core, setup, teardown);
   tcase_set_timeout(tc_core,65);
   tcase_add_test(tc_core, check_lets_encrypt);

   suite_add_tcase(s, tc_core);
   return s;
}

int main(void)
{
   int number_failed;
   Suite *s;
   SRunner *sr;

   s = externals_suite();
   sr = srunner_create(s);
   srunner_run_all(sr, CK_VERBOSE | CK_NOFORK);
   number_failed = srunner_ntests_failed(sr);
   srunner_free(sr);
   return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
