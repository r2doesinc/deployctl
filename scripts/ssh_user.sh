#!/bin/sh
#
#  ssh_user.sh
#  deployctl
#
#  Created by Danny Goossen on 1/4/17.
#
# MIT License
#
# Copyright (c) 2017 deployctl, Gioxa Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

usage()
{
    echo "usage"
 #   echo "ssh_port=<ssh_port> && RSA_PUB_KEY=<rsa pub key> && ssh_user=<newuser> && ./secure"
    echo "ssh_port=<ssh_port> && ssh_user=<newuser> && ./secure"
    echo
    echo "Make sure /root/.ssh/authorized_keys exists, this will be copied for new user"
    echo
    [ 1 -eq 0 ]
}
#[[ -n "$RSA_PUB_KEY" ]] || usage;
[[ -n "$ssh_user" ]] || usage;
[[ -n "$ssh_port" ]] || usage
set +e
echo add user
set -x
adduser "$ssh_user"
set -e
mkdir -pv /home/$ssh_user/.ssh
chmod 700 /home/$ssh_user/.ssh
cp -fv /root/.ssh/authorized_keys /home/$ssh_user/.ssh/authorized_keys
chmod 600 /home/$ssh_user/.ssh/authorized_keys
# add user pub_key to authorized_keys of the new user
#echo "$RSA_PUB_KEY" >> "/home/$ssh_user/.ssh/authorized_keys"
# change owner of the ssh dir to the user
chown -R $ssh_user /home/$ssh_user/.ssh
# restore selinux context for .ssh
restorecon -r -v -F /home/$ssh_user/.ssh
# add new user to sudoers
echo " $ssh_user ALL=(ALL)   ALL" >> /etc/sudoers
# set pswd = user
echo "$ssh_user:$ssh_user" | chpasswd
# force user to change passwd
chage -d 0 $ssh_user
# change listen port sshd
sed -ri.bak "s/#?Port .*/Port $ssh_port/" /etc/ssh/sshd_config
#disable root Login
sed -ri.bak "s/#?PermitRootLogin .*/PermitRootLogin no/" /etc/ssh/sshd_config
# disable password authentification
sed -ri.bak "s/#?PasswordAuthentication .*/PasswordAuthentication no/" /etc/ssh/sshd_config
set +e
semanage port -a -t ssh_port_t -p tcp $ssh_port
set -e
systemctl restart sshd
set +x
echo
echo "new usr =$ssh_user, pwd = $ssh_user and port=$ssh_port"
echo
echo "color prompt red for root"
cat >> /etc/bashrc << \EOF
if [ $(id -u) -eq 0 ];
then # you are root, make the prompt red
   export PS1="\[\033[01;31m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[01;31m\]#\[\033[0m\] "
else
  export PS1="\[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[m\]\$ "
fi
EOF
export PS1="\[\033[01;31m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[01;31m\]#\[\033[0m\] "
