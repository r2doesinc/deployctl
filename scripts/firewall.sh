#!/bin/sh
#
#  firewall.sh
#
# Created by Danny Goossen on 2/3/16.
#
# MIT License
#
# Copyright (c) 2017 deployctl, Gioxa Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#




usage()
{
    echo "usage"
    echo "ssh_port=<ssh_port> && ./csf"
    [ 1 -eq 0 ]
}

[ -z "$ssh_port" ] || usage

set -e

#echo ###### Install dependencies for csf ##########
echo -e "\n\033[33m--------  remove firewalld if present ------\033[0;m\n"

set +e
## TODO if service or systemd
systemctl stop firewalld 2>&1 >/dev/null
systemctl disable firewalld  2>&1 >/dev/null
set -e
echo -e "\n\033[33m--------  install iptables ------\033[0;m\n"
yum -y install iptables-services
touch /etc/sysconfig/iptables
touch /etc/sysconfig/iptables6
systemctl start iptables
systemctl start ip6tables
systemctl enable iptables
systemctl enable ip6tables
echo -e "\n\033[33m--------  install csf dependencies ------\033[0;m\n"
yum -y install wget perl unzip net-tools perl-libwww-perl perl-LWP-Protocol-https perl-GDGraph -y
cd /opt
echo -e "\n\033[33m--------  downloading csf ------\033[0;m\n"
wget https://download.configserver.com/csf.tgz
tar -xzf csf.tgz
cd csf
echo -e "\n\033[33m--------  install csf ------\033[0;m\n"
./install.generic.sh
echo
echo -e "\n\033[33m--------  configure csf ------\033[0;m\n"
echo

cd /etc/csf
set +e
set -x
sed -i 's/^TESTING =.*/TESTING = "0"/' csf.conf
sed -i "s/^TCP_IN =.*/TCP_IN = \"$ssh_port,80,443\"/" csf.conf
sed -i 's/^TCP_OUT =.*/TCP_OUT = "1:65535"/' csf.conf
sed -i 's/^UDP_IN =.*/UDP_IN = ""/' csf.conf
sed -i 's/^UDP_OUT =.*/UDP_OUT = "1:65535"/' csf.conf
sed -i 's/^SMTP_BLOCK =.*/SMTP_BLOCK = "1"/' csf.conf
sed -i "s/^TCP6_IN =.*/TCP6_IN = \"$ssh_port,80,443\"/" csf.conf
sed -i 's/^TCP6_OUT =.*/TCP6_OUT = "1:65535"/' csf.conf
sed -i 's/^UDP6_IN =.*/UDP6_IN = ""/' csf.conf
sed -i 's/^UDP6_OUT =.*/UDP6_OUT = "1:65535"/' csf.conf
sed -i 's/^HOST =.*/HOST = "\/usr\/bin\/hostname"/' csf.conf
sed -i 's/^RESTRICT_UI =.*/RESTRICT_UI = "2"/' csf.conf
sed -i 's/^SYSLOG_CHECK =.*/SYSLOG_CHECK = "300"/' csf.conf
sed -i 's/^RESTRICT_SYSLOG =.*/RESTRICT_SYSLOG = "2"/' csf.conf
sed -i 's/^UDPFLOOD =.*/UDPFLOOD = "1"/' csf.conf
set +x
echo -e "\n\033[33m--------  restart csf ------\033[0;m\n"
set -x
csf -r
set +x
echo -e "\n\033[33m--------  cleanup ------\033[0;m\n"
rm -rf /opt/csf
rm /opt/csf.tgz

echo -e "\n\033[32;1m *** Success installing new firewall, ssh port $ssh_port / http and https open! ***\033[0;m\n"
