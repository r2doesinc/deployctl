#!/bin/sh
#  register.sh
#  deployctl
#
#  Created by Danny Goossen on 11/8/17.
#
# MIT License
#
# Copyright (c) 2017 deployctl, Gioxa Ltd.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
echo
echo "Registering new deployctl runner"
echo
echo "Enter the gitlab url e.g. : https://gitlab.com , followed by [ENTER]:"

read CI_SERVER_URL

echo "Enter the registration token, followed by [ENTER]:"

read REGISTRATION_TOKEN

if [ -z "$CI_SERVER_URL" ]; then
echo "gitlab url cannot be empty, exit"
exit 1;
fi

if [ -z "$REGISTRATION_TOKEN" ]; then
echo "token cannot be empty, exit"
exit 1;
fi

RUNNER_NAME=deployctl-$HOSTNAME
RUNNER_TAG_LIST=deployctl-$HOSTNAME

APIv4register="$CI_SERVER_URL/api/v4/runners/"
DATA_REGISTER="{\"token\": \"$REGISTRATION_TOKEN\",\"tag_list\": [\"$RUNNER_TAG_LIST\"],\"run_untagged\" : false,\"locked\": true, \"description\": \"$RUNNER_NAME\"}"

echo
echo "Registering runner..."
register_result=`echo "$DATA_REGISTER" | curl -sS -L -H "Content-Type: application/json"  --request POST $APIv4register -T -`

if [ ! "$?" == 0 ]; then
echo -e "\033[0;31m"
echo "---------------------------------------------"
echo "        token registration Failed"
echo "---------------------------------------------"
echo -e "\033[0;m"
exit 1
fi

token=`echo $register_result | sed -n 's/^.*"token":"\([a-z0-9]\+\)".*$/\1/p'`

# need verify to exit with set -e
if [ -z "$token" ]; then
echo -e "\033[0;31m"
echo "---------------------------------------------"
echo "token registration Failed"
echo "error: $register_result"
echo "---------------------------------------------"
echo -e "\033[0;m"


  exit 1
else
  echo
  echo "Got a new runner-token"
  echo
fi

echo "updating /opt/deploy/config/runners.yaml"
cat >> /opt/deploy/config/runners.yaml << EOF
---
token: $token
ci_url: $CI_SERVER_URL
interval: 1

EOF

echo -e "\033[0;33m"
echo "restart 'deployd' e.g. systemctl restart deployd"
echo "for the new runner to start!"
echo -e "\033[0;m"


echo -e "\033[0;32m"
echo "********************************************"
echo "                  SUCCESS"
echo "********************************************"
echo -e "\033[0;m"