# Config server for CI web-deployment

## Purpose

Setup a webserver, configured with auto deployment and auto_https_config directly from gitlab.

## Prerequisites

 1. minimal install centos7, with /root/.ssh/authorized_keys for auto ssh login establist.
the `ssh_user.sh` script will copy this authorised keys to the new ssh_user.

 2. Define some environment variables:

  * For Secure and csf firewall:

```
export ssh_user=<new_user>
export ssh_port=<ssh_port>

```

   * For deployctl-runner:

```
export CI_SERVER_URL=https://gitlab.example.com/ci
export REGISTRATION_TOKEN=theglobalorprivatprojecttoken
```

see https://docs.gitlab.com/ce/ci/runners/README.html#creating-and-registering-a-runner for more info

## SETUP:

with environment var's defined for the different scripts:

 1. user_ssh : setup user, ssh, disable root login, no password login, sudoer
 2. firewall : Configure the firewall
 3. setup.sh : setup script: installs nginx, acme.sh, deployctl(-runner)

 ## Putting it all together

```
export ssh_user=<new_user>
export ssh_port=<ssh_port>

export CI_SERVER_URL="https://gitlab.<example.com>
export REGISTRATION_TOKEN=<theglobalorprivatprojecttoken>

curl https://downloads.deployctl.com/latest/files/ssh_user.sh | bash
curl https://downloads.deployctl.com/latest/files/firewall.sh | bash
curl https://downloads.deployctl.com/latest/files/setup.sh | bash
```
