//
//  debug_repos.c
//  deployctl
//
//  Created by Danny Goossen on 30/6/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//


#include <stdio.h>
#include <stdlib.h>
#include "../src/cJSON_deploy.h"
#include "../src/repos.h"
#include "../src/common.h"

// struct to set return value of stub functions
typedef struct test_stubs_s
{
   char * arg0;
   char * arg1;
   char * arg2;
   char * arg3;
   int exitcode;

   
   int get_rpm_fileinfo;
   cJSON *rpm_info;
   
   cJSON * JSON_dir_list_x;
} test_stubs_t;


// stubfunctions:
int get_rpm_fileinfo(void * opaque, const char * filepath, const char * filename,cJSON ** rpm_info)
{
   return 0;
}

cJSON * JSON_dir_list_x( const char *path,const char *subpath,int recursive, const char * extension)
{
   return NULL;
}

int cmd_exec(void * opaque);
int cmd_write(void * opaque);


// stubfunctions:
int cmd_exec(void * opaque)
{
   test_stubs_t * teststubs= (test_stubs_t *)((data_exchange_t *)opaque)->feedback->tests;
   char ** paramlist=((data_exchange_t *)opaque)->paramlist;
  // ck_assert_str_eq(teststubs->arg0, paramlist[0]);
  // ck_assert_str_eq(teststubs->arg1, paramlist[1]);
  // ck_assert_str_eq(teststubs->arg2, paramlist[2]);
   return(teststubs->exitcode);
}


int main ()
{
   const char rpm_info_str[]= \
   "[" \
   // fully defined arch/dist => No symlink
   "{ \"arch\":	\"x86_64\", \"distribution\":	\"el7\", \"SRPM\":	false, \"type\":	\"rpm\"," \
   "\"filename\":	\"A.el7.centos.x86_64.rpm\"," \
   "\"filepath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/\"," \
   "\"fullpath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/A.el7.centos.x86_64.rpm\"}, " \
   
   // i686 arch for i386 repo but distributio => symlink to all /rpm/el6/*
   "{ \"arch\":	\"i686\", \"distribution\":	\"el6\", \"SRPM\":	false, \"type\":	\"rpm\"," \
   "\"filename\":	\"B1.el6.i686.rpm\"," \
   "\"filepath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/\"," \
   "\"fullpath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/B1.el6.i686.rpm\"}," \
 
   // no arch but distributio => symlink to all /rpm/el6/*
   "{ \"arch\":	\"noarch\", \"distribution\":	\"el6\", \"SRPM\":	false, \"type\":	\"rpm\"," \
   "\"filename\":	\"B.el6.noarch.rpm\"," \
   "\"filepath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/\"," \
   "\"fullpath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/B.el6.noarch.rpm\"}," \
   
   // no arch no dist => symlink to /rpm/*
   "{ \"arch\":	\"noarch\", \"SRPM\":	false, \"type\":	\"rpm\"," \
   "\"filename\":	\"C.noarch.rpm\"," \
   "\"filepath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/\"," \
   "\"fullpath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/C.noarch.rpm\"}," \

   // arch, no dist => only symlink to all /rpm/*/x86_64
   "{ \"arch\":	\"x86_64\", \"SRPM\":	false, \"type\":	\"rpm\"," \
   "\"filename\":	\"D.x86_64.rpm\"," \
   "\"filepath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/\"," \
   "\"fullpath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/D.x86_64.rpm\"}," \

   // arch, no dist / but not in endpoints => into skips
   "{ \"arch\":	\"armhfp\", \"SRPM\":	false, \"type\":	\"rpm\"," \
   "\"filename\":	\"E.armhfp.rpm\"," \
   "\"filepath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/\"," \
   "\"fullpath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/E.armhfp.rpm\"}," \

   // arch, dist / but not in endpoints => into skips
   "{ \"arch\":	\"noarch\",  \"distribution\":	\"el8\",\"SRPM\":	false, \"type\":	\"rpm\"," \
   "\"filename\":	\"F.noarch.rpm\"," \
   "\"filepath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/\"," \
   "\"fullpath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/F.noarch.rpm\"}," \

   // fully defined arch/dist , but error => into skips
   "{ \"arch\":	\"x86_64\", \"distribution\":	\"el7\", \"SRPM\":	false, \"type\":	\"rpm\"," \
   "\"filename\":	\"G.el7.centos.x86_64.rpm\"," \
   "\"filepath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/\"," \
      "\"error\":	\"invalid rpm\"," \
   "\"fullpath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/G.el7.centos.x86_64.rpm\"}, " \
   
   // fully defined rpm /dist/arch but SRPM => skips
   "{ \"arch\":	\"x86_64\", \"distribution\":	\"el7\", \"SRPM\":	true, \"type\":	\"rpm\"," \
   "\"filename\":	\"S.el7.centos.x86_64.rpm\"," \
   "\"filepath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/\"," \
   "\"fullpath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/S.el7.centos.x86_64.rpm\"}, " \
   
// debian
   // fully defined deb/dist/release and in endpoints => To maches and no symlink
   "{  \"type\":	\"deb\", \"distribution\":	\"ubuntu\", \"release\": \"trusty\", " \
   "\"filename\":	\"A.deb\"," \
   "\"filepath\":	\"/projectdir/deb/ubuntu/trusty\"," \
   "\"fullpath\":	\"/projectdir/deb/ubuntu/trusty/A.deb\"}, " \
   
   //  deb/dist/* and in endpoints => To maches, symlink /deb/ubuntu/*
   "{  \"type\":	\"deb\", \"distribution\":	\"ubuntu\", " \
   "\"filename\":	\"B.deb\"," \
   "\"filepath\":	\"projectdir/deb/ubuntu/\"," \
   "\"fullpath\":	\"projectdir/deb/ubuntu/B.deb\"}, " \
   
   //  deb/* and in endpoints => To maches, symlink /deb/*
   "{  \"type\":	\"deb\",  " \
   "\"filename\":	\"C.deb\"," \
   "\"filepath\":	\"projectdir/deb/\"," \
   "\"fullpath\":	\"projectdir/deb/C.deb\"}, " \

   // fully defined deb/dist/release and not in endpoints => skips
   "{  \"type\":	\"deb\", \"distribution\":	\"raspbian\", \"release\": \"buster\", " \
   "\"filename\":	\"D.deb\"," \
   "\"filepath\":	\"/projectdir/deb/raspbian/buster\"," \
   "\"fullpath\":	\"/projectdir/deb/raspbian/buster/D.deb\"}, " \

   // fully defined deb/dist/release and wrong endpoint => skips
   "{  \"type\":	\"deb\", \"distribution\":	\"ubuntu\", \"release\": \"trusty\", " \
   "\"filename\":	\"E.deb\"," \
   "\"filepath\":	\"/projectdir/deb/ubuntu/trusty/x\"," \
   "\"error\":	\"wrong subpath\"," \
   "\"subpath\":	\"/deb/ubuntu/trusty/x\"," \
   "\"fullpath\":	\"/projectdir/deb/ubuntu/trusty/x/E.deb\"} " \
  
   "]";
   
   const char repos_str[]= \
   "[" \
             "{\"rpm\":	[{ \"el7\":	[ {\"amd64\": \"x86_64\"}, { \"i386\":	[\"i386\", \"i686\"]}, {\"armhfp\":	\"armv7hl\"}]}," \
                        "{ \"el6\":	[ \"x86_64\", { \"i386\":	[\"i386\", \"i686\"]}]} "\
             "]}, " \
             "{\"deb\":	[{ \"debian\":	[\"wheezy\", \"jessie\", \"stretch\"]}, " \
                        "{ \"ubuntu\":	[\"trusty\", \"zesty\"]} ]" \
             "} " \
     "]";
   
   cJSON * repos=cJSON_Parse(repos_str);
   char* print_str=cJSON_Print(repos);
   if (print_str)
   {
      printf("Endpoints: \n%s\n",print_str);
      free(print_str);
      print_str=NULL;
   }

   
   cJSON *  rpm_info=cJSON_Parse(rpm_info_str);
  
   cJSON *  end_points=NULL; //cJSON_Parse(endpoints_str);
   cJSON * trace= cJSON_CreateArray();
   char * temp=extract_dir_structure(repos,&end_points,trace);
   
   
   
   if (temp)
   {
   printf("create dir: \n>%s<\n",temp);
      free(temp);
      temp=NULL;
   }
   print_str=cJSON_Print(end_points);
   if (print_str)
   {
      printf("Endpoints: \n%s\n",print_str);
      free(print_str);
      print_str=NULL;
   }
   
   cJSON * matches=NULL;
   cJSON * skips=NULL;
   cJSON * symlinks=NULL;
   
   
   match_end_points(NULL, rpm_info,end_points, &matches,&skips, &symlinks);
   
   
   print_str=cJSON_Print(end_points);
   if (print_str)
   {
   	printf("Endpoints: \n%s\n",print_str);
      free(print_str);
      print_str=NULL;
   }
   
   print_str=cJSON_Print(rpm_info);
   if (print_str)
   {
      printf("RPM_info: \n%s\n",print_str);
      free(print_str);
      print_str=NULL;
   }
   print_str=cJSON_Print(matches);
   if (print_str)
   {
      printf("Matches: \n%s\n",print_str);
      free(print_str);
      print_str=NULL;
   } else printf("No Matches");
   print_str=cJSON_Print(symlinks);
   if (print_str)
   {
      printf("Symlinks: \n%s\n",print_str);
      free(print_str);
      print_str=NULL;
   } else printf("No symlinks");
   print_str=cJSON_Print(skips);
   if (print_str)
   {
      printf("Skips: \n%s\n",print_str);
      free(print_str);
      print_str=NULL;
   }
   else printf("No skips\n");
   // in_list
      printf("result");
   return 0;
}