//
//  debugcjson.c
//  deployctl
//
//  Created by Danny Goossen on 23/6/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#include "debugcjson.h"
#include <stdio.h>
#include <stdlib.h>
#include "../src/cJSON_deploy.h"
#include "../src/common.h"

int main ()
{
       const char rpm_info_str[]= \
       "[{ \"arch\":	\"x86_64\", \"distribution\":	\"el7\", \"SRPM\":	false, \"type\":	\"rpm\"," \
       "\"filename\":	\"libcjson-1.4.5-1.el7.centos.x86_64.rpm\"," \
       "\"filepath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/\"," \
       "\"fullpath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/libcjson-1.4.5-1.el7.centos.x86_64.rpm\"}, " \
       "{ \"arch\":	\"x86_64\", \"SRPM\":	false, \"type\":	\"rpm\"," \
      "\"filename\":	\"libcjson-1.4.5-1.x86_64.rpm\"," \
      "\"filepath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/\"," \
      "\"fullpath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/libcjson-1.4.5-1.el7.centos.x86_64.rpm\"}," \
      "{ \"arch\":	\"noarch\", \"SRPM\":	false, \"type\":	\"rpm\"," \
      "\"filename\":	\"libcjson-1.4.5-1.x86_64.rpm\"," \
      "\"filepath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/\"," \
      "\"fullpath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/libcjson-1.4.5-1.el7.centos.x86_64.rpm\"}]";
      
       const char endpoints_str[]= \
       "[{ \"/rpm/el7/x86_64\":	[\"rpm\", \"el7\", \"x86_64\"] }, " \
       "{ \"/rpm/nodist/x86_64\":	[\"rpm\", \"nodist\", \"x86_64\"] }, " \
        "{ \"/rpm/el7/i386\":	[\"rpm\", \"el7\", \"i386\"]}]" ;
       
       
   
   
   
   cJSON *  rpm_info=cJSON_Parse(rpm_info_str);
   cJSON *  end_points=cJSON_Parse(endpoints_str);
   cJSON * matches=NULL;
   cJSON * skips=NULL;
   match_end_points( rpm_info,end_points, &matches,&skips);
   // in_list
   cJSON * list=cJSON_CreateStringArray((const char *[]){"notright","test","right"}, 3);
   int result;
   result=in_string_array(list, "notright");
   result=in_string_array(list, "test");
   result=in_string_array(list, "right");
   result=in_string_array(list, "notinlist");
   printf("result");
  return 0;
}