[![build status](https://gitlab.gioxa.com/deployctl/deployctl/badges/master/build.svg)](https://gitlab.gioxa.com/deployctl/deployctl/commits/master)
[![release](http://downloads.deployctl.com/tag.svg)](http://downloads.deployctl.com/latest)
[![coverage report](https://gitlab.gioxa.com/deployctl/deployctl/badges/master/coverage.svg)](http://master.deployctl-deployctl.gioxapp.com/files/deployctl-coverage)
[www.deployctl.com](https://www.deployctl.com)

# deployctl

a gitlab-ci-multi-runner helper to create webcontent and releases.

## Purpose

 Using Gitlab and the integrated CI/CD for software and webpages is a great way to keep track of things and manage the projects,
it being opensource and out of the box support for CI/CD makes it a must have tool in software development.

With this project we hope to add even more or easier deployment of Releases and/or static webpages.

 Easy deployment of environments for :
 
 1. static web pages
 2. publishing software releases, with release notes / downloads / metrics / links
 3. rpm/deb-repository
 
 from a Gitlab-instance (gitlab.com and/or private instance), out of the box **HTTPS**

 * No ssh-key's / tokens / passwords ..etc  needed for deployment 
 * multiple sites and environments on a deploy-server 
 * Simple configuration, straight form the `gitlab-ci.yml`
 * run once the setup on a server with a domain-name and a wild-card DNS entry.
 * tag's and release notes

## install
 
 Minimal install Centos7, with /root/.ssh/authorized_keys for auto ssh login,
 
  1. the `ssh_user.sh` script will copy this authorised keys to the new ssh_user.
  2. the `firewall.sh` will open new ssh port and port 80/443 for http/https.(Config Server Firewall)
  3. `setup.sh` will install acme.sh/nginx and deployctl
 
 ```
 export ssh_user=<new_user>
 export ssh_port=<ssh_port>

 curl https://downloads.deployctl.com/latest/files/ssh_user.sh | bash
 curl https://downloads.deployctl.com/latest/files/firewall.sh | bash
 bash -c "$(curl -s https://downloads.deployctl.com/latest/files/setup.sh)"
 ```

And input your gitlab-instance or gitlab.com as usrl and the private project token or general runner registration token.

*Remark:*

For automated install:

```
curl -s https://downloads.deployctl.com/latest/files/setup.sh | CI_SERVER_URL=https://gitlab.example.com REGISTRATION_TOKEN=<token> bash
```

ps: replace `https://gitlab.example.com` with `https://gitlab.com` or your private gitlab-instance url.
 
## Overview

All scripts commands are processed by the deployctl command processor and non deployctl commands are discarded, ensuring proper safty and protectection of the deployment server.

The runner get's the tag of the hostname and deployctl, e.g. `deployctl-gioxapp.com` where `gioxapp.com` is the hostname of the deployment server.

By adding in the tags section of a JOB in the `.gitlab-ci.yml` file allows to deploy to the domain_app or custom domai.

## example's
 
### webdeployment 
 
 extract from deployment of https://www.deployctl.com `.gitlab-ci.yml`
 
``` yalm
variables:
  #
  # missing CI variable, planned to be correct in Gitlab 10 
  CI_PROJECT_PATH_SLUG: "deployctl-www-deployctl-com"
  #
  # domain hostname of the deployment server
  DEPLOY_DOMAIN_APP: "gioxapp.com"
  #
  # Custom domain for production deployment
  DEPLOY_DOMAIN : "www.deployctl.com"
  #
  # Configure https for production deployment
  DEPLOY_CONFIG_HTTPS: "True"
  #
  # Location of the content for the site
  DEPLOY_PUBLISH_PATH: '["/":"/web/_site/"]'
 
relase:
  stage: release
  tags:
    - gioxapp-deployctl
  environment:
    name: production
    url: http://$DEPLOY_DOMAIN
  script:
    - deployctl static
  only:
    - master
```
 
*Remark:*
  1. make sure the `DEPLOY_DOMAIN_APP` points to the app-server
  2. make sure the `DEPLOY_DOMAIN` has a CNAME record pointing to `$DEPLOY_DOMAIN_APP`
  3. make sure the `CI_PROJECT_PATH_SLUG` is defined.
  4. with `DEPLOY_CONFIG_HTTPS: "True"`, site is https configured
  5. with  `DEPLOY_PUBLISH_PATH: '["/":"/web/_site/"]'`, content in `/web/_site` will be availleble at `<domain>/`
 
### Repository  rpm and apt/deb

#### Setup the Repository

 example from `repo.deployctl.com`,  add `repo.yaml` to a new repo.
 
 
 ``` yaml
 ---
 # define the projects that can deploy to repo
 projects:
   - https://gitlab.gioxa.com/deployctl/deployctl
   - https://gitlab.gioxa.com/deployctl/cJSON
 
 # define the repos
 repos:
   - rpm:
     - el7:
       - x86_64
       - armv7hl
     - fed25:
       - x86_64
     - el6:
       - noarch
 ```
 
 and define the `gitlab-ci.yml`
 
 ```yaml
 stages:
 - production
 
 variables:
 CI_PROJECT_PATH_SLUG: "deployctl-repo"
 DEPLOY_DOMAIN: "repo.deployctl.com"
 DEPLOY_CONFIG_HTTPS: "True"
 
 
 production:
   stage: production
   tags: 
   - deployctl-gioxapp.com
   environment:
     name: production
     url: http://$DEPLOY_DOMAIN/$CI_BUILD_REF_SLUG
   script:
   - deployctl repo_config
 ```
 
 Commit, and done
 
 By defining `DEPLOY_CONFIG_HTTPS: "True"` Repo will be configured for https access.
 
#### Deploy packages to the repository:
 
 example here has one stage production, first it will create release page, then push the rpm's to the repository 
 
 ```yaml
 variables:
  CI_PROJECT_PATH_SLUG: "deployctl-deployctl"
  DEPLOY_DOMAIN_APP: "gioxapp.com"
  #
  # Prodduction deployment url and https
  DEPLOY_DOMAIN: "downloads.deployctl.com"
  DEPLOY_CONFIG_HTTPS: "True"
  #
   #Deployment locations
  DEPLOY_RELEASE_PATH: '["output/","output/rpm/","output/dist/"]'
  #
  # Extra links for release page, besides the <*.url> files
  DEPLOY_hrefs: '[ "repository": "$DEPLOY_REPO_URL", "website": "http://www.deployctl.com"]'
  #
  # location directory of rpm/deb dir
  DEPLOY_REPO_PATH: "output"
  #
  # The repository url
  DEPLOY_REPO_URL: http://repo.deployctl.com
 
 Release_production:
   stage: production
   variables:
     GIT_STRATEGY: none
   environment:
     name: production
     url: https://$DEPLOY_DOMAIN/$CI_BUILD_REF_SLUG
   tags:
     - deployctl-<myhostname>
   script:
     - deployctl release
     - deployctl rpm_add
 ```
 
 #### Use the Repository
 
First add repository to the client system:

For RPM based systems:

```bash
curl <my repo_rl>/repo.rpm.sh | sudo bash
```

or 

debian / apt systems:
```bash
curl <my repo_rl>/repo.deb.sh | sudo bash
```

And 

```bash
 yum install <mynewpackage>
```

or for deb-based systems:

```bash
 apt-get install <mynewpackage>
```
 
### commands

 1. **deployctl static** : deploy static page for the environment
 2. **deployctl release** : create a download page for the environment
 3. **deployctl repo_config** : Configures a repository (production only)
 4. **deployctl rpm_add** : push packages to repository
 5. **deployctl delete**: deletes the environment

__Remarks__:

 * `DEPLOY_CONFIG_HTTPS: "True"` works only for production environment
 * **deployctl rpm_add** only needs `DEPLOY_REPO_URL: "http://repo.deployctl.com`
 
### checkout 

 Read more on: [www.deployctl.com](https://www.deployctl.com)
