/*
 common.h
 Created by Danny Goossen, Gioxa Ltd on 18/2/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

#ifndef deployctl_common_h
#define deployctl_common_h

#define FEEDBACK_FINISH 0
#define FEEDBACK_INTERMEDIATE 1


#ifndef GITVERSION
#define GITVERSION "10.0"
#endif

#ifndef PACKAGE_VERSION
#define PACKAGE_VERSION "0.0.0"
#endif

#ifndef SECRET
#define SECRET "qwerty"
#endif

#ifndef SOCK_NAME
#define   SOCK_NAME "/tmp/test.sock"
#endif

#define INVALID_SOCKET -1
#define SOCKET_ERROR -1

#include <err.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

#include <fcntl.h>
#include <sys/ioctl.h>
#include <errno.h>

#include <pwd.h>
#include <grp.h>
#include <ctype.h>

#include <string.h>
#include <strings.h>

#include <limits.h>

#include <syslog.h>
#include <pthread.h>
#include <signal.h>
#include <getopt.h>

#include <sys/socket.h>
#include <sys/resource.h>
#include <sys/un.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/termios.h>
#include <paths.h>

#include <dirent.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/param.h>
#include <time.h>

#include <cJSON.h>
#include <cmark.h>
#include <git2.h>
#include <yaml.h>


#include <zip.h>

#include <openssl/err.h>
#include <curl/curl.h>

#define PCRE2_CODE_UNIT_WIDTH 8
#include <regex.h>

#if HAVE_PTY_H==1
#include <pty.h>
#endif


#define       ANSI_BOLD_BLACK    "\033[30;1m"
#define       ANSI_BOLD_RED      "\033[31;1m"
#define       ANSI_BOLD_GREEN    "\033[32;1m"
#define       ANSI_BOLD_YELLOW   "\033[33;1m"
#define       ANSI_BOLD_BLUE     "\033[34;1m"
#define       ANSI_BOLD_MAGENTA  "\033[35;1m"
#define       ANSI_BOLD_CYAN     "\033[36;1m"
#define       ANSI_BOLD_WHITE    "\033[37;1m"
#define       ANSI_BLACK    "\033[30m"
#define       ANSI_RED      "\033[31m"
#define       ANSI_GREEN    "\033[32m"
#define       ANSI_YELLOW   "\033[33m"
#define       ANSI_BLUE     "\033[34m"
#define       ANSI_MAGENTA  "\033[35m"
#define       ANSI_CYAN     "\033[36m"
#define       ANSI_WHITE    "\033[37m"
#define       ANSI_RESET         "\033[0;m"
#define       ANSI_CLEAR         "\033[0K"
#define       ANSI_CROSS_BLACK    "\033[30;9m"
#define       ANSI_CROSS_RED      "\033[31;9m"
#define       ANSI_CROSS_GREEN    "\033[32;9m"
#define       ANSI_CROSS_YELLOW   "\033[33;9m"
#define       ANSI_CROSS_BLUE     "\033[34;9m"
#define       ANSI_CROSS_MAGENTA  "\033[35;9m"
#define       ANSI_CROSS_CYAN     "\033[36;9m"
#define       ANSI_CROSS_WHITE    "\033[37;9m"

extern const char * color_data[];
enum term_color { bold_black,bold_red,bold_green,bold_yellow,bold_blue,bold_magenta,bold_cyan,bold_white,black,red,green,yellow,blue,magenta,cyan,white,cross_black,cross_red,cross_green,cross_yellow,cross_blue,cross_magenta,cross_cyan,cross_white,none};

// struct for uid and gid
typedef struct uidguid_t
{
    uid_t uid;
    gid_t gid;
} uidguid_t;

// pointers to pass to the curl callback function to interupt a long polling
// see get_job()
typedef struct stop_struct_s
{
    int * exit;
    int * reload;
} stop_struct_t;

/*------------------------------------------------------------------------
 * global application parameters
 *------------------------------------------------------------------------*/

 typedef struct parameter_s
{
   char * socket_name;
   u_int8_t daemon; // 1 is daemon
   u_int8_t exec;
   char * pidfile;
   u_int8_t verbose;
   u_int8_t quiet;
   char  * secret;
   char * uid_user;
   char * gid_user;
   char * prefix;
   int current_user;
   char * config_file;
   int debug;
   int timeout;
   char * testprefix;
   pthread_mutex_t * nginx_lock;
   pthread_mutex_t * repo_lock;
   int hubsignal;
   int exitsignal;
} parameter_t;

// struct exchange data for functions

typedef struct data_exchange_t
{
   char ** paramlist;
    parameter_t * parameters;
   //char ** newenvp;
   cJSON * env_json;
   const char * this_command;
   const char * shellcommand;
   int needenvp;
   gid_t gid;
   uid_t uid;
   int current_user;
   struct trace_Struct * trace;
   int timeout;
} data_exchange_t;

// for trace
struct trace_Struct {
   cJSON *params;
   char * url;
   int job_id;
   size_t mark;
   void * tests;
   CURL * curl;
   size_t trace_buf_size;
   size_t min_trace_buf_increment;
   size_t trace_len;
};
#endif
