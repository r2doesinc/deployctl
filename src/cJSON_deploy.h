/*
 cJSON_deploy.h
 Created by Danny Goossen, Gioxa Ltd on 11/2/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */


#ifndef __deployctl__cJSON_deploy__
#define __deployctl__cJSON_deploy__


#include "common.h"



// create/parse and free **env for execenvp()
extern cJSON * cJSON_Create_env_obj(char** envp);
extern int parse_env(cJSON * env_json, char *** newevpp);
extern void free_envp(char *** envp);
int get_key(cJSON * item, const char * key,char ** value );

// create string from non null terminated string of n characters

extern cJSON *cJSON_CreateString_n(const char *string, size_t n);
extern void   cJSON_AddItemToObject_n(cJSON *object, const char *string,size_t n , cJSON *item);

void   cJSON_AddItemToBeginArray(cJSON *array, cJSON *item);


char * cJSON_get_key(cJSON * item, const char * key);
int validate_key(cJSON * item, const char * key,const char * value );
int check_presence(cJSON * env_json, char ** list, struct trace_Struct *trace);
char* cJSON_strdup_n(const unsigned char * str, size_t n);
cJSON *cJSON_CreateString_n(const char *string, size_t n);
int in_string_array(cJSON * json_list, char * search);



#define cJSON_AddStringToObject_n(object,name,s,n) cJSON_AddItemToObject(object, name, cJSON_CreateString_n(s,n))

#define print_json(x) if (x) {char * printit=cJSON_Print(x);debug("%s\n",printit);free (printit);} else { debug("cJSON is NULL\n");}




#endif /* defined(__deployctl__cJSON_deploy__) */
