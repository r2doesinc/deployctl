//
// zip.c
//  deploy-runner
//
//  Created by Danny Goossen on 22/7/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#include "deployd.h"

void _mkdir_m(const char *dir, const char * base,mode_t mode) {
   char tmp[1024];
   char *p = NULL;
   size_t len;
   if (!dir || strlen(dir)==0)
   {
      if (!base || strlen(base)==0) return;
      else snprintf((char*)tmp,1024,"%s",base);
   }
   else if (!base || strlen(base)==0)
   {
      if (!dir || strlen(dir)==0) return;
      else snprintf((char*)tmp,1024,"%s",dir);
   }
   else
   {
      if (dir[0]=='/')
         snprintf((char*)tmp,1024,"%s%s",base,dir);
      else
         snprintf((char*)tmp,1024,"%s/%s",base,dir);
   }
   struct stat sb;
   if (stat(tmp, &sb) == 0 && S_ISDIR(sb.st_mode))
   {
      //debug("directory %s exists\n",tmp);
   }
   else
   {
      len = strlen(tmp);
      if(tmp[len - 1] == '/')
         tmp[len - 1] = 0;
      //debug("mkdir %s\n",tmp);
      for(p = tmp + 1; *p; p++)
         if(*p == '/') {
            *p = 0;
            if (stat(tmp, &sb) == 0 && S_ISDIR(sb.st_mode))
            {
               //debug("sub directory %s exists\n",tmp);
            }
            else{
               //debug("mkdir sub %s\n",tmp);
               mkdir(tmp,mode);
            }
            *p = '/';
         }
      //debug("mkdir sub %s\n",tmp);
      mkdir(tmp, mode);
   }
}


#define FA_RDONLY       0x01            // FILE_ATTRIBUTE_READONLY
#define FA_DIREC        0x10            // FILE_ATTRIBUTE_DIRECTORY
#define FA_LINK        0x400            // FILE_ATTRIBUTE_LINK

#ifndef ZIP_OPSYS_DOS
#define ZIP_OPSYS_DOS	  	0x00u
#endif

#ifndef ZIP_TRUNCATE
#define ZIP_TRUNCATE         8
#endif


static mode_t _zip_attr2mode(zip_uint8_t opsys, zip_uint32_t attr)
{
   mode_t m =0777;
   if (opsys==ZIP_OPSYS_DOS)
   {
      int special=0;
      //mode_t m = S_IRUSR | S_IRGRP | S_IROTH;
      if (0 != (attr & FA_RDONLY))
      {// set readonly only
         mode_t ro_masks=(S_IWUSR  | S_IWGRP | S_IWOTH);
         m &= ~ro_masks;
      }
      if (attr & FA_DIREC) // directory
      {
         special =1;
         m = (S_IFDIR | m);
      }
      if (attr & FA_LINK) // link
      {
         special=1;
         m = (S_IFLNK | m );
      }
      if (!special)	m=m | S_IFREG;
      
      return m;
   }
   else if(opsys==ZIP_OPSYS_UNIX)
   {
      //                               0170000
      m=(mode_t)(attr >> 16L) &(mode_t)0177777;
      return m;
   }
   return m;
}


int unzip(const char *archive, const char *dest)
{
   struct zip *za;
   struct zip_file *zf;
   struct zip_stat sb;
   char errstr[100];
   char buf[100];
   char filename[1024];
   int err;
   ssize_t i, len;
   int fd;
   long long sum;
   //debug("start unzip\n");
   if (!dest || strlen(dest)==0)
   {
      error("dest null\n");
      return 1;
   }
   
   _mkdir(dest,"");
   if ((za = zip_open(archive, 0, &err)) == NULL) {
      zip_error_to_str(errstr, sizeof(errstr), err, errno);
      error("unzip: can't open zip archive `%s': %s\n",archive,errstr);
      return 1;
   }
   
   for (i = 0; i < zip_get_num_entries(za, 0); i++) {
      if (zip_stat_index(za, i, 0, &sb) == 0) {
         len = strlen(sb.name);
         alert("unzip: Name: [%s], Size: [%llu], mtime: [%u]\n", sb.name, sb.size, (unsigned int)sb.mtime);
         zip_uint32_t attr=0;
         zip_uint8_t opsys=0;
         mode_t mode=0;
         zip_file_get_external_attributes(za, i, 0,&opsys, &attr);
         //full_name = zip_get_name(z_archive, i, 0);
         mode=_zip_attr2mode(opsys, attr);
         
         printf("mode : [octal] %o\n",mode);
         {
            switch (mode & 0170000) {
               case  S_IFIFO:
                  printf("named pipe (fifo)\n");
                  break;
               case S_IFCHR:
                  printf("character special\n");
                  break;
               case S_IFDIR:
                  printf("directory\n");
                  _mkdir_m(sb.name,dest,mode&0777);
                  break;
               case S_IFBLK:
                  printf("block special\n");
                  break;
               case S_IFREG:
                  printf("regular\n");
               {
                  char * path=NULL;
                  split_path_file_2_path(&path, sb.name);
                  //debug("%s f2p: %s\n",sb.name,path);
                  if (path && strlen(path)>0) _mkdir(path,dest);
                  if (path) free(path);
                  
                  zf = zip_fopen_index(za, i, 0);
                  if (!zf) {
                     zip_error_to_str(errstr, sizeof(errstr), err, errno);
                     error("unzip: zip index archive `%s': %s\n",archive,errstr);
                     
                     return 1;
                  }
                  sprintf(filename,"%s/%s",dest,sb.name);
                  alert("Filename to open rw >%s<\n",filename);
                  fd = open(filename, O_RDWR | O_TRUNC | O_CREAT , mode&0777);
                  if (fd < 0) {
                     error("unzip: Open Output file %s: %s\n",filename, strerror(errno));
                     return 1;
                  }
                  //else debug("writing %s",filename);
                  sum = 0;
                  ssize_t w_ret=0;
                  while (sum != sb.size) {
                     ;
                     while  ((len = zip_fread(zf, buf, 100))==SOCKET_ERROR && errno==EINTR) {;}
                     if (len < 0) {
                        zip_error_to_str(errstr, sizeof(errstr), err, errno);
                        error("unzip: read error Archive `%s': %s\n",archive,errstr);
                        return 1;
                     }
                     while  ((w_ret=write(fd, buf,(int)len))==SOCKET_ERROR && errno==EINTR) {;}
                     if (w_ret > 0)
                        sum += w_ret;
                     else
                        break;
                  }
                  fsync(fd);
                  close(fd);
                  zip_fclose(zf);
                  if (w_ret >=0 )
                     alert("unzip: wrote %s, %lld\n",filename,sum);
                  else
                  {
                     error("w_ret!>0\n");
                     zip_close(za);
                     return 1;
                  }
                  
               }
                  break;
               case S_IFLNK:
                  printf("symbolic link\n");
               {
                  char * path=NULL;
                  split_path_file_2_path(&path, sb.name);
                  //debug("%s f2p: %s\n",sb.name,path);
                  if (path && strlen(path)>0) _mkdir(path,dest);
                  if (path) free(path);
                  
                  zf = zip_fopen_index(za, i, 0);
                  if (!zf) {
                     zip_error_to_str(errstr, sizeof(errstr), err, errno);
                     error("unzip: zip index archive `%s': %s\n",archive,errstr);
                     
                     return 1;
                  }
                  sprintf(filename,"%s/%s",dest,sb.name);
                  struct stat link_ss;
                  if (lstat(filename, &link_ss)==0) unlink(filename);
                  sum = 0;
                  while (sum != sb.size) {
                     ;
                     while  ((len = zip_fread(zf, buf, 100))==SOCKET_ERROR && errno==EINTR) {;}
                     if (len < 0) {
                        zip_error_to_str(errstr, sizeof(errstr), err, errno);
                        error("unzip: read error Archive `%s': %s\n",archive,errstr);
                        return 1;
                     }
                     if (len>0)
                     {
                        sum=sum+len;
                     }
                     if (len==0) break;
                     
                  }
                  buf[sb.size]=0;
                  zip_fclose(zf);
                  symlink(buf , filename);
               }
                  break;
               case S_IFSOCK:
                  printf("socket\n");
                  break;
               default:
                  break;
            }
            
         }
         
      }
   }
   if (zip_close(za) == -1) {
      error( "unzip: can't close zip archive `%s'\n", archive);
      return 1;
   }
   
   return 0;
}

int list_zip_it(char * upload_zip_name,char * CI_PROJECT_DIR,cJSON * filelist)
{
   int result=0;
   int error_n = 0;
   struct zip *archive = zip_open(upload_zip_name, ZIP_TRUNCATE | ZIP_CREATE, &error_n);
   if(!archive)
   {
      error("could not open or create archive\n");
      return -1;
   }
   //debug ("start list_it\n");
   mode_t mode=0;
   cJSON * item;
   cJSON_ArrayForEach(item,filelist)
   {
      char * path=NULL;
      if (item->type==cJSON_String)
         path=item->valuestring;
      else
         path=cJSON_get_key(item, "path");
      if (cJSON_GetObjectItem(item, "mode"))
      {
         mode=(mode_t)cJSON_GetObjectItem(item, "mode")->valueint;
      }
      else
      {
         struct stat sb;
         if (stat(path, &sb) == 0 ) mode=sb.st_mode;
      }
      zip_uint32_t attr=0;
      attr=((mode ) << 16L);
      
      char rel_file[1024];
      if (strncmp(path,CI_PROJECT_DIR,strlen(CI_PROJECT_DIR))==0 )
      {
         snprintf(rel_file,1024,"%s",path+strlen(CI_PROJECT_DIR)+1);
         //debug("archive filename: %s\n",rel_file);
      } else
      {
         error("filename outside projectdir\n");
         continue;
      }
      if (S_ISDIR(mode))
      {
         int index = (int)zip_add_dir(archive, rel_file);
         if (index>0) zip_file_set_external_attributes(archive, index, 0, ZIP_OPSYS_UNIX, attr);
      }
      if (S_ISLNK(mode)) {
         char * link=calloc(1, 1024);
         //readlinkat()
         ssize_t size_link=readlink(path , link, 1023);
         if (size_link > 0)
         {
            struct zip_source *source = zip_source_buffer(archive , link, ( zip_uint64_t)size_link,1); // 1 being freed if no more needed
            if (source)
            {
               int index = (int)zip_add(archive, rel_file, source);
               if (index>0) zip_file_set_external_attributes(archive, index, 0, ZIP_OPSYS_UNIX, attr);
            }
            else error("failed to create source buffer: %s \n", zip_strerror(archive) );
         }
         else error("failed to read link: %s \n",path );
      }
      if (S_ISREG(mode))
      {
         struct zip_source *source = zip_source_file(archive, path, 0, 0);
         //zip_source_buffer(archive, data, sizeof(data), 0);
         if(source == NULL)
         {
            error("failed to create source buffer: %s \n", zip_strerror(archive) );
            result=1;
            break;
         }
         // todo calculate filename relative to project_dir
         int index = (int)zip_add(archive, rel_file, source);
         if(index < 0 )
         {
            int zep,sep;
            zip_error_get(archive, &zep, &sep);
            if (zep== ZIP_ER_EXISTS )
            {
               error("failed to add file to archive, already exists: %s ", zip_strerror(archive) );
               zip_source_free(source);
            }
            else
            {
               error("failed to add file to archive: %s ", zip_strerror(archive) );
               zip_source_free(source);
               result=1;
               break;
            }
         }
         else
         {
            zip_file_set_external_attributes(archive, index, 0, ZIP_OPSYS_UNIX, attr);
         }
      }
   }
   zip_close(archive);
   return result;
}
