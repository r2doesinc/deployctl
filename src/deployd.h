/*
 deployd.h
 Created by Danny Goossen, Gioxa Ltd on 18/2/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */


#ifndef deployctl_deployd_h
#define deployctl_deployd_h

#ifndef VERBOSE_YN
#define VERBOSE_YN 1
#endif

#ifndef APP_NAME
#define   APP_NAME "deployd"
#endif

#ifndef DAEMONIZE_YN
#define DAEMONIZE_YN 0
#endif
#ifndef PID_FILE
#define PID_FILE "/var/run/deployed.pid"
#endif
#ifndef CONFIG_FILE
#define CONFIG_FILE "/opt/deploy/config/runners.yaml"
#endif

#ifndef PREFIX
#define PREFIX "/opt"
#endif

#ifndef UIDUSER
#define UIDUSER "root"
#endif

#ifndef GIDUSER
#define GIDUSER "nginx"
#endif




#define MAX_RETURN_BUFF 0x8000
#define TIME_OUT_CHILD 60

#include "common.h"
#include "cJSON_deploy.h"
#include "dyn_buffer.h"
#include "error.h"
#include "gitlab_api.h"
#include "deploy-runner.h"
#include "dyn_buffer.h"
#include "yaml2cjson.h"
#include "utils.h"
#include "downloads.h"
#include "commands.h"
#include "exec.h"
#include "prepare_runner.h"
#include "libgit2.h"
#include "release_html.h"
#include "exec.h"
#include "svg_badge.h"
#include "exec.h"
#include "http_config.h"
#include "deployd_cmd.h"
#include "release_html.h"
#include "letsencrypt.h"
#include "externals.h"
#include "repos.h"
#include "substitute_repo_script.h"

#include "downloads.h"
#include "openssl.h"
#include "repo_io.h"
#include "rpm.h"
#include "dpl_zip.h"

#endif
