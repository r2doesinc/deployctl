//
//  repos.c
//  deployctl
//
//  Created by Danny Goossen on 10/5/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//


#include "deployd.h"

/*
 --- yaml repos
 projects:
 - projecturl1
 - projecturlb

 repos:
 - rpm:
 - centos7:
 - x86_64
 - armhfp
 - fedora25:
 - x64_86
 - coreos:
 - x86_64
 - apt:
 - general
 - x86_64
 - kodi:

 mkdir -pv repopath/(rpm/(centos7/(x86_64,armhfp),))
 */
void add_endpoint( cJSON** end_points, cJSON * trace_endpoints)//, char *value)
{
   char endpoint_str[1024];
   cJSON * value=NULL;
   cJSON * pos=NULL;
   value=cJSON_CreateArray();

   endpoint_str[0]=0;
   cJSON_ArrayForEach(pos,trace_endpoints)
   {
      if (cJSON_IsString(pos))
      {
         cJSON_AddItemToArray(value, cJSON_CreateString(pos->valuestring));
         sprintf(endpoint_str+strlen(endpoint_str), "/%s",pos->valuestring);
      }
      else
      { // if not string :
         // use key of child and add object as e.g. {"amd64":	"x86_64"}
         sprintf(endpoint_str+strlen(endpoint_str), "/%s",pos->child->string);
         cJSON_AddItemToArray(value, cJSON_Duplicate(pos, 1));
      }
   }
   cJSON * tmp= cJSON_CreateObject();
   cJSON_AddItemToObject(tmp, endpoint_str,  value);
   if (!*end_points) *end_points=cJSON_CreateArray();
   cJSON_AddItemToArray(*end_points, tmp);
   tmp=NULL;
}


char * extract_dir_structure( cJSON* repo, cJSON** end_points, cJSON * trace_endpoints)
{
   struct MemoryStruct mem;
   init_dynamicbuf(&mem);
   if (!repo) return NULL;
   cJSON * pos;
   int item=0;
   char * temp=NULL;
   if (repo->type==cJSON_Array)
   {
      cJSON_ArrayForEach(pos,repo)
      {
         if (item) Writedynamicbuf(",", &mem);
         if (pos->type==cJSON_Object)
         {
			/* if (strchr(pos->child->string,' '))
			 { // quote if name contains spaces
				Writedynamicbuf("\"", &mem);
            	Writedynamicbuf(pos->child->string, &mem);
				Writedynamicbuf("\"", &mem);
			 }
			else */
				Writedynamicbuf(pos->child->string, &mem);
			cJSON * this_trace= cJSON_Duplicate(trace_endpoints, 1);
            if (cJSON_GetArraySize(this_trace) <2)
            { // not at the end, call ourselfs to create next element in the trace.
               cJSON_AddItemToArray(this_trace, cJSON_CreateString(pos->child->string));
               temp= extract_dir_structure(pos->child,end_points,this_trace);
            }
            else
            { // end of the line, add object to trace and make endpoint.
               cJSON_AddItemToArray(this_trace, cJSON_Duplicate(pos, 1));
               add_endpoint(end_points, this_trace);
            }
            cJSON_Delete(this_trace);
            this_trace=NULL;
         }
         if (pos->type==cJSON_String) // end of the line, add as endpoint
         {
			 if (strchr(pos->valuestring,' '))
			 { // quote if name contains spaces
				 Writedynamicbuf("\"", &mem);
				 Writedynamicbuf(pos->valuestring, &mem);
				 Writedynamicbuf("\"", &mem);
			 }
			 else
			     Writedynamicbuf(pos->valuestring, &mem);
            cJSON * this_trace= cJSON_Duplicate(trace_endpoints, 1);
            cJSON_AddItemToArray(this_trace, cJSON_CreateString(pos->valuestring));
            add_endpoint(end_points, this_trace);
         }
         if (temp){
            Writedynamicbuf("/", &mem);
            Writedynamicbuf(temp, &mem);
            free(temp);
            temp=NULL;
         }
         item++;
      }
   }
   if (mem.memory && strlen(mem.memory)==0)
   {
      free_dynamicbuf(&mem);
   }
   if (item>1 && mem.memory)
   {
      char * temp_result=calloc(1,strlen(mem.memory)+3);
      sprintf(temp_result,"{%s}",mem.memory);
      free_dynamicbuf(&mem);
      return temp_result;
   }
   else
      return mem.memory;
}



/*------------------------------------------------------------------------
 * converts a repo.json obj to a javascript var with project info
 * returns NULL on errors
 * return a null terminated string, caller is responsible for freeing it!
 *------------------------------------------------------------------------*/
char * create_repo_json_js (void * opaque, char * download_url, char * repo_json)
{
   debug("start  create_repo_json_js\n");
   cJSON * env_cjson=((data_exchange_t *)opaque)->env_json;

   struct MemoryStruct js_out;
   init_dynamicbuf(&js_out);

   /*
    var target = "Releases"
    var download_url= "https://downloads.deployctl.com";
    var deployctl_version="0.0.20";
    var project_info={
    "name":	"deployctl",
    "link":	"https://gitlab.gioxa.com/deployctl/deployctl",
    "path": "/deployctl/deployctl"
    };
    */

   Writedynamicbuf("var target = \"",  &js_out);
   Writedynamicbuf( "Repository",  &js_out);
   Writedynamicbuf("\";\n",  &js_out);


   if (download_url)
   {
      Writedynamicbuf("var download_url = \"",  &js_out);
      Writedynamicbuf( download_url,  &js_out);
      Writedynamicbuf("\";\n",  &js_out);
   }
   Writedynamicbuf("var deployctl_version = \"",  &js_out);
   Writedynamicbuf( PACKAGE_VERSION,  &js_out);
   Writedynamicbuf("\";\n",  &js_out);
   // project_info
   Writedynamicbuf("var project_info = {",  &js_out);
   // project name
   Writedynamicbuf( "\"name\" : \"",  &js_out);
   Writedynamicbuf( cJSON_get_key(env_cjson, "CI_PROJECT_NAME"),  &js_out);
   Writedynamicbuf("\",\n",  &js_out);
   // project path
   Writedynamicbuf( "\"path\" : \"",  &js_out);
   Writedynamicbuf( cJSON_get_key(env_cjson, "CI_PROJECT_PATH"),  &js_out);
   Writedynamicbuf("\",\n",  &js_out);
   // project path
   Writedynamicbuf( "\"link\" : \"",  &js_out);
   Writedynamicbuf( cJSON_get_key(env_cjson, "CI_PROJECT_URL"),  &js_out);
   Writedynamicbuf("\"",  &js_out);
   // close
   Writedynamicbuf("};\n",  &js_out);
   if (repo_json)
   {
      Writedynamicbuf("var RepoData = ",  &js_out);
      Writedynamicbuf(repo_json,  &js_out);
      Writedynamicbuf(";\n",  &js_out);
   }
   return (js_out.memory);
}





void get_rpm_info(cJSON * rpm_list,cJSON ** repo_info)
{
   cJSON * pos=NULL;
   cJSON* rpm_info_item=NULL;
   cJSON_ArrayForEach(pos,rpm_list)
   {
      char temp[1024];
      sprintf(temp, "%s/",cJSON_get_key(pos,"path")); // need to add slash
      debug("%s%s\n",temp,cJSON_get_key(pos,"filename"));
      get_rpm_fileinfo(NULL,temp, cJSON_get_key(pos,"filename"),&rpm_info_item);
      if (rpm_info_item)
      {
         if (!*repo_info ) *repo_info=cJSON_CreateArray();
         cJSON_AddItemToArray(*repo_info, rpm_info_item);
         rpm_info_item=NULL;
      }
   } // for each rpmfile
   return;
}

void get_deb_info(cJSON * deb_list,cJSON ** repo_info)
{
   if (!deb_list || !repo_info) return;
   cJSON * item=NULL;
   cJSON * pos=NULL;
   cJSON_ArrayForEach(pos,deb_list)
   {
      item=cJSON_CreateObject();
      char *buf=strdup(cJSON_get_key(pos, "subpath"));
      int i = 0;
      char *p = strtok (buf, "/");
      char *array[4];
      while (p != NULL&& i<4)
      {
         array[i++] = p;
         p = strtok (NULL, "/");
      }
      if (i>3 || i ==0)
      {
         cJSON_AddStringToObject(item, "error","wrong subpath");
         cJSON_AddStringToObject(item, "subpath",cJSON_get_key(pos, "subpath"));
      }
      else
         cJSON_AddStringToObject(item, "type",array[0]);
      if (i>1)
          cJSON_AddStringToObject(item, "distribution",array[1]);
      if (i>2)
         cJSON_AddStringToObject(item, "release",array[2]);

      cJSON_AddStringToObject(item, "filename", cJSON_get_key(pos, "filename"));
      cJSON_AddStringToObject(item, "fullpath",cJSON_get_key(pos, "filepath"));
      if (!*repo_info)* repo_info =cJSON_CreateArray();
      cJSON_AddItemToArray(*repo_info, item);
      free(buf);
      buf=NULL;
   }
   
   return;
}

void repo_rpm_list( const char *projectpath, cJSON** repo_info)
{
   if (!projectpath || strlen(projectpath)==0) return ;
   debug("getting directory listing: %s/rpm\n",projectpath);
   cJSON * filearray=JSON_dir_list_x( projectpath,"rpm",0, ".rpm");
   debug("DONE directory listing: %s/rpm\n",projectpath);
   get_rpm_info(filearray,repo_info);
   if (filearray)cJSON_Delete(filearray);
   return;
}

void repo_deb_list( const char *projectpath, cJSON** repo_info)
{
   if (!projectpath || strlen(projectpath)==0) return;
   debug("getting directory listing: %s/deb\n",projectpath);
   cJSON * filearray=JSON_dir_list_x( projectpath,"deb",1, ".deb");
   debug("DONE directory listing: %s/deb\n",projectpath);
   get_deb_info( filearray, repo_info);
   if (filearray)cJSON_Delete(filearray);
   return ;
}

int get_repo_list(void * opaque, const char *projectpath, cJSON** repo_info)
{
   repo_rpm_list(projectpath,repo_info);
   repo_deb_list(projectpath,repo_info);
   return 0;
}

int match_end_points(void * opaque, cJSON* repo_info, cJSON* end_points, cJSON ** matches, cJSON ** skips, cJSON** symlinks)
{

   // find endpoint for packages and/or wildcards and/or skipped packages.

   cJSON * pos=NULL;
   cJSON_ArrayForEach(pos,repo_info)
   {
      int found=0;
      const char * type=cJSON_get_key(pos, "type");
      const char * distribution=cJSON_get_key(pos, "distribution");
      const char * arch=cJSON_get_key(pos, "arch");
      const char * release=cJSON_get_key(pos, "release");
       const char * error=cJSON_get_key(pos, "error");
      const char * endpoint_type=NULL;
      const char * endpoint_distribution=NULL;
      const char * endpoint_arch=NULL;
      const char * endpoint_release=NULL;
      const char * endpoint=NULL;
      cJSON * tmp=NULL;
      int srpm=0;
      if ((tmp=cJSON_GetObjectItem(pos, "SRPM"))) srpm=tmp->valueint;

      cJSON * pos_endpoints=NULL;
      int wildcard_found_1=0;
      int wildcard_found_2=0;
      int wildcard_found_3=0;
      if (!error && !srpm)
      {
         cJSON_ArrayForEach(pos_endpoints,end_points)
         {
            endpoint_type =cJSON_GetArrayItem(pos_endpoints->child, 0)->valuestring;
            endpoint_distribution =cJSON_GetArrayItem(pos_endpoints->child, 1)->valuestring;
            endpoint_arch=NULL;
            cJSON * arch_item= cJSON_GetArrayItem(pos_endpoints->child, 2);
            if ( cJSON_IsString(arch_item)) endpoint_arch=arch_item->valuestring;
            if ( cJSON_IsObject(arch_item)) {
               if (cJSON_IsString(arch_item->child))
               endpoint_arch=arch_item->child->valuestring;
               else if (cJSON_IsArray(arch_item->child) )
               {
                  if (!arch || strcmp(arch,"noarch")==0 )
                  {
                     endpoint_arch=arch_item->child->string;
                  }
                  else
                  {
                     cJSON * rpm_arch_list=NULL;
                     cJSON_ArrayForEach(rpm_arch_list,arch_item->child)
                     {
                        if (strcmp(arch,rpm_arch_list->valuestring)==0)  {
                           endpoint_arch=rpm_arch_list->valuestring;
                        }
                     }
                     // if still NULL give it the repo arch
                     if (endpoint_arch==NULL) endpoint_arch=arch_item->child->string;
                  }
               }
            }
            endpoint_release =cJSON_GetArrayItem(pos_endpoints->child, 2)->valuestring;
            endpoint =pos_endpoints->child->string;

            // debug
            if (strcmp(type,"rpm")==0 && !distribution && arch && !endpoint_release && strcmp(arch,"i486")==0)
            {
               // this is the culprit
               debug("%s: %s :%s\n", cJSON_get_key(pos, "filename"),endpoint,endpoint_arch);
            }

            if ( \
                (  type && endpoint_type && strcmp(type,endpoint_type)==0 ) && \
                ( ( ! distribution && endpoint_distribution && strcmp("nodist",endpoint_distribution)==0  ) || \
                 (   distribution && endpoint_distribution && strcmp(distribution,endpoint_distribution)==0 ) \
                 ) && \
                 (( arch && endpoint_arch && strcmp(arch,endpoint_arch)==0 ) || ( release && endpoint_release && strcmp(release,endpoint_release)==0 ))
                )
            { // found in Full
               if (!*matches) *matches=cJSON_CreateArray();
               cJSON * tmp=cJSON_CreateObject();
               cJSON_AddStringToObject(tmp, "fullpath", cJSON_get_key(pos, "fullpath"));
               cJSON_AddStringToObject(tmp, "filename", cJSON_get_key(pos, "filename"));
               cJSON_AddStringToObject(tmp, "endpoint", endpoint);
               cJSON_AddItemToArray(*matches, tmp);
               found=1;
				// no break, loop all basearm's on rpm

               if (strcmp(type,"rpm")!=0 )break;
            } // found in Full
            else if ( \
                (  type && endpoint_type && strcmp(type,endpoint_type)==0 ) && \
                ( ( ! distribution && endpoint_distribution && strcmp("nodist",endpoint_distribution)==0  ) || \
                 (   distribution && endpoint_distribution && strcmp(distribution,endpoint_distribution)==0 ) ) &&\
                 ( (!arch && !release) ||  (arch && strcmp(arch,"noarch")==0 ))
                )
            { // wild card on arch or release (rpm or deb)
               if (!wildcard_found_1 && !wildcard_found_2 && !wildcard_found_3)
               {
                  if (!*matches) *matches=cJSON_CreateArray();
                  cJSON * tmp=cJSON_CreateObject();
                  cJSON_AddStringToObject(tmp, "fullpath", cJSON_get_key(pos, "fullpath"));
                  cJSON_AddStringToObject(tmp, "filename", cJSON_get_key(pos, "filename"));
                  char new_sub_endpoint[1024];
                  sprintf(new_sub_endpoint, "/%s/%s",endpoint_type,endpoint_distribution);
                  cJSON_AddStringToObject(tmp, "endpoint", new_sub_endpoint);
                  cJSON_AddItemToArray(*matches, tmp);
                  wildcard_found_1=1;
                  found=1;
               }
               if (wildcard_found_1)
               { //prepare symlinks
                  found=1;
                  if (!*symlinks) *symlinks=cJSON_CreateArray();
                  cJSON * tmp=cJSON_CreateObject();
                  // create
                  char sym_source[256];
                  sprintf(sym_source, "/%s/%s",endpoint_type,endpoint_distribution);
                  cJSON_AddStringToObject(tmp, "source", sym_source );
                  cJSON_AddStringToObject(tmp, "filename", cJSON_get_key(pos, "filename"));
                  cJSON_AddStringToObject(tmp, "endpoint", endpoint);
                  cJSON_AddItemToArray(*symlinks, tmp);
               }
            } // wild card on arch or release (rpm or deb)
            else if ( \
                     (  type && endpoint_type && strcmp(type,endpoint_type)==0 ) && \
                     ( !distribution ||  (distribution && strcmp(distribution,"nodist")==0 )) && \
                     ( ((arch && endpoint_arch && strcmp(arch,endpoint_arch)==0)|| (release && endpoint_release && strcmp(release,endpoint_release)==0 )) && endpoint_arch  && !(arch && strcmp(arch,"noarch")==0)) )
            { // wild card on distribution but not on arch rpm
               if (!wildcard_found_1 && !wildcard_found_2 && !wildcard_found_3)
               {
                  if (!*matches) *matches=cJSON_CreateArray();
                  cJSON * tmp=cJSON_CreateObject();
                  cJSON_AddStringToObject(tmp, "fullpath", cJSON_get_key(pos, "fullpath"));
                  cJSON_AddStringToObject(tmp, "filename", cJSON_get_key(pos, "filename"));
                  char new_sub_endpoint[1024];
                  sprintf(new_sub_endpoint, "/%s",endpoint_type);
                  cJSON_AddStringToObject(tmp, "endpoint", new_sub_endpoint);
                  cJSON_AddItemToArray(*matches, tmp);
                  wildcard_found_2=1;
                  found=1;
               }
               if (wildcard_found_2)
               { //prepare symlinks
                  found=1;
                  if (!*symlinks) *symlinks=cJSON_CreateArray();
                  cJSON * tmp=cJSON_CreateObject();
                  char sym_source[256];
                  sprintf(sym_source, "/%s",endpoint_type);
                  cJSON_AddStringToObject(tmp, "source", sym_source );
                  cJSON_AddStringToObject(tmp, "filename", cJSON_get_key(pos, "filename"));
                  cJSON_AddStringToObject(tmp, "endpoint", endpoint);
                  cJSON_AddItemToArray(*symlinks, tmp);
               }
            } // wild card on distribution but not on arch rpm

            else if ( type && endpoint_type && strcmp(type,endpoint_type)==0 && \
                     ( !distribution ||  (distribution && strcmp(distribution,"nodist")==0 )) && \
                     ( (!arch && !release) ||  (arch && strcmp(arch,"noarch")==0 )) \
                     )
            { // wild card on distro and arch / distro (rpm/deb)
               if (!wildcard_found_1 && !wildcard_found_2 && !wildcard_found_3)
               {
                  if (!*matches) *matches=cJSON_CreateArray();
                  cJSON * tmp=cJSON_CreateObject();
                  cJSON_AddStringToObject(tmp, "fullpath", cJSON_get_key(pos, "fullpath"));
                  cJSON_AddStringToObject(tmp, "filename", cJSON_get_key(pos, "filename"));
                  char new_sub_endpoint[1024];
                  sprintf(new_sub_endpoint, "/%s",endpoint_type);
                  cJSON_AddStringToObject(tmp, "endpoint", new_sub_endpoint);
                  cJSON_AddItemToArray(*matches, tmp);
                  wildcard_found_3=1;
                  found=1;
               }

               if (wildcard_found_3)
               { //prepare symlinks
                  found=1;
                  if (!*symlinks) *symlinks=cJSON_CreateArray();
                  cJSON * tmp=cJSON_CreateObject();
                  char sym_source[256];
                  sprintf(sym_source, "/%s",endpoint_type);
                  cJSON_AddStringToObject(tmp, "source", sym_source );
                  cJSON_AddStringToObject(tmp, "filename", cJSON_get_key(pos, "filename"));
                  cJSON_AddStringToObject(tmp, "endpoint", endpoint);
                  cJSON_AddItemToArray(*symlinks, tmp);
               }
            } // wild card on distro
         }
      }
      if (!found)
      {
         if (!*skips) *skips=cJSON_CreateArray();
         cJSON * tmp=cJSON_CreateObject();
         if (cJSON_get_key(pos, "filename"))cJSON_AddStringToObject(tmp, "filename", cJSON_get_key(pos, "filename"));
         if (type)cJSON_AddStringToObject(tmp, "type", type);
         if (distribution)cJSON_AddStringToObject(tmp, "distribution", distribution);
         if (release) cJSON_AddStringToObject(tmp, "release", release);
         if (arch) cJSON_AddStringToObject(tmp, "arch", arch);
         if (error ) cJSON_AddStringToObject(tmp, "error", error);
         if (srpm) cJSON_AddStringToObject(tmp, "SRPM", "True");
         if (cJSON_get_key(pos, "subpath"))cJSON_AddStringToObject(tmp, "subpath",cJSON_get_key(pos, "subpath") );
         cJSON_AddItemToArray(*skips, tmp);
      }
   }
   return 0;
}

void repo_print_skips(void * opaque,cJSON * skips)
{
   if (!skips) return;
   if (cJSON_GetArraySize(skips)==0) return;
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;

   debug("\nSKIPS\n");
   cJSON * pos=NULL;
   if (cJSON_GetArraySize(skips)==1)
      Write_dyn_trace(trace, none,"\nSKIPPED File:\n");
   else
      Write_dyn_trace(trace, none,"\nSKIPPED Files\n");

      cJSON_ArrayForEach(pos,skips)
      {
         Write_dyn_trace(trace, none,"-%s: ",cJSON_get_key(pos, "filename"));
         if (cJSON_get_key(pos, "error"))Write_dyn_trace(trace, none,"%s ",cJSON_get_key(pos, "error"));
         if (cJSON_get_key(pos, "subpath"))Write_dyn_trace(trace, none,"(%s) ",cJSON_get_key(pos, "subpath"));
         if (cJSON_get_key(pos, "distribution")) Write_dyn_trace(trace, none,"[%s",cJSON_get_key(pos, "distribution"));
         if (!cJSON_get_key(pos, "arch") && !cJSON_get_key(pos, "release")) Write_dyn_trace(trace, none,"]");
         if (cJSON_get_key(pos, "arch")) Write_dyn_trace(trace, none,"/%s]",cJSON_get_key(pos, "arch"));
         if (cJSON_get_key(pos, "release"))Write_dyn_trace(trace, none,"/%s]",cJSON_get_key(pos, "release"));
      }

   Write_dyn_trace(trace, none,"\nFiles are skipped when the rpm data does not match the repo-config");
   return;
}

void repo_copy_matches(void * opaque,cJSON * matches,char* basepath)
{
   if (!matches || !basepath) return;
   if (cJSON_GetArraySize(matches)==0) return;

   debug("\nSymlinks\n");

   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;
   char * newarg[8];
   char var_arg1_string[1024]; // temporary string

   void * saved_args=((data_exchange_t *)opaque)->paramlist;
   ((data_exchange_t *)opaque)->paramlist=(char **)newarg;
   ((data_exchange_t *)opaque)->needenvp=0;
   int error=0;
   cJSON * pos=NULL;
   cJSON_ArrayForEach(pos,matches)
   {
      newarg[0]="/bin/cp";
      newarg[1]="-f";
      newarg[2]=cJSON_get_key(pos, "fullpath");
      newarg[3]=(char *)var_arg1_string;
      sprintf((char *)newarg[3],"%s/public%s/", basepath,cJSON_get_key(pos, "endpoint"));
      newarg[4]=NULL;


      Write_dyn_trace_pad(trace, none,39,"+ cp %s", cJSON_get_key(pos, "filename"));
      Write_dyn_trace_pad(trace, none,36," > %s ", cJSON_get_key(pos, "endpoint"));

      debug("cmd: %s %s %s %s\n",newarg[0],newarg[1],newarg[2],newarg[3]);
      error=cmd_exec(opaque);
      if (error)
      {
         debug("Failed copy %s \n",cJSON_get_key(pos, "filename"));
         Write_dyn_trace(trace, red,"\n ERR: Failed copy\n");
         break;
      }
      else
         Write_dyn_trace(trace, green,"[OK]\n");
   }
   //Restore arguments
   ((data_exchange_t *)opaque)->paramlist=saved_args;
}



void repo_create_symlinks(void * opaque,cJSON * symlinks,char* basepath)
{
   if (!symlinks || !basepath) return;
   if (cJSON_GetArraySize(symlinks)==0) return;

   debug("\nSymlinks\n");

   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;
   char * newarg[8];
   char var_arg1_string[1024]; // temporary string
   char var_arg2_string[1024]; // temporary string
   void * saved_args=((data_exchange_t *)opaque)->paramlist;
   ((data_exchange_t *)opaque)->paramlist=(char **)newarg;
   ((data_exchange_t *)opaque)->needenvp=0;
   int error=0;
   cJSON * pos=NULL;
   cJSON_ArrayForEach(pos,symlinks)
   {
      newarg[0]="/bin/ln";
      newarg[1]="-fs";
      newarg[2]=(char *)var_arg1_string;
      newarg[3]=(char *)var_arg2_string;
      sprintf((char *)newarg[2],"%s/public%s/%s", basepath,cJSON_get_key(pos, "source"),cJSON_get_key(pos, "filename"));
      sprintf((char *)newarg[3],"%s/public%s/%s", basepath,cJSON_get_key(pos, "endpoint"),cJSON_get_key(pos, "filename"));
      newarg[4]=NULL;

     Write_dyn_trace_pad(trace, none,39,"+ ln %s", cJSON_get_key(pos, "filename"));
     Write_dyn_trace_pad(trace, none,36," > %s ", cJSON_get_key(pos, "endpoint"));

      debug("cmd: %s %s %s %s\n",newarg[0],newarg[1],newarg[2],newarg[3]);
      error=cmd_exec(opaque);
      if (error)
      {
         debug("Failed symlink %s \n",cJSON_get_key(pos, "filename"));
         Write_dyn_trace(trace, red,"\n ERR: Failed Symlink\n");
         break;
      }
      else
         Write_dyn_trace(trace, green,"[OK]\n");
   }
   //Restore arguments
   ((data_exchange_t *)opaque)->paramlist=saved_args;
}

// escape space, caller responsible for freeing

char * escape_space(const char * in)
{
	if (!in) return NULL;

	struct MemoryStruct mem;
	init_dynamicbuf( &mem);
	long pos_s=0;
	char * pos_space=NULL;
	while ((pos_space=strchr(in+pos_s, ' '))) // change ' ' to "\ "
	{
		Writedynamicbuf_n((void*)in+pos_s, (size_t)(pos_space-(in+pos_s)), &mem);
		Writedynamicbuf("\\ ",&mem);
		pos_s=pos_space-in+1;
	}
	Writedynamicbuf((void*)in+pos_s, &mem);
	return mem.memory;
}

cJSON * create_repo_dirs(void * opaque, char * repo_PATH , cJSON* repos)
{
   cJSON * endpoints=NULL;
   cJSON * trace_end= cJSON_CreateArray();

   char * temp=extract_dir_structure(repos,&endpoints,trace_end);
   cJSON_Delete(trace_end);
   if (!temp)
   {
	   if (endpoints) cJSON_Delete(endpoints);
       return NULL;
   }

   // feedback buffer
   int exitcode=0;
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;
   // env json
   char * newarg[4];
   // no need to set environment for individual commands
   ((data_exchange_t *)opaque)->needenvp=0;
   void * saved_args=((data_exchange_t *)opaque)->paramlist;
   ((data_exchange_t *)opaque)->paramlist=(char **)newarg;
   char var_arg1_string[1024];
   char * sub = escape_space(temp);
   sprintf(var_arg1_string, "mkdir -pv %s/public/%s",repo_PATH,sub);
	if (sub) free(sub);
	free(temp);
   newarg[0]="/bin/sh";
   newarg[1]="-c";
   newarg[2]=var_arg1_string;
   newarg[3]=NULL;
   debug("create repodir's\n%s\n\n",newarg[2]);
   Write_dyn_trace_pad(trace, none,75,"+ mkdir repos");
   exitcode=cmd_exec(opaque);
   if (exitcode)
     Write_dyn_trace(trace, red,"[FAILED]\n");
   else
     Write_dyn_trace(trace, green,"[OK]\n");
   if (exitcode)
      error("\nERR: creating dir structure\n");
   else
      debug ("created repo dir structure\n");

   ((data_exchange_t *)opaque)->paramlist=saved_args;
   return endpoints;
}

int init_repo_dirs(void * opaque, char * basepath, cJSON* endpoints)
{
	// feedback buffer
   int exitcode=0;
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;
	// env json
	char * newarg[4];
	// no need to set environment for individual commands
	((data_exchange_t *)opaque)->needenvp=0;
	void * saved_args=((data_exchange_t *)opaque)->paramlist;
	((data_exchange_t *)opaque)->paramlist=(char **)newarg;
	char var_arg1_string[1024];

	newarg[0]="/bin/sh";
	newarg[1]="-c";
	newarg[2]=var_arg1_string;
	newarg[3]=NULL;
	int count=0;
	cJSON * pos=NULL;
	cJSON_ArrayForEach(pos,endpoints)
	{
    if ((count%5)==0) update_details(trace);
		debug("create repo: %s\n",pos->child->string);
    int n_len=(int)strlen(pos->child->string);
    char padn[]="                                             ";
    if (n_len <= 32) padn[32-n_len]=0; else padn[0]=0;

    Write_dyn_trace_pad(trace, none,75,"+ create repo: %s",pos->child->string);

		if (strcmp(pos->child->child->valuestring,"rpm")==0)
		{
			char * sub = escape_space(pos->child->string);
			sprintf(var_arg1_string, "/bin/createrepo --quiet --checkts %s/public%s",basepath,sub);
			if (sub) free(sub);
		}
		else if (strcmp(pos->child->child->valuestring,"deb")==0)
			sprintf(var_arg1_string, "cd %s/public%s && /bin/dpkg-scanpackages -m . | gzip -c > Packages.gz ",basepath,pos->child->string);
		else {
			Write_dyn_trace(trace, red,"\t unknow repo type");
         exitcode=1;
		}
		if (!exitcode)exitcode=cmd_exec(opaque);
		if (exitcode)
		{
			Write_dyn_trace(trace, red,"\t[Failed]\n");
			break;
		}
		else if (strcmp(pos->child->child->valuestring,"rpm")==0) Write_dyn_trace(trace, green,"[OK]\n");
	}

	if (exitcode)
		error("\nERR: creating repos\n");
	else
		debug ("created repos\n");

	((data_exchange_t *)opaque)->paramlist=saved_args;

	return exitcode;
}



int update_repo_dirs(void * opaque, char * basepath, cJSON* endpoints)
{
   // feedback buffer
   int exitcode=0;
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;
   // env json
   char * newarg[8];
   // no need to set environment for individual commands
   ((data_exchange_t *)opaque)->needenvp=0;
   void * saved_args=((data_exchange_t *)opaque)->paramlist;
   ((data_exchange_t *)opaque)->paramlist=(char **)newarg;
   char var_arg1_string[1024];

   newarg[0]="/bin/sh";
   newarg[1]="-c";
   newarg[2]=var_arg1_string;
   newarg[3]=NULL;
   int count=0;
   cJSON * pos=NULL;
   cJSON_ArrayForEach(pos,endpoints)
   {
     if ((count%5)==0) update_details(trace);
      debug("update repo: %s\n",pos->child->string);

      Write_dyn_trace_pad(trace, none,75,"+ update repo: %s",pos->child->string);
      if (strcmp(pos->child->child->valuestring,"rpm")==0)
	  {

		  char * sub = escape_space(pos->child->string);
		  sprintf(var_arg1_string, "/bin/createrepo --update --quiet --checkts %s/public%s",basepath,sub);
		  if (sub) free(sub);
	  }
	  else if (strcmp(pos->child->child->valuestring,"deb")==0)
         sprintf(var_arg1_string, "cd %s/public%s && /bin/dpkg-scanpackages -m . | gzip -c > Packages.gz ",basepath,pos->child->string);
      else {
        Write_dyn_trace(trace, magenta,"unknow repo type\n");
         exitcode=1;
      }
      if (!exitcode)exitcode=cmd_exec(opaque);
      if (exitcode)
      {
         Write_dyn_trace(trace, red,"[Failed]\n");
         break;
      }
      else if (strcmp(pos->child->child->valuestring,"rpm")==0) Write_dyn_trace(trace, green,"[OK]\n");
   }

   if (exitcode)
      error("\nERR: updating repos\n");
   else
      debug ("updated repos\n");

   ((data_exchange_t *)opaque)->paramlist=saved_args;

   return 0;
}


/*------------------------------------------------------------------------
* substitude script vars for repo setup
* returns 0 on success
*------------------------------------------------------------------------*/
int copy_script_sh(void * opaque, const char * source, const char * destination, char * repo_name, char * repo_url, int PGP_repo, int PGP_pack)
{
   if (!source || !destination) return 1;
   // feedback buffer
   int exitcode=0;
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;
  // env json
   const char * newarg[8];
   // no need to set environment for individual commands
   ((data_exchange_t *)opaque)->needenvp=0;
   void * saved_args=((data_exchange_t *)opaque)->paramlist;
   ((data_exchange_t *)opaque)->paramlist=(char **)newarg;

   debug("Read script: %s\n",source);
   FILE *f = fopen(source, "r");
   if (f == NULL)
   {
      debug("process_read_file: %s\n",strerror(errno));
      return 1;
   }
   struct MemoryStruct mem;
   init_dynamicbuf(&mem);
   char buf[1024];
   bzero(buf, 1024);
   while( fgets( buf, 1024, f ) != NULL )
   {
      Writedynamicbuf(buf, &mem );
      bzero(buf, 1024);
   }
   fclose(f);
   //fprintf(stderr,"readfile: >\n%s\n",mem.memory);

   // process file
   // repo_url=@http://repo.url@
   // repo_name=@repo_name@

   char * tmp=substitute_repo_script(mem.memory, repo_name,repo_url,PGP_repo,0);

   if (tmp)
   {
       // writefile to destination
       newarg[0]="write file";
       newarg[1]=destination;
       newarg[2]=tmp;
       newarg[3]=NULL;
       exitcode=cmd_write(opaque);
       if (!exitcode)
       {
          debug("wrote  %s \n",newarg[1]);
          Write_dyn_trace(trace, green,"[OK]\n");
       }
       else Write_dyn_trace(trace, red,"[FAILED]\n");
   }
   else
  {
      Write_dyn_trace(trace, red,"[FAILED]\n");
      exitcode=1;
   }
   if (mem.memory) free(mem.memory);
   ((data_exchange_t *)opaque)->paramlist=saved_args;
   return exitcode;
}
