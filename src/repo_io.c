//
//  repo_io.c
//  deployctl
//
//  Created by Danny Goossen on 29/6/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#include "deployd.h"



/*------------------------------------------------------------------------
 * Read a JSON file with dirpath/filename
 * returns NULL on errors
 * returns a cJSON object, caller is responsible for freeing it!
 *------------------------------------------------------------------------*/
cJSON * read_json(void * opaque, const char * base_path,const char * sub_path,const char * filename)
{
   // debug
   //printf("%s\n",dirpath);
   if (!base_path) return NULL;
   if (!sub_path) return NULL;
   if (!filename || strlen(filename)==0) return NULL;
   char path[1024];
   sprintf(path,"%s%s/%s",base_path,sub_path,filename);

   FILE *f = fopen(path, "r");
   if (f == NULL)
   {
      debug("ERR: read json file %s:%s\n",path,strerror(errno));
      return NULL;
   }
   struct MemoryStruct mem;
   init_dynamicbuf(&mem);
   char buf[1024];
   bzero(buf, 1024);
   while( fgets( buf, 1024, f ) != NULL )
   {
      Writedynamicbuf(buf, &mem );
      bzero(buf, 1024);
   }
   fclose(f);
   cJSON * result=cJSON_Parse(mem.memory);
   free(mem.memory);
   return result;
}



cJSON * JSON_dir_list_x( const char *path,const char *subpath,int recursive, const char * extension)
{
   DIR *dp=NULL;
   struct dirent *ep;
   char dirpath[1024];
   char filepath[1024];
   char thissubpath[1024];
   if(subpath)
   {
      sprintf(dirpath, "%s/%s",path,subpath);

   }
   else
   {
      sprintf(dirpath, "%s",path);

   }
   dp = opendir (dirpath);
   cJSON * filearray=NULL;;
   cJSON * tmp=NULL;
   if (dp != NULL)
   {
      while ((ep = readdir (dp)))
      {
         if (ep->d_name[0]!='.' && ep->d_type!=DT_DIR && (strstr(ep->d_name, extension)))
         {
            tmp= cJSON_CreateObject();
            cJSON_AddStringToObject(tmp, "filename", ep->d_name);
            cJSON_AddStringToObject(tmp, "path", dirpath);
            sprintf(filepath, "%s/%s",dirpath,ep->d_name);
            cJSON_AddStringToObject(tmp, "filepath",filepath);
            if (recursive)
            {
               cJSON_AddStringToObject(tmp, "subpath",subpath);
            }
            if (!filearray) filearray=cJSON_CreateArray();
            cJSON_AddItemToArray(filearray, tmp);
         } else  if (ep->d_name[0]!='.' && ep->d_type==DT_DIR && recursive )
         {
            sprintf(thissubpath, "%s/%s",subpath,ep->d_name);
            cJSON * temp=JSON_dir_list_x( path,thissubpath,recursive, extension);
            // now merge temp into filearray
            if (temp)
            {
               cJSON * pos=NULL;
               cJSON_ArrayForEach(pos,temp)
               {
                  if (!filearray) filearray=cJSON_CreateArray();
                  cJSON_AddItemToArray(filearray, cJSON_Duplicate(pos, 1));
               }
               cJSON_Delete(temp);
               pos=NULL;
               temp=NULL;
            }
         }
      }
      (void) closedir (dp);
   }
   else
   {
      debug("Err: Directory %s JSON parser %s", dirpath, strerror(errno));
      return NULL;
   }
   return filearray;
}
