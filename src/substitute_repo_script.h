//
//  substitute_repo_script.h
//  deployctl
//
//  Created by Danny Goossen on 2/7/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#ifndef __deployctl__substitute_repo_script__
#define __deployctl__substitute_repo_script__

char * substitute_repo_script(char * in, char * repo_name, char * repo_url, int PGP_repo, int PGP_pack);
#endif /* defined(__deployctl__substitute_repo_script__) */
