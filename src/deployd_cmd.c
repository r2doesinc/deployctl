/*
 deployd_cmd.c
 Created by Danny Goossen, Gioxa Ltd on 6/2/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

#include "deployd.h"


int cmd_tree(void * opaque)
{
      int exitcode=0;
      struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;
      char command[4096]; // temporary string

      cJSON * env_json=((data_exchange_t *)opaque)->env_json;
      char * dir=cJSON_get_key(env_json, "CI_PROJECT_DIR");

      ((data_exchange_t *)opaque)->needenvp=0;

         int snres=snprintf((char *)command,4096,"cd %s && tree -I '.git' -hCqFax",dir);
         if (snres>=4096)
         {
            Write_dyn_trace(trace, red,"ERROR: command exeeds max length\n");
            exitcode=1;
            return(exitcode);
         }
         Write_dyn_trace(trace, none,"+ %s \n",command);
         update_details(trace);
         debug("cmd: %s\n",command);
         ((data_exchange_t *)opaque)->shellcommand=command;
         exitcode=exec_color(opaque);
         if (exitcode) {debug("ERROR: ls -la\n");}
         // check if certs

      return(exitcode);
}

int cmd_color_test(void * opaque)
{
   int exitcode=0;
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;
   char command[4096]; // temporary string
   
   
   ((data_exchange_t *)opaque)->needenvp=0;
   // export TERM=xterm-256color && source ~/.bash_profile &&
   //int snres=snprintf((char *)command,4096,"cd %s && tree -hCqcFx",dir);
   int snres=snprintf((char *)command,4096,"colortest.py");
   if (snres>=4096)
   {
      Write_dyn_trace(trace, red,"ERROR: command exeeds max length\n");
      exitcode=1;
      return(exitcode);
   }
   Write_dyn_trace(trace, none,"+ %s \n",command);
   update_details(trace);
   debug("cmd: %s\n",command);
   ((data_exchange_t *)opaque)->shellcommand=command;
   exitcode=exec_color(opaque);
   if (exitcode) {debug("ERROR: ls -la\n");}
  
   return(exitcode);
}

int cmd_env(void * opaque)
{
      int exitcode=0;
      struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;
      char command[4096]; // temporary string

      //cJSON * env_json=((data_exchange_t *)opaque)->env_json;

      ((data_exchange_t *)opaque)->needenvp=1;

         int snres=snprintf((char *)command,4096,"env");
         if (snres>=4096)
         {
            Write_dyn_trace(trace, red,"ERROR: command exeeds max length\n");
            exitcode=1;
            return(exitcode);
         }
         Write_dyn_trace(trace, none,"+ %s \n",command);
         update_details(trace);
         debug("cmd: %s\n",command);
         ((data_exchange_t *)opaque)->shellcommand=command;
         exitcode=exec_color(opaque);
         if (exitcode) {debug("ERROR: ls -la\n");}
         // check if certs
         update_details(trace);
      return(exitcode);
}
/*------------------------------------------------------------------------
 * Internal Command : Delete : Delete an environment
 *------------------------------------------------------------------------*/
int cmd_delete(void * opaque)
{
   // feedback buffer
   int exitcode=0;
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;
    cJSON * env_json=((data_exchange_t *)opaque)->env_json;
   parameter_t * param=((data_exchange_t *)opaque)->parameters;
    //  just to make sure we're at the right position
    char * newarg[8];
    // no need to set environment for individual commands
    ((data_exchange_t *)opaque)->needenvp=0;
    ((data_exchange_t *)opaque)->paramlist=(char **)newarg;
     char basePATH[1024];
   debug("checking environment\n");
   //   if (cJSON_GetObjectItem(env_json, "CI_PROJECT_PATH_SLUG") && cJSON_GetObjectItem(env_json, "CI_ENVIRONMENT_SLUG")&& cJSON_GetObjectItem(env_json, "CI_PROJECT_DIR") && cJSON_GetObjectItem(env_json, "CI_ENVIRONMENT_NAME") && cJSON_GetObjectItem(env_json, "CI_COMMIT_SHA"))
      int cnt=0;
   if ((cnt=check_presence(env_json,
                     (char *[])
                     {"CI_PROJECT_PATH_SLUG","CI_ENVIRONMENT_SLUG","CI_ENVIRONMENT_NAME","CI_PROJECT_PATH","CI_PROJECT_DIR","CI_COMMIT_SHA","CI_PROJECT_URL",NULL}
                     , trace)))
   {
      Write_dyn_trace(trace, red,"\nERROR: %d Environment variable(s) Missing\n",cnt);
      debug("\nERROR: %d Environment variable(s) Missing!!!\n",cnt);
      exitcode=1;
   }
   else
   {
      // inform deployctl of environment deployment
      debug("Deploy environment :%s\n",cJSON_GetObjectItem(env_json, "CI_ENVIRONMENT_NAME")->valuestring);
      //Write_dyn_trace(trace, none,"Deploy environment :%s\n",cJSON_GetObjectItem(env_json, "CI_ENVIRONMENT_NAME")->valuestring);
      if (validate_key(env_json, "CI_ENVIRONMENT_NAME", "production")==0)
      {
         if (! cJSON_GetObjectItem(env_json, "DEPLOY_DOMAIN"))
         { // we got no deploydomain, so make one
            // as per : $CI_ENVIRONMENT_SLUG.$CI_PROJECT_PATH_SLUG.$DEPLOY_DOMAIN_APP
            debug("PRODUCTION deployment, no deploydomain \n" );
            if(validate_project_path_slug(env_json, trace,1)!=0)
            {
               Write_dyn_trace(trace, red,"Environment not OK!! \n ** \"DEPLOY_DOMAIN\" and/or \"DEPLOY_APP_DOMAIN\" not defined ** \n");
               exitcode=1;
               return exitcode;
            }
            else
            {
               debug("assemble domain\n");
               char * env_slug=cJSON_get_key(env_json, "CI_ENVIRONMENT_SLUG");
               char * project_path_slug=cJSON_get_key(env_json, "CI_PROJECT_PATH_SLUG");
               char * deploy_app_domain=cJSON_get_key(env_json, "DEPLOY_DOMAIN_APP");
                  debug("parts%s.%s.%s\n",env_slug,project_path_slug,deploy_app_domain);

               if ((strlen(env_slug)+strlen(project_path_slug)+strlen(deploy_app_domain)+3) > 255)
               {
                  debug("ENVIRONMENT/PROJECTPATH/DEPLOYAPP to long\n");
                  Write_dyn_trace(trace, red,"ERROR: ENVIRONMENT/PROJECTPATH/DEPLOYAPP to long\n");
                  exitcode=1;
                  return exitcode;
               }

               char deploy_domain[256];
               sprintf(deploy_domain, "%s.%s.%s",env_slug,project_path_slug,deploy_app_domain);
               debug("new domain %s\n",deploy_domain);
               cJSON_AddItemToObject(env_json, "DEPLOY_DOMAIN", cJSON_CreateString(deploy_domain));
            }
         }
         sprintf((char *)basePATH,"%s/deploy/domain/%s",param->prefix,cJSON_get_key(env_json, "DEPLOY_DOMAIN"));
         debug("basePATH=%s\n",basePATH);
      }
      else
      {
         debug("NOT a production deployment\n");
         if (! cJSON_GetObjectItem(env_json, "DEPLOY_DOMAIN_APP"))
         {
            debug("DEPLOY_DOMAIN_APP not found\n");
            Write_dyn_trace(trace, none,"ERROR: Environment not OK, $DEPLOY_APP_DOMAIN not defined\n");
            exitcode=1;
            return exitcode;
         }
         if(validate_project_path_slug(env_json, trace,1)!=0)
         {
            Write_dyn_trace(trace, red,"ERR: Environment not OK!! \n");
            exitcode=1;
            return exitcode;
         }
            sprintf((char *)basePATH,"%s/deploy/sites/%s/%s",param->prefix,cJSON_get_key(env_json, "CI_PROJECT_PATH_SLUG"),cJSON_get_key(env_json, "CI_ENVIRONMENT_SLUG") );
      }
        //rm
      if (check_namespace(opaque, basePATH)!=0)
      {
         Write_dyn_trace(trace, red,"\n ERROR: Namespace belows to another Project!! \n");
         exitcode=1;
         return exitcode;
      }

        newarg[0]="/bin/rm";
        newarg[1]="-rf";
        newarg[2]=basePATH;
        newarg[3]=NULL;
        Write_dyn_trace(trace, none,"+ rm deployment \n");
        debug("cmd: %s %s %s \n",newarg[0],newarg[1],newarg[2]);
        exitcode=cmd_exec(opaque);
        if (exitcode) {debug("Failed exec rmdir %s\n",newarg[2]);return exitcode;}
    }
   // TODO
   return exitcode;
}

/*------------------------------------------------------------------------
 * Internal Command : config https, configures https (letsencrypt)
 *------------------------------------------------------------------------*/
int cmd_conf_https(void * opaque)
{
   // feedback buffer
   int exitcode=0;
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;
    // env json
    cJSON * env_json=((data_exchange_t *)opaque)->env_json;
   parameter_t * param=((data_exchange_t *)opaque)->parameters;
    char * newarg[11];
    // no need to set environment for individual commands
    ((data_exchange_t *)opaque)->needenvp=0;
    ((data_exchange_t *)opaque)->paramlist=(char **)newarg;
   char basePATH[1024];
   char baseHREF[1024];
    struct stat st;
    debug("checking environment\n");
   int cnt=0;
   if ((cnt=check_presence(env_json,
                     (char *[])
      {"CI_PROJECT_PATH_SLUG","CI_ENVIRONMENT_SLUG","CI_ENVIRONMENT_NAME","CI_PROJECT_PATH","GITLAB_USER_EMAIL",NULL}
                     , trace)))
   {
      Write_dyn_trace(trace, none,"ERROR: %d Environment variable(s) Missing\n",cnt);
      debug("\nERROR: %d Environment variable(s) Missing!!!\n",cnt);
      exitcode=1;
   }
   else
   {
        // inform deployctl of environment deployment
        debug("Deploy environment :%s\n",cJSON_get_key(env_json, "CI_ENVIRONMENT_NAME"));
        //Write_dyn_trace(trace, none,"Deploy environment :%s\n",cJSON_get_key(env_json, "CI_ENVIRONMENT_NAME"));
        debug("check CI_ENV\n");
      if (validate_key(env_json, "CI_ENVIRONMENT_NAME", "production")==0)
        {
            debug("PRODUCTION deployment=> OK\n");
            if (! cJSON_GetObjectItem(env_json, "DEPLOY_DOMAIN"))
            { // we got no deploydomain, so make one
            // as per : $CI_ENVIRONMENT_SLUG.$CI_PROJECT_PATH_SLUG.$DEPLOY_DOMAIN_APP
            if(validate_project_path_slug(env_json, trace,1)!=0)
            {
               Write_dyn_trace(trace, red,"Environment not OK!! \n ** \"DEPLOY_DOMAIN\" and/or \"DEPLOY_APP_DOMAIN\" not defined ** \n");
               exitcode=1;
               return exitcode;
            }
            else
            {
               char * env_slug=cJSON_get_key(env_json, "CI_ENVIRONMENT_SLUG");
                char * project_path_slug=cJSON_get_key(env_json, "CI_PROJECT_PATH_SLUG");
               char * deploy_app_domain=cJSON_get_key(env_json, "DEPLOY_DOMAIN_APP");
               char * deploy_domain=calloc(1,strlen(env_slug)+strlen(project_path_slug)+strlen(deploy_app_domain)+3);
               sprintf(deploy_domain, "%s.%s.%s",env_slug,project_path_slug,deploy_app_domain);
               cJSON_AddItemToObject(env_json, "DEPLOY_DOMAIN", cJSON_CreateString(deploy_domain));
            }
            }
         sprintf(baseHREF,"http://%s",cJSON_get_key(env_json, "DEPLOY_DOMAIN"));
         sprintf((char *)basePATH,"%s/deploy/domain/%s",param->prefix,cJSON_get_key(env_json, "DEPLOY_DOMAIN"));
        }
        else
        {
                debug("config-https only for production environment\n");
                Write_dyn_trace(trace, red,"\n ERROR: config-https only for production environment\n");
                exitcode=1;
                return exitcode;
        }
        // check domaindir
        if(stat(basePATH,&st) != 0)
        {
         // deploydir not present no point to continue
            debug("ERROR: Deploy dir %s not present\n",basePATH);
            Write_dyn_trace(trace, red,"\n ERROR: no deployment present\n");
            exitcode=1;
            return exitcode;
        }
      //Write_dyn_trace(trace, none,"Deploy environment :%s\n",cJSON_get_key(env_json, "CI_ENVIRONMENT_NAME"));
      // now check if dir exist, if so check the "CI_PROJECT_URL" !!!
      if (check_namespace(opaque, basePATH)!=0)
      {
         Write_dyn_trace(trace, red,"\n ERROR: Namespace belows to another Project!! \n");
         exitcode=1;
         return exitcode;
      }
      // check if dns points to this instance for this url
      exitcode= url_check(opaque, basePATH,baseHREF);
      if (exitcode)
      {
         debug("ERR: url check\n");
         return exitcode;
      }
      update_details(trace);
      debug("start letsencript\n");
        // check if letsencript config exists, if not create new cert
        exitcode=letsencrypt(opaque,cJSON_get_key(env_json, "DEPLOY_DOMAIN"),cJSON_get_key(env_json, "GITLAB_USER_EMAIL"));
      // write https
      if (!exitcode)
      {
         exitcode= write_http_config(opaque,1,basePATH,cJSON_get_key(env_json, "DEPLOY_DOMAIN"));
         if (exitcode){debug("Failed create http config\n");return exitcode;}
         exitcode=  verify_http_config(opaque);
         if (exitcode)
         {
            debug("Failed nginx -t \n");
            delete_http_config(opaque,basePATH);

            return exitcode;
         }
         else exitcode=reload_http_config(opaque);
      }
      else debug("ERR: let's encrypt failed\n");
       update_details(trace);
    }
    return exitcode;
}

/*------------------------------------------------------------------------
 * Internal Command : static : filter the env and rsync project/public to public
 *------------------------------------------------------------------------*/
int cmd_static(void * opaque)
{
   // feedback buffer
   int exitcode=0;
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;
   // env json
   cJSON * env_json=((data_exchange_t *)opaque)->env_json;
    char * newarg[8];
    // no need to set environment for individual commands
    ((data_exchange_t *)opaque)->needenvp=0;
   ((data_exchange_t *)opaque)->paramlist=(char **)newarg;
   parameter_t * param=((data_exchange_t *)opaque)->parameters;
    char var_arg1_string[1024];
   // char * var_dest_dir_string[1024];
   char basePATH[1024];
   char baseHREF[1024];
   char pubPATH[1024];
    int deploy_production=0;
  // setdebug();
   debug("checking environment\n");
   int cnt=0;
   if ((cnt=check_presence(env_json,
                     (char *[])
                     {"CI_PROJECT_PATH_SLUG","CI_ENVIRONMENT_SLUG","CI_ENVIRONMENT_NAME","CI_PROJECT_PATH","CI_PROJECT_DIR","CI_COMMIT_SHA","CI_PROJECT_URL",NULL}
                     , trace)))
   {
      Write_dyn_trace(trace, red,"ERROR: %d Environment variable(s) Missing\n",cnt);
      debug("\nERROR: %d Environment variable(s) Missing!!!\n",cnt);
      exitcode=1;
   }
   else
   {
      // inform deployctl of environment deployment
      debug("Deploy environment :%s\n",cJSON_get_key(env_json,  "CI_ENVIRONMENT_NAME"));

      debug("wrote env\n");
      if (validate_key(env_json, "CI_ENVIRONMENT_NAME", "production")==0)
      {
        debug("validate_key\n");
         if (! cJSON_GetObjectItem(env_json, "DEPLOY_DOMAIN"))
         { // we got no deploydomain, so make one
            // as per : $CI_ENVIRONMENT_SLUG.$CI_PROJECT_PATH_SLUG.$DEPLOY_DOMAIN_APP
            //debug("PRODUCTION deployment, no deploydomain and slugok=%d \n",(validate_project_path_slug(opaque, trace)!=0) );
            if(validate_project_path_slug(env_json, trace,1)!=0)
            {
               Write_dyn_trace(trace, none,"Environment not OK!! \n ** \"DEPLOY_DOMAIN\" and/or \"DEPLOY_APP_DOMAIN\" not defined ** \n");
               exitcode=1;
               return exitcode;
            }
            else
            {
               debug("assemble domain\n");
               char * env_slug=cJSON_get_key(env_json, "CI_ENVIRONMENT_SLUG");
               char * project_path_slug=cJSON_get_key(env_json, "CI_PROJECT_PATH_SLUG");
               char * deploy_app_domain=cJSON_get_key(env_json, "DEPLOY_DOMAIN_APP");
               debug("parts%s.%s.%s\n",env_slug,project_path_slug,deploy_app_domain);
               // TODO change to one function
               if ((strlen(env_slug)+strlen(project_path_slug)+strlen(deploy_app_domain)+3) > 255)
               {
                  debug("ENVIRONMENT/PROJECTPATH/DEPLOYAPP to long\n");
                  Write_dyn_trace(trace, none,"ERR: ENVIRONMENT/PROJECTPATH/DEPLOYAPP to long\n");
                  exitcode=1;
                  return exitcode;
               }
               // TODO check others, need to be static, no malloc => mem leaks
               char deploy_domain[256];
               sprintf(deploy_domain, "%s.%s.%s",env_slug,project_path_slug,deploy_app_domain);
               debug("new domain %s\n",deploy_domain);
               cJSON_AddItemToObject(env_json, "DEPLOY_DOMAIN", cJSON_CreateString(deploy_domain));
            }
         }
         deploy_production=1;
         sprintf((char *)basePATH,"%s/deploy/domain/%s",param->prefix,cJSON_get_key(env_json, "DEPLOY_DOMAIN"));
         sprintf(baseHREF,"http://%s",cJSON_get_key(env_json, "DEPLOY_DOMAIN"));
      }
      else
      {
         debug("NOT a production deployment\n");
         deploy_production=0;
         if (! cJSON_GetObjectItem(env_json, "DEPLOY_DOMAIN_APP"))
         {
            debug("DEPLOY_DOMAIN_APP not found\n");
            Write_dyn_trace(trace, none,"ERR: Environment not OK, $DEPLOY_APP_DOMAIN not defined\n");
            exitcode=1;
            return exitcode;
         }
         if(validate_project_path_slug(env_json, trace,1)!=0)
         {
            Write_dyn_trace(trace, none,"ERR: Environment not OK!! \n");
            exitcode=1;
            return exitcode;
         }
         debug("assemble domain\n");
         char * env_slug=cJSON_get_key(env_json, "CI_ENVIRONMENT_SLUG");
         char * project_path_slug=cJSON_get_key(env_json, "CI_PROJECT_PATH_SLUG");
         char * deploy_app_domain=cJSON_get_key(env_json, "DEPLOY_DOMAIN_APP");
         debug("parts%s.%s.%s\n",env_slug,project_path_slug,deploy_app_domain);
         debug("paramprefix=%s\n",param->prefix);
         sprintf((char *)basePATH,"%s/deploy/sites/%s/%s",param->prefix,cJSON_get_key(env_json, "CI_PROJECT_PATH_SLUG"),cJSON_get_key(env_json, "CI_ENVIRONMENT_SLUG") );
         sprintf(baseHREF,"http://%s.%s.%s",cJSON_get_key(env_json, "CI_ENVIRONMENT_SLUG"),cJSON_get_key(env_json, "CI_PROJECT_PATH_SLUG"), cJSON_get_key(env_json, "DEPLOY_DOMAIN_APP"));
         char deploy_domain[256];
         sprintf(deploy_domain, "%s.%s.%s",env_slug,project_path_slug,deploy_app_domain);
         debug("new domain %s\n",deploy_domain);
         if (cJSON_get_key(env_json,"DEPLOY_DOMAIN"))
         cJSON_ReplaceItemInObject(env_json, "DEPLOY_DOMAIN", cJSON_CreateString(deploy_domain));
         else
         cJSON_AddItemToObject(env_json, "DEPLOY_DOMAIN", cJSON_CreateString(deploy_domain));
        debug("basepath=%s\n",basePATH);
      }
      // now check if dir exist, if so check the "CI_PROJECT_URL" !!!
      if (check_namespace(opaque, basePATH)!=0)
      {
         Write_dyn_trace(trace, none,"ERR: Namespace belows to another Project!! \n");
         exitcode=1;
         return exitcode;
      }

      // need pubdir
      sprintf(pubPATH,"%s/public",basePATH);
      debug("pubpath=%s, %d\n",pubPATH,exitcode);

      _mkdir(NULL,pubPATH);

        Write_dyn_trace(trace, none,"+ mkdir public \n");
      //if (deploy_production)
      {
         exitcode= write_http_config(opaque,0,basePATH,cJSON_get_key(env_json, "DEPLOY_DOMAIN"));
         if (exitcode){debug("Failed create http config\n");return exitcode;}
         exitcode=  verify_http_config(opaque);
         if (exitcode)
         {
            debug("Failed nginx -t \n");
            delete_http_config(opaque,basePATH);

            return exitcode;
         }
         else exitcode=reload_http_config(opaque);
      }

      // check if dns points to this instance for this url
      if (!exitcode) exitcode= url_check(opaque, basePATH,baseHREF);
      // remove pubdir
      _rmdir(NULL ,pubPATH );
      debug("starting with publish paths, exitcode %d\n",exitcode);
      // replace with our wishes
      if (!exitcode)
      {
         cJSON * dep_path;
         cJSON_ArrayForEach(dep_path, cJSON_GetObjectItem(env_json, "DEPLOY_PUBLISH_PATH"))
         {
            char * source=dep_path->child->valuestring;
            char * target=dep_path->child->string;
            char * dir=NULL;
            split_path_file_2_path(&dir,target);
            _mkdir(dir,pubPATH);
            debug("s=%s, t=%s,tdir=%s\n",source,target,dir);
            if (dir) free(dir);
            if (!exitcode)
            {
              // rsync
               newarg[0]="/bin/sh";
               newarg[1]="-c";
               newarg[2]=(char *)var_arg1_string;
               sprintf((char *)newarg[2],"/usr/bin/rsync -r %s%s %s%s",cJSON_get_key(env_json, "CI_PROJECT_DIR"),source,pubPATH,target);
               newarg[3]=NULL;
               Write_dyn_trace(trace, none,"+ rsync ");
               Write_dyn_trace_pad(trace, cyan,75-8,"%s -> %s", source,target);
               debug("cmd: %s %s %s\n",newarg[0],newarg[1],newarg[2]);
               exitcode=cmd_exec(opaque);
               if (exitcode)
               {
                 debug("Failed exec rsync %s: %s\n",newarg[2],trace);
                 Write_dyn_trace(trace, red,"\n  [FAILED]\n");
                 return exitcode;
             }
           		else Write_dyn_trace(trace, green,"[OK]\n");
            }
         }
      }
      if (!exitcode)
      {
         exitcode=write_namespace(opaque, basePATH);
         if (exitcode) debug("Failed to write_namespace\n");
      }
      if (!exitcode && deploy_production && validate_key(env_json, "DEPLOY_CONFIG_HTTPS","True" )==0)
      {
         //config https

         debug("start letsencript\n");
         // check if letsencript config exists, if not create new cert
         exitcode=letsencrypt(opaque,cJSON_get_key(env_json, "DEPLOY_DOMAIN"),cJSON_get_key(env_json, "GITLAB_USER_EMAIL"));
         // write https
         if (!exitcode)
         {
            exitcode= write_http_config(opaque,1,basePATH,cJSON_get_key(env_json, "DEPLOY_DOMAIN"));
            if (exitcode){debug("Failed create http config\n");return exitcode;}
            exitcode=  verify_http_config(opaque);
            if (exitcode)
            {
               debug("Failed nginx -t \n");
               delete_http_config(opaque,basePATH);

               return exitcode;
            }
            else exitcode=reload_http_config(opaque);
         }
         else debug("ERR: let's encrypt failed\n");
         update_details(trace);
      }
   }
   return exitcode;
}

/*------------------------------------------------------------------------
 * Internal Command : Release : filter the env, create release and rsync
 *------------------------------------------------------------------------*/
int cmd_release(void * opaque)
{
   // feedback buffer
   int exitcode=0;
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;
   // env json
   cJSON * env_json=((data_exchange_t *)opaque)->env_json;
   char * newarg[8];
   // no need to set environment for individual commands
   ((data_exchange_t *)opaque)->needenvp=0;
   ((data_exchange_t *)opaque)->paramlist=(char **)newarg;
   parameter_t * param=((data_exchange_t *)opaque)->parameters;
   char var_arg1_string[1024];
   char var_arg2_string[1024];
   // char * var_dest_dir_string[1024];
   int deploy_production=0;
   debug("checking environment\n");
   char basePATH[1024];
   char baseHREF[1024];
   char tagPATH[1024];
   char tagHREF[1024];
   char subPATH[1024];
   char * build_ref_slug=NULL;
   int cnt=0;

   if ((cnt=check_presence(env_json,
                     (char *[])
      {"CI_PROJECT_PATH_SLUG","CI_ENVIRONMENT_SLUG","CI_ENVIRONMENT_NAME","CI_PROJECT_PATH","CI_PROJECT_DIR","CI_COMMIT_SHA","CI_COMMIT_REF_SLUG","CI_PROJECT_URL","CI_JOB_TOKEN","CI_PROJECT_ID",NULL}
                     , trace)))
   {
      Write_dyn_trace(trace, red,"ERROR: %d Environment variable(s) Missing\n",cnt);
      debug("\nERROR: %d Environment variable(s) Missing!!!\n",cnt);
      exitcode=1;
   }
   else
   {
      // inform deployctl of environment deployment
      debug("Deploy environment :%s\n",cJSON_get_key(env_json, "CI_ENVIRONMENT_NAME"));
      //Write_dyn_trace(trace, none,"Deploy environment :%s\n",cJSON_GetObjectItem(env_json, "CI_ENVIRONMENT_NAME")->valuestring);
      build_ref_slug=cJSON_get_key(env_json, "CI_COMMIT_REF_SLUG");
      if (validate_key(env_json, "CI_ENVIRONMENT_NAME", "production")==0)
      {
         debug("PRODUCTION deployment\n");
            deploy_production=1;
            if (!cJSON_get_key(env_json,"CI_COMMIT_TAG"))
         {
            Write_dyn_trace(trace, none,"Production release without a release TAG not OK!! \n");
            exitcode=1;
            return exitcode;
         }
         if (! cJSON_GetObjectItem(env_json, "DEPLOY_DOMAIN"))
         { // we got no deploydomain, so make one
            // as per : $CI_ENVIRONMENT_SLUG.$CI_PROJECT_PATH_SLUG.$DEPLOY_DOMAIN_APP

            debug("\nNo Deploy demain:\n");
            if(validate_project_path_slug(env_json, trace,1)!=0)
            {
               debug("no valid project path\n");
               Write_dyn_trace(trace, red,"Environment not OK!! \n ** \"DEPLOY_DOMAIN\" and/or \"DEPLOY_APP_DOMAIN\" not defined ** \n");
               exitcode=1;
               return exitcode;
            }
            else
            {
               debug("extracting new domain\n");
               char * env_slug=cJSON_get_key(env_json, "CI_ENVIRONMENT_SLUG");
               char * project_path_slug=cJSON_get_key(env_json, "CI_PROJECT_PATH_SLUG");
               char * deploy_app_domain=cJSON_get_key(env_json, "DEPLOY_DOMAIN_APP");
               char deploy_domain[256];
               sprintf(deploy_domain, "%s.%s.%s",env_slug,project_path_slug,deploy_app_domain);
               debug("new domain %s\n",deploy_domain);
               cJSON_AddItemToObject(env_json, "DEPLOY_DOMAIN", cJSON_CreateString(deploy_domain));
            }
         }
         debug("building paths\n");
            sprintf(baseHREF,"http://%s",cJSON_get_key(env_json, "DEPLOY_DOMAIN"));
            sprintf((char *)tagHREF,"%s/%s",baseHREF,build_ref_slug);
            sprintf((char *)basePATH,"%s/deploy/domain/%s",param->prefix,cJSON_get_key(env_json, "DEPLOY_DOMAIN"));
            sprintf((char *)tagPATH,"%s/public/%s",basePATH,build_ref_slug);
        // **** read release and udate with new release
         sprintf(subPATH,"/%s/files",build_ref_slug);
        }
      else
      {
         debug("NOT a production deployment\n");
         deploy_production=0;
         if (! cJSON_GetObjectItem(env_json, "DEPLOY_DOMAIN_APP"))
      	{
            debug("DEPLOY_DOMAIN_APP not found\n");
            Write_dyn_trace(trace, none,"ERR: Environment not OK, $DEPLOY_APP_DOMAIN not defined\n");
            exitcode=1;
            return exitcode;
      	}
         if(validate_project_path_slug(env_json, trace,1)!=0)
         {
            debug("ERR: Project_Path_slug, Environment not OK!! \n");
            Write_dyn_trace(trace, red,"ERR: Environment not OK!! \n");
            exitcode=1;
            return exitcode;
         }
         debug("assemble domain\n");
         char * env_slug=cJSON_get_key(env_json, "CI_ENVIRONMENT_SLUG");
         char * project_path_slug=cJSON_get_key(env_json, "CI_PROJECT_PATH_SLUG");
         char * deploy_app_domain=cJSON_get_key(env_json, "DEPLOY_DOMAIN_APP");
         debug("parts%s.%s.%s\n",env_slug,project_path_slug,deploy_app_domain);


      	// set the variables for directory and HREF
         sprintf(baseHREF,"http://%s.%s.%s",cJSON_get_key(env_json, "CI_ENVIRONMENT_SLUG"),cJSON_get_key(env_json, "CI_PROJECT_PATH_SLUG"), cJSON_get_key(env_json, "DEPLOY_DOMAIN_APP"));
         sprintf((char *)basePATH,"%s/deploy/sites/%s/%s",param->prefix,cJSON_get_key(env_json, "CI_PROJECT_PATH_SLUG"),cJSON_get_key(env_json, "CI_ENVIRONMENT_SLUG"));
         // non domain deploys have no tags history
         sprintf((char *)tagPATH,"%s/public",basePATH);
         sprintf((char *)tagHREF,"%s",baseHREF);
         sprintf(subPATH,"/files");
         char deploy_domain[256];
         sprintf(deploy_domain, "%s.%s.%s",env_slug,project_path_slug,deploy_app_domain);
         debug("new domain %s\n",deploy_domain);
         if (cJSON_get_key(env_json,"DEPLOY_DOMAIN"))
         cJSON_ReplaceItemInObject(env_json, "DEPLOY_DOMAIN", cJSON_CreateString(deploy_domain));
         else
         cJSON_AddItemToObject(env_json, "DEPLOY_DOMAIN", cJSON_CreateString(deploy_domain));

      }

      debug("checking namespace\n");
      if (check_namespace(opaque, basePATH)!=0)
      {
         Write_dyn_trace(trace, red,"\nERROR: Namespace belongs to another Project!! \n");
         exitcode=1;
         return exitcode;
      }

      _mkdir("/files",tagPATH);


       // on deploy production we create a http config file
      //if (deploy_production)
      {
         exitcode= write_http_config(opaque,0,basePATH,cJSON_get_key(env_json, "DEPLOY_DOMAIN"));
         if (exitcode){debug("Failed create http config\n");return exitcode;}
         exitcode=  verify_http_config(opaque);
         if (exitcode)
         {
            debug("Failed nginx -t \n");
            delete_http_config(opaque,basePATH);

            return exitcode;
         }
         else exitcode=reload_http_config(opaque);
      }
      // check if dns points to this instance for this url
      if (!exitcode) exitcode= url_check(opaque, basePATH,baseHREF);
      if (!exitcode)
      {
         exitcode=write_namespace(opaque, basePATH);
         if (!exitcode) debug("Failed to write_namespace\n");
      }
      _rmdir("/files",tagPATH);
      if (!exitcode)
      {
         cJSON * dep_path;
         cJSON_ArrayForEach(dep_path, cJSON_GetObjectItem(env_json, "DEPLOY_RELEASE_PATH"))
         {
            char * source=dep_path->child->valuestring;
            char * target=dep_path->child->string;
            char * dir=NULL;
            split_path_file_2_path(&dir,target);
            char files_path[1024];
            sprintf(files_path,"%s/files%s",tagPATH,dir);
            _mkdir(NULL,files_path);
            debug("s=%s, t=%s,tdir=%s, >%s<\n",source,target,dir,files_path);
            if (dir) free(dir);
            // rsync
            newarg[0]="/usr/bin/rsync";
            newarg[1]="-r";
            newarg[2]=(char *)var_arg1_string;
            sprintf((char *)newarg[2],"%s%s",cJSON_get_key(env_json, "CI_PROJECT_DIR"),source);
            newarg[3]=(char *)var_arg2_string;
            sprintf((char *)newarg[3],"%s%s",files_path,target);
            newarg[4]=NULL;
            Write_dyn_trace(trace, none,"+ rsync release ");
            Write_dyn_trace_pad(trace, cyan,75-16,"%s -> %s",source,target);
            debug("cmd: %s %s %s %s\n",newarg[0],newarg[1],newarg[2],newarg[3]);
            exitcode=cmd_exec(opaque);
            if (!exitcode)
              Write_dyn_trace(trace, green,"[OK]\n");
            else
              Write_dyn_trace(trace, red,"[FAILED]\n");
            if (exitcode)
               {debug("Failed exec rsync %s\n",newarg[2]);return exitcode;}
         }
   }
   } // environment check ok


   // get gitlab release info
   cJSON * grelease_tag=NULL;
   if (!exitcode && cJSON_GetObjectItem(env_json, "CI_COMMIT_TAG"))
   {
      Write_dyn_trace_pad(trace, none,75,"+ get gitlab tag ...");
       update_details(trace);
      debug("getting release from gitlab and put into html buffer\n");
      if (!get_release(opaque, &grelease_tag))
      {
         char * temp_out=cJSON_get_key(grelease_tag, "message");
         if (temp_out)
         {
            debug("release info ok \n");
            Write_dyn_trace(trace, green,"[OK]\n");
         }
         else
         {
            debug ("no release info\n");
            Write_dyn_trace(trace, bold_magenta,"\nNO release info\n");
			}
      }
      else
      {
         Write_dyn_trace(trace, bold_magenta,"\n API Problem\n");
         debug ("problem getting tag from gitlab\n");
      }
      update_details(trace);
   }
   // create this release=> for all environment
   char filepath[1024];
   char * last_tag=NULL;
   char * release_print=NULL;
   char * release_json_js_print=NULL;
   cJSON * releases=NULL;
   cJSON * this_release=NULL;
   int clean_tag=0;

   if (!exitcode && deploy_production)
   {
      clean_tag=is_clean_tag(opaque,cJSON_get_key(env_json, "CI_COMMIT_TAG"),(const char *[]) {"BETA","TEST","RC","PRC","ALPHA",NULL});
   }
   if (!exitcode && deploy_production && clean_tag)
   {
      last_tag=strdup(cJSON_get_key(env_json, "CI_COMMIT_TAG"));
   } else if (!exitcode && deploy_production)
   {
      last_tag=check_last_tag(opaque,basePATH);
   }

   debug("last_tag: %s\n",last_tag);
   if (!exitcode)
   {
   	 this_release= create_thisrelease_json(  opaque , grelease_tag,  deploy_production, basePATH,subPATH);
   }
   if (!exitcode && this_release)
   {
      release_print = create_releases_json( opaque , this_release,  &releases, deploy_production);
   }
   if (!exitcode && release_print)
   {
   	release_json_js_print= create_release_json_js (opaque , baseHREF, release_print,1 , last_tag); // tagHREF?
   }
   //
   //NOW write it!!! in tagPATH
   if (!exitcode && release_json_js_print)
   {
      // **** read release and udate with new release
      sprintf(filepath,"%s/deployw_json.js",tagPATH);
      newarg[0]="write_file";
      newarg[1]=(char *)filepath;
      newarg[2]=(char *)release_json_js_print;
      newarg[3]=NULL;
         debug("deployw_json.js:==>\n%s\n\n",newarg[2]);
      if (deploy_production)
         Write_dyn_trace_pad(trace, none,75,"+ write /%s/deployw_json.js",build_ref_slug);
      else
         Write_dyn_trace_pad(trace, none,75,"+ write /%s/deployw_json.js",build_ref_slug);
         exitcode=cmd_write(opaque);
         if (!exitcode)
           Write_dyn_trace(trace, green,"[OK]\n");
         else
           Write_dyn_trace(trace, red,"[FAILED]\n");

         if (exitcode)
            error("\nERR: writing deployw_json.js\n");
         else
            debug ("wrote %s\n", newarg[1]);
   }

   // cleanup
   if (releases) { cJSON_Delete(releases);releases=NULL; }
   if (release_print) {free(release_print); release_print=NULL; }
   if (release_json_js_print) {free(release_json_js_print); release_json_js_print=NULL; }
   if (grelease_tag){cJSON_Delete(grelease_tag); grelease_tag=NULL;}
   update_details(trace);
   // ******************************************************************
   // now do the things for production env!, this time save to basePATH
   // ******************************************************************
   if (!exitcode && deploy_production && this_release)
   {
      // **** read release and udate with new release
      sprintf(filepath,"%s/public/release.json",basePATH);
      releases=read_release_json(opaque, filepath);
      release_print=create_releases_json(opaque, this_release, &releases, 1);
      newarg[0]="write_file";
      newarg[1]=(char *)filepath;
      newarg[2]=(char *)release_print;
      newarg[3]=NULL;
      if (newarg[2])
      {
         debug("release.json:==>\n%s\n\n",newarg[2]);
         Write_dyn_trace_pad(trace, none,75,"+ write /release.json");
         exitcode=cmd_write(opaque);
         if (exitcode){
           Write_dyn_trace(trace, red,"[FAIL]\n");
            error("\nERR: writing release.json\n");
        }
        else
        {
          Write_dyn_trace(trace, green,"[OK]\n");
           debug ("wrote %s\n", newarg[1]);
        }
      }
      else
      { Write_dyn_trace(trace, red,"\nERR: preparing release.json \n"); error(" getting print JSONBUFF\n");exitcode=1; }
   }
   update_details(trace);
   if (!exitcode && release_print) release_json_js_print= create_release_json_js(opaque, baseHREF,release_print,0,last_tag);
   if (release_json_js_print)
   { // write it
      sprintf(filepath,"%s/public/deployw_json.js",basePATH);
      newarg[0]="write_file";
      newarg[1]=(char *)filepath;
      newarg[2]=(char *)release_json_js_print;
      newarg[3]=NULL;
      //debug("release.json:==>\n%s\n\n",newarg[2]);
      Write_dyn_trace_pad(trace, none,75,"+ write /deployw_json.js");
      exitcode=cmd_write(opaque);
      if (exitcode){
        Write_dyn_trace(trace, red,"[FAIL]\n");
        error("\nERR: writing deployw_json.js\n");
     }
     else
     {
       Write_dyn_trace(trace, green,"[OK]\n");
        debug ("wrote %s\n", newarg[1]);
     }

      free(release_json_js_print);
   }
   else if (deploy_production)
   {
       Write_dyn_trace(trace, red,"\nERR: preparing deployw_json.js \n");
       error("\nERR: getting data for deployw_json.js\n");
       exitcode=1;
   }
   // And clean it up.
   if (release_print) free(release_print);
   if (last_tag) free(last_tag);
   if(this_release)
   {
      cJSON_Delete(this_release);
      debug ("cJSON_delete releases OK\n");
      this_release=NULL;
   }
   if(releases)
   {
      cJSON_Delete(releases);
      debug ("cJSON_delete releases OK\n");
      releases=NULL;
   }
   update_details(trace);

   // symlink css/js/fonts/bootstrap etc for baseHREF
   // TODO should check if no .LAYOUT before symlinking
   if (!exitcode )
   {
      newarg[0]="/bin/sh";
      newarg[1]="-c";
      newarg[2]=(char *)var_arg1_string;
      sprintf((char *)newarg[2],"/bin/ln -sf %s/deploy/config/html_deploy/* %s/public/",param->prefix,basePATH );
      newarg[3]=NULL;
      Write_dyn_trace_pad(trace, none,75,"+ symlink webapp to /");
      debug("cmd:  %s %s %s \n",newarg[0],newarg[1],newarg[2]);
      exitcode=cmd_exec(opaque);
      if (exitcode){
        Write_dyn_trace(trace, red,"[FAIL]\n");
        debug("Failed create webapp symlink %s",newarg[2]);
     }
     else
     {
       Write_dyn_trace(trace, green,"[OK]\n");
     }
      update_details(trace);
	}
   // symlink index.html for the tag release
   if (!exitcode && deploy_production)
   {
      newarg[0]="/bin/sh";
      newarg[1]="-c";
      newarg[2]=(char *)var_arg1_string;
      sprintf((char *)newarg[2],"/bin/ln -sf %s/deploy/config/html_deploy/index.html %s/",param->prefix,tagPATH );
      newarg[3]=NULL;
      Write_dyn_trace_pad(trace, none,75,"+ symlink index.html to /%s",build_ref_slug);
      debug("cmd:  %s %s %s \n",newarg[0],newarg[1],newarg[2]);
      exitcode=cmd_exec(opaque);
      if (exitcode){
        Write_dyn_trace(trace, red,"[FAIL]\n");
        debug("Failed create symlink %s\n",newarg[2]);
     }
     else
     {
       Write_dyn_trace(trace, green,"[OK]\n");
     }
      update_details(trace);
   }
  // create release badge on a clean_tag ( no beta or RC or test ...)
   if (!exitcode && deploy_production && clean_tag)
   {
		debug("\n create badge\n");
        // Create the svg badge release on top level
		char * svg_out=NULL;
      if (cJSON_GetObjectItem(env_json, "CI_COMMIT_TAG"))
      {
           make_svg_badge("Release", cJSON_get_key(env_json, "CI_COMMIT_TAG"),&svg_out);
      /* NO deploy release without TAG
      else
      {
         char commit_short[10];
         sprintf(commit_short,"%.*s",8,cJSON_get_key(env_json, "CI_COMMIT_SHA"));
          make_svg_badge("Commit", commit_short,&svg_out);
      } */

      }
      if (svg_out)
      {
         newarg[0]="write_file";
         newarg[1]=(char *)var_arg1_string;
         sprintf((char *)newarg[1],"%s/public/tag.svg",basePATH);
         newarg[2]=svg_out;
         newarg[3]=NULL;
         Write_dyn_trace_pad(trace, none,75,"+ write public/tag.svg");

         exitcode=cmd_write(opaque);
         if (!exitcode) { debug("wrote %s, %d bytes\n",newarg[1],strlen(svg_out));}
         if (exitcode){
           Write_dyn_trace(trace, red,"[FAIL]\n");
        }
        else
        {
          Write_dyn_trace(trace, green,"[OK]\n");
        }
         free(svg_out);
         svg_out=NULL;
      }
      else
      {Write_dyn_trace(trace, red,"\n- Failed creating svg release tag\n"); debug("Failed making svg release tag\n"); }
      update_details(trace);
   }
   // symlink latest to this release, only on clean tag
   if (!exitcode && deploy_production && clean_tag )
   {
      // ln -sf tag latest
      newarg[0]="/bin/ln";
      newarg[1]="-sf";
      newarg[2]=(char *)var_arg1_string;
      sprintf((char *)newarg[2],"%s",tagPATH);
      newarg[3]=(char *)var_arg2_string;
      sprintf((char *)newarg[3],"%s/public/latest",basePATH);
      newarg[4]=NULL;
      Write_dyn_trace_pad(trace, none,75,"+ symlink latest");

      int thiserror=0;
      // unlink, only throw error on none existing file
      if (unlink (newarg[3])!=0 && (thiserror=errno)!=ENOENT) sock_error_no("unlink sysmlink latest",thiserror);
      exitcode=cmd_exec(opaque);
      if (exitcode){
        Write_dyn_trace(trace, red,"[FAIL]\n");
        debug("Failed ln -sf %s %s\n",newarg[2],newarg[3]);
        return exitcode;
      }
		else {debug("ln -sf %s %s\n",newarg[2],newarg[3]);Write_dyn_trace(trace, green,"[OK]\n");}
   }
   // write public/latest.tag, contains latest tag info
   if (!exitcode && deploy_production && clean_tag )
   {
      char * tag=cJSON_get_key(env_json, "CI_COMMIT_TAG");
      newarg[0]="write_file";
      newarg[1]=(char *)var_arg1_string;
      sprintf((char *)newarg[1],"%s/public/latest.tag",basePATH);
      newarg[2]= tag;
      newarg[3]=NULL;
      Write_dyn_trace_pad(trace, none,75,"+ write public/latest.tag");

      exitcode=cmd_write(opaque);
      if (!exitcode)
        Write_dyn_trace(trace, green,"[OK]\n");
      else
        Write_dyn_trace(trace, red,"[FAILED]\n");

      if (!exitcode) { debug("wrote %s => %s\n",newarg[1],newarg[2]);}
   }
   update_details(trace);
   if (!exitcode && deploy_production && validate_key(env_json, "DEPLOY_CONFIG_HTTPS","True" )==0)
   {
      //config https

      debug("start letsencript\n");
      // check if letsencript config exists, if not create new cert
      exitcode=letsencrypt(opaque,cJSON_get_key(env_json, "DEPLOY_DOMAIN"),cJSON_get_key(env_json, "GITLAB_USER_EMAIL"));
      // write https
      if (!exitcode)
      {
         exitcode= write_http_config(opaque,1,basePATH,cJSON_get_key(env_json, "DEPLOY_DOMAIN"));
         if (exitcode){debug("Failed create http config\n");return exitcode;}
         exitcode=  verify_http_config(opaque);
         if (exitcode)
         {
            debug("Failed nginx -t \n");
            delete_http_config(opaque,basePATH);

            return exitcode;
         }
         else exitcode=reload_http_config(opaque);
      }
      else debug("ERR: let's encrypt failed\n");
      update_details(trace);

   }
   update_details(trace);
   return exitcode;
}


/*
 ---
 projects:
 - projecturl1
 - projecturlb

 repos:
   - rpm:
     - el7:
       - x86_64
       - i386
 - apt:
 - test2: apt-test2

 */

/*------------------------------------------------------------------------
 * Internal Command : rpm_init : filter the env, init rpm
 *------------------------------------------------------------------------*/
int cmd_repo_config(void * opaque)
{
   // feedback buffer
   int exitcode=0;
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;

   // env json
   cJSON * env_json=((data_exchange_t *)opaque)->env_json;
   char * newarg[8];
   // no need to set environment for individual commands
   ((data_exchange_t *)opaque)->needenvp=0;
   ((data_exchange_t *)opaque)->paramlist=(char **)newarg;
   parameter_t * param=((data_exchange_t *)opaque)->parameters;
   char var_arg1_string[1024];
   char var_arg2_string[1024];
   // char * var_dest_dir_string[1024];
   int deploy_production=0;
   debug("checking environment\n");
   char basePATH[1024];
   char baseHREF[1024];
   char baseHREF_prod[1024];

   char repoNAME[1024];
   int cnt=0;

   if ((cnt=check_presence(env_json,
                           (char *[])
                           {"CI_PROJECT_PATH_SLUG","CI_ENVIRONMENT_SLUG","CI_ENVIRONMENT_NAME","CI_PROJECT_PATH","CI_PROJECT_NAME","CI_PROJECT_DIR","CI_COMMIT_SHA","CI_COMMIT_REF_SLUG","CI_PROJECT_URL","CI_JOB_TOKEN","CI_PROJECT_ID",NULL}
                           , trace)))
   {
      Write_dyn_trace(trace, none,"ERROR: %d Environment variable(s) Missing\n",cnt);
      debug("\nERROR: %d Environment variable(s) Missing!!!\n",cnt);
      exitcode=1;
   }
   else
   {
      // inform deployctl of environment deployment
      debug("Deploy environment :%s\n",cJSON_get_key(env_json, "CI_ENVIRONMENT_NAME"));
      //Write_dyn_trace(trace, none,"Deploy environment :%s\n",cJSON_GetObjectItem(env_json, "CI_ENVIRONMENT_NAME")->valuestring);
      if (validate_key(env_json, "CI_ENVIRONMENT_NAME", "production")==0)
      {
         debug("RPM _config\n");
         deploy_production=1;
		if (! cJSON_GetObjectItem(env_json, "DEPLOY_DOMAIN"))
         { // we got no deploydomain, so make one
            // as per : $CI_ENVIRONMENT_SLUG.$CI_PROJECT_PATH_SLUG.$DEPLOY_DOMAIN_APP

            debug("\nNo Deploy demain:\n");
            if(validate_project_path_slug(env_json, trace, 1)!=0)
            {
               debug("no valid project path\n");
               Write_dyn_trace(trace, none,"Environment not OK!! \n ** \"DEPLOY_DOMAIN\" and/or \"DEPLOY_APP_DOMAIN\" not defined ** \n");
               exitcode=1;
               return exitcode;
            }
            else
            {
               debug("extracting new domain\n");
               char * env_slug=cJSON_get_key(env_json, "CI_ENVIRONMENT_SLUG");
               char * project_path_slug=cJSON_get_key(env_json, "CI_PROJECT_PATH_SLUG");
               char * deploy_app_domain=cJSON_get_key(env_json, "DEPLOY_DOMAIN_APP");
               char deploy_domain[256];
               sprintf(deploy_domain, "%s.%s.%s",env_slug,project_path_slug,deploy_app_domain);
               debug("new domain %s\n",deploy_domain);
               cJSON_AddItemToObject(env_json, "DEPLOY_DOMAIN", cJSON_CreateString(deploy_domain));
            }
         }
         debug("building paths\n");
         sprintf(baseHREF,"http://%s",cJSON_get_key(env_json, "DEPLOY_DOMAIN"));

		 if (deploy_production && validate_key(env_json, "DEPLOY_CONFIG_HTTPS","True" )==0)
		     sprintf(baseHREF_prod,"https://%s",cJSON_get_key(env_json, "DEPLOY_DOMAIN"));
		  else
			 sprintf(baseHREF_prod,"http://%s",cJSON_get_key(env_json, "DEPLOY_DOMAIN"));


         sprintf((char *)basePATH,"%s/deploy/domain/%s",param->prefix,cJSON_get_key(env_json, "DEPLOY_DOMAIN"));
         char * tmp=cJSON_get_key(env_json, "DEPLOY_REPO_NAME");
         if (tmp && strlen(tmp)>0) sprintf(repoNAME,"%s",tmp); else sprintf(repoNAME,"%s",cJSON_get_key(env_json, "CI_PROJECT_NAME"));

      }
      else
      {

         Write_dyn_trace(trace, none,"ERROR: NOT a production deployment\n");
         debug("\nERROR: NOT a production deployment\n");
         exitcode=1;
         return exitcode;
      }

      debug("checking namespace\n");
      if (check_namespace(opaque, basePATH)!=0)
      {
         Write_dyn_trace(trace, none,"ERR: Namespace belongs to another Project!! \n");
         exitcode=1;
         return exitcode;
      }
      debug("make the directory structure for rpm\n");

      //make the directory structure for rpm
      newarg[0]="/bin/mkdir";
      newarg[1]="-p";
      newarg[2]=(char *)var_arg1_string;
      sprintf((char *)newarg[2],"%s/public",basePATH);
      newarg[3]=NULL;
      Write_dyn_trace_pad(trace, none,75,"+ mkdir repo");

      debug("cmd: %s %s %s \n",newarg[0],newarg[1],newarg[2]);
      exitcode=cmd_exec(opaque);

      if (exitcode)
        Write_dyn_trace(trace, red,"[FAILED]\n");
      else
        Write_dyn_trace(trace, green,"[OK]\n");

      if (exitcode) {debug("Failed exec mkdir %s\n",newarg[2]);return exitcode;}

      exitcode= write_http_config(opaque,2,basePATH,cJSON_get_key(env_json, "DEPLOY_DOMAIN"));
      if (exitcode){debug("Failed create http config\n");return exitcode;}
      exitcode=  verify_http_config(opaque);
      if (exitcode)
      {
         debug("Failed nginx -t \n");
         delete_http_config(opaque,basePATH);

         return exitcode;
      }
      else exitcode=reload_http_config(opaque);

      // check if dns points to this instance for this url
      if (!exitcode) exitcode= url_check(opaque, basePATH,baseHREF);
      if (!exitcode)
      {
         exitcode=write_namespace(opaque, basePATH);
         if (!exitcode) debug("Failed to write_namespace\n");
      }
      // read yalm_file
      char * repo_file=NULL;
      cJSON * repo_json=NULL;
      repo_file =read_file(cJSON_get_key(env_json, "CI_PROJECT_DIR"), "repo.yaml");
      if (!repo_file)
      {
         debug("Failed to get repofile \n");
         exitcode=1;
         return exitcode;
      }
      else debug("read repo.yaml ok\n");
      // convert to json
      repo_json= yaml_sting_2_cJSON (opaque ,repo_file);
      // validate json
      if (!repo_json)
      {
         debug("Failed to create cJSON from yaml  \n");
         free(repo_file);
         exitcode=1;
         return exitcode;
      }
      else
      {
         debug(" got repo_json\n");
      }
      // returns an array of documents
      // we only want first one
      cJSON * temp=repo_json;
      repo_json=cJSON_DetachItemFromArray(temp, 0);
      cJSON_Delete(temp);

      // we need a minimun of projects and repos
      if (check_presence(repo_json,(char *[]){"projects","repos",NULL}, trace)!=0)
      {
         Write_dyn_trace(trace, red,"ERROR: missing yaml entries\n");
         debug("\nERROR: missing yaml entries\n");
         if (repo_json) cJSON_Delete(repo_json);
         exitcode=1;
         return exitcode;
      } else
      {
         debug("got correct yaml entries\n");
      }
            //create directory structure for rpm
      cJSON * endpoints=NULL;
      if (!exitcode )
      {
         endpoints = create_repo_dirs(opaque, basePATH , cJSON_GetObjectItem(repo_json, "repos"));
      }
      else
      {
         Write_dyn_trace(trace, none,"ERR: creating Repodirs\n");

         debug("ERR: creating Repodirs\n");
      }
       update_details(trace);
      // read description.md, convert to html and add to repo.json
	   if (!exitcode && repo_json)
	   {
		   char * tmp = read_file(cJSON_get_key(env_json, "CI_PROJECT_DIR"), "description.md");
		   if (tmp)
		   {
			   char * tmp_html=NULL;
			   char * sub_md=substitute_repo_script(tmp,repoNAME, baseHREF_prod, 0,0);
			   if (sub_md)
			   {
			     tmp_html=html_body_release(sub_md);
				   free(sub_md);
			   }
			   else
			   {
					tmp_html=html_body_release(tmp);
			   }
			   free(tmp);
			   if (tmp_html)
			   {
				   cJSON_AddItemToObject(repo_json, "description",cJSON_CreateString(tmp_html));
				   free(tmp_html);
			   }
		   }
	   }

	   //
      if (!exitcode && repo_json )
      {
         // **** write repo.json and deployw_json.js
         cJSON_AddItemToObject(repo_json, "endpoints",endpoints);
         cJSON_AddItemToObject(repo_json, "base_path", cJSON_CreateString(basePATH));
         cJSON_AddItemToObject(repo_json, "base_href", cJSON_CreateString(baseHREF_prod));
         add_deploy_info_json(opaque,repo_json);
         char filepath[1024];
         char * repo_print=cJSON_Print(repo_json);
         if (repo_print)
         { // write it out
           sprintf(filepath,"%s/public/repo.json",basePATH);
           newarg[0]="write_file";
           newarg[1]=(char *)filepath;
           newarg[2]=repo_print;
           newarg[3]=NULL;
           debug("repo.json:==>\n%s\n\n",newarg[2]);
           Write_dyn_trace_pad(trace, none,75,"+ write /repo.json");

           exitcode=cmd_write(opaque);
           if (exitcode)
             Write_dyn_trace(trace, red,"[FAILED]\n");
           else
             Write_dyn_trace(trace, green,"[OK]\n");
         }
         char * deployw_prt=NULL;
         if (!exitcode && repo_print)
         {
            // convert to js variables
            deployw_prt=create_repo_json_js (opaque, baseHREF_prod, repo_print);
         }
         if (!exitcode && deployw_prt)
         { // wrtie deployw_json.js
            sprintf(filepath,"%s/public/deployw_json.js",basePATH);
            newarg[0]="write_file";
            newarg[1]=(char *)filepath;
            newarg[2]=deployw_prt;
            newarg[3]=NULL;
            debug("deployw_json_js:==>\n%s\n\n",newarg[2]);
            Write_dyn_trace_pad(trace, none,75,"+ write /deployw_json_js");
            exitcode=cmd_write(opaque);
         }
         if (deployw_prt) free(deployw_prt);
         if (repo_print) free(repo_print);

         if (exitcode)
         {
            error("\nERR: writing json data\n");
            Write_dyn_trace(trace, red,"[FAILED]\n");
          }
          else   Write_dyn_trace(trace, green,"[OK]\n");
      }
      update_details(trace);
      if ( endpoints && !exitcode) exitcode=init_repo_dirs(opaque, basePATH, endpoints);

      //if (endpoints) cJSON_Delete(endpoints);


      // symlink css/js/fonts/bootstrap etc for baseHREF
      // TODO should check if no .LAYOUT before symlinking
      if (!exitcode )
      {
         newarg[0]="/bin/sh";
         newarg[1]="-c";
         newarg[2]=(char *)var_arg1_string;
         sprintf((char *)newarg[2],"/bin/ln -sf %s/deploy/config/html_deploy/* %s/public/",param->prefix,basePATH );
         newarg[3]=NULL;
         Write_dyn_trace_pad(trace, none,75,"+ symlink webapp to /");
         debug("cmd:  %s %s %s \n",newarg[0],newarg[1],newarg[2]);
         exitcode=cmd_exec(opaque);
         if (exitcode)
         {
           Write_dyn_trace(trace, red,"[FAILED]\n");
            debug("Failed create symlink %s",newarg[2]);
          }
         else  Write_dyn_trace(trace, green,"[OK]\n");

      }
      if (!exitcode )
      {
         sprintf((char *)var_arg1_string,"%s/deploy/config/repo_conf/repo.rpm.sh",param->prefix );
          sprintf((char *)var_arg2_string,"%s/public/repo.rpm.sh",basePATH );
         Write_dyn_trace_pad(trace, none,75,"+ setup repo script rpm ... ");

         exitcode=copy_script_sh(opaque, var_arg1_string, var_arg2_string, repoNAME, baseHREF, 0,0);
      }
      if (!exitcode )
      {

         sprintf((char *)var_arg1_string,"%s/deploy/config/repo_conf/repo.deb.sh",param->prefix );
         sprintf((char *)var_arg2_string,"%s/public/repo.deb.sh",basePATH );
         Write_dyn_trace_pad(trace, none,75,"+ setup repo script deb ... ");

         exitcode=copy_script_sh(opaque, var_arg1_string, var_arg2_string, repoNAME, baseHREF_prod, 0,0);
      }

      if (repo_json) cJSON_Delete(repo_json);
      update_details(trace);

      if (!exitcode && deploy_production && validate_key(env_json, "DEPLOY_CONFIG_HTTPS","True" )==0)
      {
         //config https

         debug("start letsencript\n");
         // check if letsencript config exists, if not create new cert
         exitcode=letsencrypt(opaque,cJSON_get_key(env_json, "DEPLOY_DOMAIN"),cJSON_get_key(env_json, "GITLAB_USER_EMAIL"));
         // write https
         if (!exitcode)
         {
            exitcode= write_http_config(opaque,3,basePATH,cJSON_get_key(env_json, "DEPLOY_DOMAIN"));
            if (exitcode){debug("Failed create http config\n");return exitcode;}
            exitcode=  verify_http_config(opaque);
            if (exitcode)
            {
               debug("Failed nginx -t \n");
               delete_http_config(opaque,basePATH);

               return exitcode;
            }
            else exitcode=reload_http_config(opaque);
         }
         else debug("ERR: let's encrypt failed\n");

      }
   }
   return exitcode;
}

/*------------------------------------------------------------------------
 * Internal Command : rpm_repo_add : add packages to the repo (rmp/deb)
 *------------------------------------------------------------------------*/
int cmd_repo_add(void * opaque)
{
   // feedback buffer
   int exitcode=0;
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;
   // env json
   cJSON * env_json=((data_exchange_t *)opaque)->env_json;
   char * newarg[8];

   ((data_exchange_t *)opaque)->needenvp=0;
   ((data_exchange_t *)opaque)->paramlist=(char **)newarg;

   debug("checking environment\n");
   char * basePATH;
   char * baseHREF;
   int cnt=0;

	// do we have minimun environment var's ?
   if ((cnt=check_presence(env_json,
                           (char *[]){"CI_PROJECT_DIR","CI_PROJECT_URL","DEPLOY_REPO_URL",NULL}
                           , trace)))
   {
      Write_dyn_trace(trace, red,"ERROR: %d Environment variable(s) Missing\n",cnt);
      debug("\nERROR: %d Environment variable(s) Missing!!!\n",cnt);
      exitcode=1;
	   return exitcode;
   }

	baseHREF=cJSON_get_key(env_json, "DEPLOY_REPO_URL");

	// get the repository config file

	cJSON * repos=NULL;
	debug(" url repo\n");
	Write_dyn_trace_pad(trace, none,75,"+ Getting repo config...");
	exitcode=url_repo_base_path (&basePATH,baseHREF,trace);
	if (!exitcode)
	{
		Write_dyn_trace_pad(trace, none,75,"+ Reading repo config...");
		repos=read_json(opaque, basePATH,"/public","repo.json");

		if (!repos)
		{
			Write_dyn_trace(trace, red,"[FAILED]\n  ERROR: could not read repo config\n");
			debug("\nERROR: could not read repo config\n");
			exitcode=1;
		}
		else Write_dyn_trace(trace, green,"[OK]\n");
	}
	else
	{
    Write_dyn_trace(trace, red,"[FAILED]\n  ERROR: could not accuire url base_path_url\n");
		debug("\nERROR: could get config request\n");
		exitcode=1;
	}

	// verify the repo configuration
	if (!exitcode)
	{
		debug("checking validity of repo config\n");
		Write_dyn_trace_pad(trace, none,75,"+ Verify config...");
		if ((cnt=check_presence(repos,
								(char *[])
								{"projects","endpoints","base_path","base_href","repos",NULL}
								, trace)))
		{
			Write_dyn_trace(trace, red,"ERROR: incomplete repo file in repository\n");
			debug("\nERROR: incomplete repo file in repository\n");
			exitcode=1;
		}
		else Write_dyn_trace(trace, green,"[OK]\n");
	}

	// check if this project has access to this repository
	if (!exitcode)
	{
		debug("checking repo_access\n");
		Write_dyn_trace_pad(trace, none,75,"+ Checking access...");
		if (in_string_array(cJSON_GetObjectItem(repos, "projects"),cJSON_get_key(env_json, "CI_PROJECT_URL"))!=0)
		{
			Write_dyn_trace(trace, red,"\nERROR: access denied to this repo\n");
			debug("\nERROR: access denied\n");
			exitcode=1;
		}
		else Write_dyn_trace(trace, green,"[OK]\n");
	}


	// check if enviroment condition met!
	debug("checking environment\n");
	char * env_env=cJSON_get_key(env_json, "CI_ENVIRONMENT_NAME");
	if ( cJSON_GetObjectItem(repos, "environments"))
	{
		debug("environment list defined\n");
		//loop allowed environment list
		cJSON * pos=NULL;
		int found=0;
		cJSON_ArrayForEach(pos,cJSON_GetObjectItem(repos, "environments"))
		{
			char * test_item=pos->valuestring;
			debug("environment list item: %s\n",test_item);
			if (test_item && strcmp(test_item,"none")==0) {found=1; break;}
			if (test_item && env_env && strcmp(test_item,"all")==0) {found=1; break;}
			if (test_item && env_env && strcmp(test_item,"review/*")==0 && strncmp(env_env,"review/*",8)==0) {found=1; break;}
			if (test_item && env_env && strcmp(test_item,env_env)==0) {found=1; break;}
		}
		if (!found)
		{
		Write_dyn_trace(trace, red,"\n  ERROR: defined environment condition not met!\n");
		debug("\nERROR: defined environment condition not met!\n");
		if (repos) cJSON_Delete(repos);
		exitcode=1;
		return exitcode;
		}
	}
	else
	{
		debug("no environment list defined\n");
		// no environment condition defined in repo.yaml, need to have production env
		if ( !env_env || strcmp(env_env,"production")!=0 )
		{
			Write_dyn_trace(trace, red,"ERROR: default 'production' environment not met!\n");
			debug("\nERROR: default 'production' environment not met!\n");
			if (repos) cJSON_Delete(repos);
			exitcode=1;
			return exitcode;
		}
	}
	debug("start deployment\n");

	// inform deployctl of environment deployment, if any
	if (env_env)
	{
    Write_dyn_trace(trace, yellow,"  Deployment repo url:");
    Write_dyn_trace(trace, cyan," %s\n",baseHREF);
	}

	// Start
	if (!exitcode)
	{
	// first read repository directories (rpm/deb) and get info on type,distribution and Arch/release (rpm/debian)
		 cJSON * repo_list=NULL;
      char repo_path_full[1024];
       char * user_path=cJSON_GetArrayItem(cJSON_GetObjectItem(env_json, "DEPLOY_REPO_PATH"),0)->valuestring;
      sprintf(repo_path_full, "%s%s",cJSON_get_key(env_json, "CI_PROJECT_DIR"),user_path);

		 get_repo_list(opaque,repo_path_full, &repo_list);

		 cJSON * matches=NULL;
		 cJSON * skips=NULL;
		 cJSON * symlinks=NULL;

		 // with the info, create matches and/or symlinks and/or skips
		 if (repo_list)
		 {
			match_end_points( opaque, repo_list,cJSON_GetObjectItem(repos, "endpoints"), &matches,&skips,&symlinks);
			cJSON_Delete(repo_list);
			repo_list=NULL;
		 }
     update_details(trace);
      if (matches && cJSON_GetArraySize(matches)>0)
      {
		 // copy the matches in place
     Write_dyn_trace(trace, yellow,"\n  Copy packages in place from: ");
      Write_dyn_trace(trace, cyan,"%s/*\n",user_path);
		 repo_copy_matches(opaque,matches,basePATH);
     update_details(trace);
      }
		 // create symlinks for the wildcards
      if (symlinks && cJSON_GetArraySize(symlinks)>0)
      {
     		Write_dyn_trace(trace, yellow,"\n  symlink wildcards in place...\n");
		 repo_create_symlinks(opaque,symlinks,basePATH);
     update_details(trace);
      }
		 // Inform User of problems
		 if (skips && cJSON_GetArraySize(skips)>0)
       {
          repo_print_skips(opaque,skips);
     update_details(trace);
       }
		 // update repos if something to update
		 if (matches)
     {
       Write_dyn_trace(trace, yellow,"\n  Update repositories...\n");
         update_repo_dirs(opaque, basePATH, cJSON_GetObjectItem(repos, "endpoints"));
     }
     update_details(trace);
		 // cleanup
		 if (repos) cJSON_Delete(repos);
		 if (matches) cJSON_Delete(matches);
		 if (symlinks) cJSON_Delete(symlinks);
		 if (skips)  cJSON_Delete(skips);
	}

   return exitcode;
}
