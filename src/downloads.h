//
//  downloads.h
//  deploy-runner
//
//  Created by Danny Goossen on 23/7/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#ifndef __deploy_runner__downloads__
#define __deploy_runner__downloads__

#include "common.h"

#define NUMT 6

struct dl_artifacts_s {
   struct data_thread data[NUMT];
   int free_count;
   int todo;
   int next;
   int next_unzip;
   int unzip_todo;
   pthread_t tid[NUMT];
   char * proj_dir;
   char * zip_dir;
};

cJSON * prepare_artifacts(cJSON * job);
void scan_artifacts_status(cJSON * artifacts, struct trace_Struct *trace);

void init_download_artifacts(struct dl_artifacts_s * dl_artifacts, int count,char * path, char* job_id);
void close_download_artifacts(struct dl_artifacts_s * dl_artifacts);
void cancel_download_artifacts(struct dl_artifacts_s * dl_artifacts);
int download_artifacts(cJSON * artifacts, struct dl_artifacts_s * dl_artifacts);
int progress_change(struct dl_artifacts_s * dl_artifacts);
#endif /* defined(__deploy_runner__downloads__) */
