//
//  downloads.c
//  deploy-runner
//
//  Created by Danny Goossen on 23/7/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#include "deployd.h"

#include <ftw.h>




/*
 List of URLs to fetch.

 If you intend to use a SSL-based protocol here you might need to setup TLS
 library mutex callbacks as described here:

 https://curl.haxx.se/libcurl/c/threadsafe.html

 */



/*
 int pthread_create(pthread_t *new_thread_ID,
 const pthread_attr_t *attr,
 void * (*start_func)(void *), void *arg);
 */



// init and have a struct pointer with thread info
// the download to add if thread availleble
// free to tear it all down

void init_download_artifacts(struct dl_artifacts_s * dl_artifacts, int count,char * proj_dir, char* zip_dir)
{
   //setdebug();
   _mkdir("",proj_dir );
   _mkdir("",zip_dir );
   int i=0;
   dl_artifacts->free_count=NUMT;
   for(i=0; i< NUMT; i++) {
      dl_artifacts->data[i].finish=0;
      dl_artifacts->data[i].free=1;
      dl_artifacts->data[i].path=zip_dir;
      dl_artifacts->data[i].http_code=-1;
      dl_artifacts->data[i].artifact=NULL;
      dl_artifacts->data[i].progress=0;

   }
   dl_artifacts->todo=count;
   dl_artifacts->unzip_todo=count;
   dl_artifacts->next_unzip=0;
   dl_artifacts->next=0;
   dl_artifacts->proj_dir=proj_dir;
   dl_artifacts->zip_dir=zip_dir;

   return;
}

void cancel_download_artifacts(struct dl_artifacts_s * dl_artifacts)
{
   debug("Canceling download\n");
   int i=0;
   for(i=0; i< NUMT; i++) {
      if (!dl_artifacts->data[i].free) pthread_cancel(dl_artifacts->tid[i]);
   }
   //clrdebug();
   return;
}

void close_download_artifacts(struct dl_artifacts_s * dl_artifacts)
{
   debug("waiting for unfinished threads to end\n");
   int i=0;
   for(i=0; i< NUMT; i++) {
      if (!dl_artifacts->data[i].free) pthread_join(dl_artifacts->tid[i], NULL);
      dl_artifacts->data[i].finish=0;
      dl_artifacts->data[i].path=NULL;
      dl_artifacts->data[i].free=1;
      dl_artifacts->data[i].path=NULL;
      dl_artifacts->data[i].http_code=-1;
      dl_artifacts->data[i].artifact=NULL;
   }
   dl_artifacts->free_count=NUMT;


   //clrdebug();
   return;
}


int start_an_artifacts_download(cJSON * artifacts,struct dl_artifacts_s * dl_artifacts,int thread_number)
{
   int error_1=0;
   dl_artifacts->data[thread_number].free=0;
   dl_artifacts->data[thread_number].error=0;
   dl_artifacts->data[thread_number].artifact=cJSON_GetArrayItem(artifacts, dl_artifacts->next);
   cJSON*error_count_json=cJSON_GetObjectItem(dl_artifacts->data[thread_number].artifact, "error_count");
   cJSON_ReplaceItemInObject(dl_artifacts->data[thread_number].artifact, "status", cJSON_CreateString("download"));
   cJSON_GetObjectItem(dl_artifacts->data[thread_number].artifact, "progress")->valueint=0;
   if (error_count_json)
      dl_artifacts->data[thread_number].error_count=error_count_json->valueint;
   else {
      dl_artifacts->data[thread_number].error_count=0;
      cJSON_AddNumberToObject(dl_artifacts->data[thread_number].artifact, "error_count", 0);
   }
   dl_artifacts->data[thread_number].progress=0;
   error_1=pthread_create(&dl_artifacts->tid[thread_number],
                        NULL, /* default attributes please */
                        get_artifact,
                        (void *)&dl_artifacts->data[thread_number]);
   if(0 != error_1)
   {
      error( "Couldn't run thread number %d, errno %d\n", thread_number, error_1);
      return -1;
   }
   else
   {
      dl_artifacts->free_count--;
      return 0;
   }

}

int unzip_one(cJSON* tmp,struct dl_artifacts_s * dl_artifacts)
{
  if (!tmp)
  {
    error("unzip one no JSON data\n");
  return -1;

}
    char filename[1024];
    sprintf(filename, "%s/%s.zip",dl_artifacts->zip_dir, cJSON_get_key(tmp, "name"));
    char unzipdest[1024];
    sprintf(unzipdest, "%s/project_dir",dl_artifacts->proj_dir);
    debug("* unzip_one: \n ->file: %s \n ->dest: %s\n",filename,unzipdest);
    if (unzip(filename, unzipdest)==0)
    { //success
       dl_artifacts->next_unzip++;
       dl_artifacts->unzip_todo--;
       cJSON_ReplaceItemInObject(tmp, "unzip_status", cJSON_CreateString("done"));
       debug("Unzip status to done\n");
       return 0;
    }
    else
    { // fail
       cJSON_ReplaceItemInObject(tmp, "unzip_status", cJSON_CreateString("error"));
        error("Unzip status to ERROR\n");
       return -1;
    }
}

// Prepare artifacts create a JSON structure containing everything needed to download artifact
// from JOB::dependencies
// artifacts[]:
//     :api (url request)

cJSON * prepare_artifacts(cJSON * job)
{
   cJSON * artifacts=NULL;
   cJSON * dependencies=cJSON_GetObjectItem(job, "dependencies");
   debug("Getting %d dependencie(s)\n",cJSON_GetArraySize(dependencies));
   cJSON * dependency;
   char url[1024];
   //setdebug();
   cJSON_ArrayForEach(dependency,dependencies)
   {

      cJSON * file=cJSON_GetObjectItem(dependency, "artifacts_file");
      if (file)
      {
         sprintf(url,"%s/api/v4/jobs/%d/artifacts",cJSON_get_key(job, "ci_url"),cJSON_GetObjectItem(dependency, "id")->valueint);
         cJSON * tmp=cJSON_CreateObject();
         cJSON_AddStringToObject(tmp, "api", url);
         cJSON_AddStringToObject(tmp, "name",cJSON_get_key(dependency, "name" ) );
         cJSON_AddStringToObject(tmp, "status","pending");
         cJSON_AddStringToObject(tmp, "unzip_status","");
         cJSON_AddStringToObject(tmp, "filename",cJSON_get_key(file, "filename" ));
         // JOB-TOKEN is token as per dependency token
         char header_auth[256];
         sprintf(header_auth, "JOB-TOKEN: %s",cJSON_get_key(dependency, "token" ));
         cJSON_AddStringToObject(tmp, "header_auth",header_auth);
         cJSON_AddNumberToObject(tmp, "size",cJSON_GetObjectItem(file, "size")->valueint);
         cJSON_AddNumberToObject(tmp, "progress",0);
         if(!artifacts) artifacts=cJSON_CreateArray();
         cJSON_AddItemToArray(artifacts, tmp);
      }
      else
         debug("artifacts %s has no file \n",cJSON_get_key(dependency, "name" ));
   }
   //clrdebug();
   return artifacts;
}


// quick scan of the threads to see if ANY PROGRESS CHANGE
// resturns 1 on an update
int progress_change(struct dl_artifacts_s * dl_artifacts)
{
   int i;
   int result=0;
   for(i=0; i< NUMT; i++)
   {
      if (dl_artifacts->data[i].progress)
      {
         debug("prgress change\n");
         result= 1;
         dl_artifacts->data[i].progress=0;
      }
   }
   return result;
}

void scan_artifacts_status(cJSON * artifacts, struct trace_Struct *trace)
{
   debug("scan artifacts status\n");
   cJSON * artifact;
   char temp[1024];
   clear_till_mark_dynamic_trace(trace);
   ssize_t len_job=0;
   cJSON_ArrayForEach(artifact,artifacts)
   {
      len_job=strlen(cJSON_get_key(artifact, "name"));
      char padding[]="                                 ";
      if (len_job<20) padding[20-len_job]=0; else padding[0]=0;
      sprintf(temp,"  + %s%s  ",cJSON_get_key(artifact, "name"),padding);
      Write_dynamic_trace(temp,NULL,trace);
      if (validate_key(artifact,"status", "done")==0)
         Write_dynamic_trace_color("  [OK]  ",NULL,green,trace);
      else if (validate_key(artifact,"status", "error")==0)
         Write_dynamic_trace_color(" [FAIL] ",NULL,red,trace);
       else if (validate_key(artifact,"status", "download")==0)
       {
          Write_dyn_trace(trace, none, "%2d%%",cJSON_GetObjectItem(artifact, "progress")->valueint);
       }
      else
         Write_dynamic_trace_color(cJSON_get_key(artifact, "status"),NULL,yellow,trace);
      
      Write_dynamic_trace("    \t",NULL,trace);

      if (validate_key(artifact,"unzip_status", "done")==0)
         Write_dynamic_trace_color(" [OK] ",NULL,green,trace);
      else if (validate_key(artifact,"unzip_status", "error")==0)
         Write_dynamic_trace_color("[FAIL]",NULL,red,trace);
      else
         Write_dynamic_trace_color(cJSON_get_key(artifact, "unzip_status"),NULL,yellow,trace);
      Write_dynamic_trace_color("\n",NULL,bold_yellow,trace);
   }
   return;
}


int download_artifacts(cJSON * artifacts, struct dl_artifacts_s * dl_artifacts)
{

   int i;
   int error = 0;
   // check if we have threads finished / maybe retry on error
   if (dl_artifacts->free_count != NUMT)
   { //swoop and check if finished
      for(i=0; i< NUMT; i++) {
         if (dl_artifacts->data[i].finish && !dl_artifacts->data[i].free )
         {
            pthread_join(dl_artifacts->tid[i], NULL); // shall not return EINTR
            debug("Joined artithread %s\n",cJSON_get_key(dl_artifacts->data[i].artifact, "name"));
            dl_artifacts->data[i].finish=0;
            dl_artifacts->data[i].free=1;
            dl_artifacts->free_count++;
            if (dl_artifacts->data[i].error !=0)
            {
               debug("got a download error, error_cout=%d\n",dl_artifacts->data[i].error_count);
               dl_artifacts->data[i].error_count++;
               if (dl_artifacts->data[i].error_count < 3)
               {
                  cJSON_GetObjectItem(dl_artifacts->data[i].artifact, "error_count")->valueint=dl_artifacts->data[i].error_count;
                	cJSON_ReplaceItemInObject(dl_artifacts->data[i].artifact, "status", cJSON_CreateString("retry"));
                  // and startit again
                  error=start_an_artifacts_download(artifacts, dl_artifacts, i);
                  if(0 != error) return error;
               }
               else return -1;
            }
            else
            {
            	cJSON_ReplaceItemInObject(dl_artifacts->data[i].artifact, "unzip_status", cJSON_CreateString("pending"));
              debug("unzip pending\n");
            }
         }
      }
   }

   // do we still have something to do? if not and all threads free we're finished return 0;
   if (dl_artifacts->todo==0 && dl_artifacts->free_count == NUMT && dl_artifacts->unzip_todo==0) return 0;

   // do we do have something to do
   if (dl_artifacts->todo > 0 )
   {
     debug("todo==%d, unzip_todo %d, next download=%d, next unzip=%d\n",dl_artifacts->unzip_todo,dl_artifacts->todo, dl_artifacts->next,dl_artifacts->next_unzip);
      //if no free threads, sleep and comeback later
      if (dl_artifacts->free_count == 0)
      {
         cJSON * tmp=cJSON_GetArrayItem(artifacts, dl_artifacts->next_unzip);
         if (tmp && validate_key(tmp,"status", "done")==0)
         { // unzip one
            if (unzip_one(tmp,dl_artifacts)==-1) return -1;
         }
         else
         	usleep(300000); // sleep a bit come back later, we got nothing else todo
         return (dl_artifacts->todo+ dl_artifacts->unzip_todo);
      }
       // we have something to do, find free thread
      for(i=0; i< NUMT; i++) {
         if (dl_artifacts->data[i].free==1) break;
      }

      error=start_an_artifacts_download(artifacts, dl_artifacts, i);
      if(0 != error)
         return error;
      else
      {
         dl_artifacts->next++;
         dl_artifacts->todo--;
      }
   }
   else if (dl_artifacts->unzip_todo > 0 )
   {
      debug("todo==%d, unzip_todo %d, next unzip=%d\n",dl_artifacts->unzip_todo,dl_artifacts->todo,dl_artifacts->next_unzip);
      cJSON * tmp=cJSON_GetArrayItem(artifacts, dl_artifacts->next_unzip);
      if (tmp && validate_key(tmp,"unzip_status", "pending")==0)
      { // unzip one
         if (unzip_one(tmp,dl_artifacts)==-1) return -1;
      }
      else {
        usleep(300000);
        //printf("waiting for downloads to provide a download to unzip\n");
      }
   }
   if ((dl_artifacts->todo+ dl_artifacts->unzip_todo)==0  && dl_artifacts->free_count!=NUMT) return NUMT-dl_artifacts->free_count;
   return (dl_artifacts->todo+ dl_artifacts->unzip_todo);
}
