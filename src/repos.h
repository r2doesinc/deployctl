//
//  repos.h
//  deployctl
//
//  Created by Danny Goossen on 10/5/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#ifndef __deployctl__repos__
#define __deployctl__repos__

#include "common.h"

cJSON * create_repo_dirs(void * opaque, char * repo_PATH , cJSON* repos);

int init_repo_dirs(void * opaque, char * basepath, cJSON* endpoints);
int update_repo_dirs(void * opaque, char * basepath, cJSON* endpoints);

int get_repo_list(void * opaque, const char *projectpath, cJSON** repo_info);

char * create_repo_json_js (void * opaque, char * download_url, char * repo_json);
int match_end_points(void * opaque, cJSON* repo_info, cJSON* end_points, cJSON ** matches, cJSON ** skips, cJSON** symlinks);
char * extract_dir_structure( cJSON* repo, cJSON** end_points, cJSON * trace_endpoints);
cJSON * read_json(void * opaque, const char * base_path,const char * sub_path,const char * filename);

void repo_print_skips(void * opaque,cJSON * skips);
void repo_copy_matches(void * opaque,cJSON * matches,char* basepath);
void repo_create_symlinks(void * opaque,cJSON * symlinks,char* basepath);

int copy_script_sh(void * opaque, const char * source, const char * destination, char * repo_name, char * repo_url, int PGP_repo, int PGP_pack);

#endif /* defined(__deployctl__repos__) */
