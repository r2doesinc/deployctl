//
//  utils.h
//  deployctl
//
//  Created by Danny Goossen on 10/5/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#ifndef __deployctl__utils__
#define __deployctl__utils__

#include "common.h"


void upper_string(char s[]);
void upper_string_n(char s[],size_t n);
void lower_string(char s[]);

void _rmdir(const char * dir, const char * base);
void split_path_file(char** p, char** f,const char *pf);
void split_path_file_2_path(char** p,const char *pf);
void _mkdir(const char *dir, const char * base);
int getfilelist(char * projectdir,cJSON * paths,cJSON ** filelist);

#endif /* defined(__deployctl__utils__) */
