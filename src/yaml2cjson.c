//
//  yaml2cjson.c
//  deployctl
//
//  Created by Danny Goossen on 10/5/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//


#include "deployd.h"



// doesn't print styles.
cJSON * yaml_node_json(yaml_document_t *document_p, yaml_node_t *node)
{
   static int x = 0;
   x++;
   int node_n = x;
   cJSON * result=NULL;
   yaml_node_t *next_node_p;

   switch (node->type) {
      case YAML_NO_NODE:
         printf("Empty node(%d):\n", node_n);
         break;
      case YAML_SCALAR_NODE:
      {
         if (strcmp((const char*)node->tag,YAML_NULL_TAG)==0)
            result=cJSON_CreateNull();
         else if (strcmp((const char*)node->tag,YAML_STR_TAG)==0)
            result=cJSON_CreateString_n((const char* )node->data.scalar.value, node->data.scalar.length);
         else if (strcmp((const char*)node->tag,YAML_BOOL_TAG)==0)
         {
            upper_string_n((char*)node->data.scalar.value, node->data.scalar.length);
            if (node->data.scalar.length==4 && strncmp((const char *)node->data.scalar.value, "TRUE", 4)==0)
               result=cJSON_CreateTrue();
            else if (node->data.scalar.length==5 && strncmp((const char *)node->data.scalar.value, "FALSE", 5)==0)
               result=cJSON_CreateFalse();
            else if (node->data.scalar.length==3 && strncmp((const char *)node->data.scalar.value, "YES", 3)==0)
               result=cJSON_CreateTrue();
            else if (node->data.scalar.length==2 && strncmp((const char *)node->data.scalar.value, "NO", 2)==0)
               result=cJSON_CreateFalse();
            else if (node->data.scalar.length==2 && strncmp((const char *)node->data.scalar.value, "ON", 2)==0)
               result=cJSON_CreateTrue();
            else if (node->data.scalar.length==3 && strncmp((const char *)node->data.scalar.value, "OFF", 3)==0)
               result=cJSON_CreateFalse();
            else if (node->data.scalar.length==1 && strncmp((const char *)node->data.scalar.value, "Y", 1)==0)
               result=cJSON_CreateTrue();
            else if (node->data.scalar.length==1 && strncmp((const char *)node->data.scalar.value, "N", 1)==0)
               result=cJSON_CreateFalse();
            else if (node->data.scalar.length==1 && strncmp((const char *)node->data.scalar.value, "1", 1)==0)
               result=cJSON_CreateTrue();
            else if (node->data.scalar.length==1 && strncmp((const char *)node->data.scalar.value, "0", 1)==0)
               result=cJSON_CreateFalse();
            else
               result=cJSON_CreateString_n((const char*)node->data.scalar.value, node->data.scalar.length);
         }
         else if (strcmp((const char*)node->tag,YAML_INT_TAG)==0)
         {
            char * errCheck;
            char * buf=cJSON_strdup_n((const unsigned char*)node->data.scalar.value, node->data.scalar.length);
            int i = (int)strtol(buf, &errCheck,(10));
            free(buf);
            if(errCheck == buf)
               result=cJSON_CreateString_n((const char*)node->data.scalar.value, node->data.scalar.length);
            else
               result=cJSON_CreateNumber(i);
         }
         else if (strcmp((const char*)node->tag,YAML_FLOAT_TAG)==0)
         {
            char * errCheck;
            char * buf=cJSON_strdup_n((const unsigned char*)node->data.scalar.value, node->data.scalar.length);
            float f = strtod(buf, &errCheck);
            free(buf);
            if(errCheck == buf)
               result=cJSON_CreateString_n((const char*)node->data.scalar.value, node->data.scalar.length);
            else
               result=cJSON_CreateNumber(f);
         }
         else if (strcmp((const char*)node->tag,YAML_TIMESTAMP_TAG)==0)
            result=cJSON_CreateString_n((const char*)node->data.scalar.value, node->data.scalar.length);
         break;
      }
      case YAML_SEQUENCE_NODE:
      {
         yaml_node_item_t *i_node;
         cJSON * temp=NULL;
         for (i_node = node->data.sequence.items.start; i_node < node->data.sequence.items.top; i_node++) {
            next_node_p = yaml_document_get_node(document_p, *i_node);
            if (next_node_p)
            {
               temp=yaml_node_json(document_p, next_node_p);
               if (temp)
               {
                  if (result==NULL ) result=cJSON_CreateArray();
                  cJSON_AddItemToArray(result, temp);
               }
            }
         }
         break;
      }
      case YAML_MAPPING_NODE:
      {
         yaml_node_pair_t *i_node_p;
         cJSON * temp=NULL;
         for (i_node_p = node->data.mapping.pairs.start; i_node_p < node->data.mapping.pairs.top; i_node_p++) {

            next_node_p = yaml_document_get_node(document_p, i_node_p->value);
            if (next_node_p) {
               temp=yaml_node_json(document_p, next_node_p);
            }
            next_node_p = yaml_document_get_node(document_p, i_node_p->key);
            if (next_node_p && next_node_p->type==YAML_SCALAR_NODE)
            {
               if (result==NULL) result=cJSON_CreateObject();
               if (strncmp("<<", (const char*)next_node_p->data.scalar.value, next_node_p->data.scalar.length)==0)
               { // merge, deconstruct temp
                  if (temp && temp->type==cJSON_Object)
                  {
                     cJSON * temp_item= temp->child;
                     temp->child=NULL;
                     cJSON * traverse;
                     temp->child=NULL;
                     if (result->child)
                     {
                        traverse=result->child;
                        while ( traverse->next !=NULL) traverse=traverse->next;
                        traverse->next=temp_item;
                        temp_item->prev=traverse;
                     }
                     else
                     {
                        result->child=temp_item;
                     }
                  } else if (temp && temp->type==cJSON_Array)
                  {
                     // loop the array and add children to result
                     cJSON * pos=NULL;
                     int item=0;
                     cJSON * traverse;
                     cJSON_ArrayForEach(pos,temp)
                     {
                        if (result->child)
                        {
                           traverse=result->child;
                           while ( traverse->next !=NULL) traverse=traverse->next;
                           traverse->next=pos->child;
                           pos->child=NULL;
                        }
                        else
                        {
                           result->child=pos->child;
                           pos->child=NULL;
                        }
                        item ++;
                     }
                  }
                  else
                  {
                     cJSON_AddItemToObject(result, "", temp);
                  }
                  cJSON_Delete(temp);
               }
               else
                  cJSON_AddItemToObject_n(result, (const char*)next_node_p->data.scalar.value, next_node_p->data.scalar.length, temp);
            }else if (next_node_p)
            {
               cJSON * temp_key=yaml_node_json(document_p, next_node_p);
               if (temp_key){
                  char * special_key=cJSON_PrintUnformatted(temp_key);
                  char * print_key=calloc(1, strlen(special_key)+ 25);
                  // TODO can maybe do something with raw
                  sprintf(print_key,".error_unsupported_key: %s",special_key);
                  cJSON_AddItemToObject(result,print_key,temp);
                  if (special_key) free(special_key);
                  if (print_key) free(print_key);
               }
            }
         }
         break;
      }
      default:
         break;
   }
   return result;
}

void yaml_document_2_cJSON(yaml_document_t *document_p, cJSON ** json_doc)
{
   cJSON * result=NULL;
   result=yaml_node_json(document_p, yaml_document_get_root_node(document_p));
   if (result && *json_doc==NULL) *json_doc=cJSON_CreateArray();
   if (result) cJSON_AddItemToArray(*json_doc,result);
}

cJSON * yaml_sting_2_cJSON (void * opaque ,const char * yaml)
{
   cJSON * result=NULL;
   yaml_parser_t  parser;
   yaml_document_t document;
   yaml_parser_initialize(&parser);
   yaml_parser_set_input_string(&parser, (const unsigned char *) yaml, strlen(yaml));

   int done = 0;
   while (!done)
   {
      if (!yaml_parser_load(&parser, &document)) {
         debug( "Failed to load document \n");
         // todo feedback
         break;
      }

      done = (!yaml_document_get_root_node(&document));

      if (!done)
        yaml_document_2_cJSON(&document,&result);

      yaml_document_delete(&document);
   }
   yaml_parser_delete(&parser);
   return result;
}
