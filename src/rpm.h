//
//  rpm.h
//  deployctl
//
//  Created by Danny Goossen on 29/5/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#ifndef __deployctl__rpm__
#define __deployctl__rpm__

#include "common.h"

#define RPM_OK 0
#define RPM_ERR_SYSTEM 1
#define RPM_ERR_OPEN 2
#define RPM_ERR_READ 3
#define RPM_ERR_INVALID 4
#define RPM_ERR_BADARG 5



int get_rpm_fileinfo(void * opaque, const char * filepath, const char * filename,cJSON ** rpm_info);

#endif /* defined(__deployctl__rpm__) */
