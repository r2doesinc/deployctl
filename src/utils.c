//
//  utils.c
//  deployctl
//
//  Created by Danny Goossen on 10/5/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#define _XOPEN_SOURCE 500
#include <ftw.h>
typedef unsigned short int u_short;
#include <fts.h>

#include "deployd.h"




#ifndef OPEN_MAX
#define OPEN_MAX 1023
#endif


/*------------------------------------------------------------------------
 * Convert string to uppercase
 *------------------------------------------------------------------------*/
void upper_string(char s[]) {
   if (!s) return;
   int c = 0;

   while (s[c] != '\0') {
      if (s[c] >= 'a' && s[c] <= 'z') {
         s[c] = s[c] - 32;
      }
      c++;
   }
}
/*------------------------------------------------------------------------
 * Convert string to lower case
 *------------------------------------------------------------------------*/
void lower_string(char s[]) {
   if (!s) return;
   int c = 0;
   
   while (s[c] != '\0') {
      if (s[c] >= 'A' && s[c] <= 'Z') {
         s[c] = s[c] + 32;
      }
      c++;
   }
}

void upper_string_n(char s[],size_t n) {
   if (!s) return;
   int c = 0;

   while (s[c] != '\0' && c<n) {
      if (s[c] >= 'a' && s[c] <= 'z') {
         s[c] = s[c] - 32;
      }
      c++;
   }
}

void split_path_file(char** p, char** f,const char *pf) {
  if (!pf || strlen(pf)==0) return;
   const char *slash = pf;
   char *next;
   while ((next = strpbrk(slash + 1, "\\/"))) slash = next;
   if (pf != slash) slash++;
   *p = cJSON_strdup_n((const unsigned char *)pf, slash - pf);
   *f = strdup(slash);
}

void split_path_file_2_path(char** p, const char *pf) {
    if (!pf || strlen(pf)==0) return;
   const char *slash = pf;
   char *next;
   while ((next = strpbrk(slash + 1, "\\/"))) slash = next;
   if (pf != slash) slash++;
   *p =cJSON_strdup_n((const unsigned char *)pf, slash - pf);
}

int
nftw_x(const char *path, int (*fn)(const char *, const struct stat *, int,
                                 struct FTW *,void * data), int nfds, int ftwflags,void * data)
{
   const char *paths[2];
   struct FTW ftw;
   FTSENT *cur;
   FTS *ftsp;
   int ftsflags, fnflag, error, postorder, sverrno;

   /* XXX - nfds is currently unused */
   if (nfds < 1 || nfds > OPEN_MAX) {
      errno = EINVAL;
      return (-1);
   }

   ftsflags = FTS_COMFOLLOW;
   if (!(ftwflags & FTW_CHDIR))
      ftsflags |= FTS_NOCHDIR;
   if (ftwflags & FTW_MOUNT)
      ftsflags |= FTS_XDEV;
   if (ftwflags & FTW_PHYS)
      ftsflags |= FTS_PHYSICAL;
   postorder = (ftwflags & FTW_DEPTH) != 0;
   paths[0] = path;
   paths[1] = NULL;
   ftsp = fts_open((char * const *)paths, ftsflags, NULL);
   if (ftsp == NULL)
      return (-1);
   error = 0;
   while ((cur = fts_read(ftsp)) != NULL) {
      switch (cur->fts_info) {
         case FTS_D:
            if (postorder)
               continue;
            fnflag = FTW_D;
            break;
         case FTS_DNR:
            fnflag = FTW_DNR;
            break;
         case FTS_DP:
            if (!postorder)
               continue;
            fnflag = FTW_DP;
            break;
         case FTS_F:
         case FTS_DEFAULT:
            fnflag = FTW_F;
            break;
         case FTS_NS:
         case FTS_NSOK:
            fnflag = FTW_NS;
            break;
         case FTS_SL:
            fnflag = FTW_SL;
            break;
         case FTS_SLNONE:
            fnflag = FTW_SLN;
            break;
         case FTS_DC:
            errno = ELOOP;
            /* FALLTHROUGH */
         default:
            error = -1;
            goto done;
      }
      ftw.base = cur->fts_pathlen - cur->fts_namelen;
      ftw.level = cur->fts_level;
      error = fn(cur->fts_path, cur->fts_statp, fnflag, &ftw,data);
      if (error != 0)
         break;
   }
done:
   sverrno = errno;
   (void) fts_close(ftsp);
   errno = sverrno;
   return (error);
}

int remove_it(const char *path, const struct stat *s, int flag, struct FTW *f)
{
   int status=0;
   int (*rm_func)( const char * );

   switch( flag ) {
      default:     rm_func = unlink; break;
      case FTW_DP: rm_func = rmdir;
   }
  status = (rm_func( path ), status != 0 );
    //  perror( path );
   return status;
}

struct payload{
   cJSON ** filelist;
};


int add_to_file_list(const char *path,int mode,cJSON ** filelist)
{
   int result=0;
   size_t len=strlen(path);
   if ((S_ISDIR(mode) && len>2 && strcmp(path+len-2, ".")!=0 && len >3 && strcmp(path+len-3, "..")!=0 )|| !S_ISDIR(mode))
   {
      if (!*filelist) *filelist=cJSON_CreateArray();
      if (*filelist)
      {
         cJSON * tmpo=cJSON_CreateObject();
         cJSON_AddStringToObject(tmpo, "path", path);
         cJSON_AddNumberToObject(tmpo, "mode", mode);
         cJSON_AddItemToArray(*filelist,tmpo);
      }
      else
         result=1;
   }
   return result;
}

int list_it(const char *path, const struct stat *s, int flag, struct FTW *f,void* data)
{
   int status=0;
   switch( flag ) {
      default:     ; break;
         /* we'll handle dir/symlinks/files */
      case FTW_SL:
      case FTW_F:
      case FTW_D:
         status=add_to_file_list( path,s->st_mode ,data) ;break;
   }
   return status;
}


int getfilelist(char * projectdir,cJSON * paths,cJSON ** filelist)
{
   
   if (!projectdir || !paths || !filelist ) return -1;
   char tmp[1024];
   cJSON * path;
   cJSON_ArrayForEach(path,paths)
   {
      snprintf((char*)tmp,1024,"%s%s",projectdir,path->valuestring);
      debug("get filelist for path %s\n",tmp);
      
      
      struct stat sb;
      
      if (stat(tmp, &sb) == 0 && S_ISDIR(sb.st_mode))
      {
         
         add_to_file_list(path->valuestring, sb.st_mode, filelist);
         if (nftw_x((char*)tmp, list_it, 6 ,FTW_PHYS ,(void*)filelist)) //| FTW_DEPTH
         {
            perror( tmp );
            error("nftw return non 0\n");
            return 1;
         }
      }
      else if (stat(tmp, &sb) == 0 && (S_ISLNK(sb.st_mode)|| S_ISREG(sb.st_mode)))
         add_to_file_list(path->valuestring, sb.st_mode, filelist);
      
   }
   return 0;
}

// Recursive remove directory
void _rmdir(const char * dir, const char * base)
{
   char tmp[1024];
   if (!dir || strlen(dir)==0)
   {
      if (!base || strlen(base)==0) return;
      else snprintf((char*)tmp,1024,"%s",base);
   }
   else if (!base || strlen(base)==0)
   {
      if (!dir || strlen(dir)==0) return;
      else snprintf((char*)tmp,1024,"%s",dir);
   }
   else
  {
     if (dir[0]=='/')
     snprintf((char*)tmp,1024,"%s%s",base,dir);
     else
     snprintf((char*)tmp,1024,"%s/%s",base,dir);
  }

   struct stat sb;

   if (stat(tmp, &sb) == 0 && S_ISDIR(sb.st_mode))
   {
      if (nftw((char*)tmp, remove_it, 6 ,FTW_PHYS | FTW_DEPTH))
      {
          perror( tmp );
      }
   }
}

// mdir -p
void _mkdir(const char *dir, const char * base) {
   char tmp[1024];
   char *p = NULL;
   size_t len;
   if (!dir || strlen(dir)==0)
   {
      if (!base || strlen(base)==0) return;
      else snprintf((char*)tmp,1024,"%s",base);
   }
   else if (!base || strlen(base)==0)
   {
      if (!dir || strlen(dir)==0) return;
      else snprintf((char*)tmp,1024,"%s",dir);
   }
   else
  {
     if (dir[0]=='/')
     snprintf((char*)tmp,1024,"%s%s",base,dir);
     else
     snprintf((char*)tmp,1024,"%s/%s",base,dir);
  }
   struct stat sb;
   if (stat(tmp, &sb) == 0 && S_ISDIR(sb.st_mode))
   {
      //debug("directory %s exists\n",tmp);
   }
   else
   {
      len = strlen(tmp);
      if(tmp[len - 1] == '/')
         tmp[len - 1] = 0;
      //debug("mkdir %s\n",tmp);
      for(p = tmp + 1; *p; p++)
         if(*p == '/') {
            *p = 0;
            if (stat(tmp, &sb) == 0 && S_ISDIR(sb.st_mode))
            {
               //debug("sub directory %s exists\n",tmp);
            }
            else{
            //debug("mkdir sub %s\n",tmp);
            mkdir(tmp,S_IRWXG | S_IRWXU);
            }
            *p = '/';
         }
      //debug("mkdir sub %s\n",tmp);
      mkdir(tmp, S_IRWXG | S_IRWXU);
   }
}

/*

*/
