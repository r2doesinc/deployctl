//
//  bash_it.h
//  deployctl
//
//  Created by Danny Goossen on 6/8/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#ifndef __deployctl__bash_it__
#define __deployctl__bash_it__

#include "common.h"



// variable substitutions
int bash_subs_one_level(struct trace_Struct * trace,cJSON *env_vars, int level, int unpriviledged );
int bash_subs_specials(struct trace_Struct * trace,cJSON *env_vars, int unpriviledged);
int check_path(char * path , int allow_wildcard,int unpriviledged);

// sanitize deploy variables
int process_deploy_path( cJSON * env_vars,struct trace_Struct *trace, const char * name, const char * default_source, const char * default_target, int unpriviledged );
int process_deploy_href( cJSON * env_vars,struct trace_Struct *trace );

int post_process_deploy_href( cJSON * env_vars,struct trace_Struct *trace );
int post_process_deploy_path( cJSON * env_vars,struct trace_Struct *trace, const char * name , const char * default_source, const char * default_target,int unpriviledged);

int pre_project_path_slug_check(cJSON* env_vars, void * trace);
int verify_project_name_path_namespace(cJSON* env_vars, void * trace);
int verify_project_url(cJSON*job,cJSON*env_vars,void*trace);
#endif /* defined(__deployctl__bash_it__) */
