//
//  substitute_repo_script.c
//  deployctl
//
//  Created by Danny Goossen on 2/7/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#include "deployd.h"


char * substitute_repo_script(char * in, char * repo_name, char * repo_url, int PGP_repo, int PGP_pack)
{
   struct MemoryStruct output;
   init_dynamicbuf( &output);
   regex_t regex;
   int reti;
   size_t prev_end=0;
   char regex_str[]= "@@([a-z])@@";

   int regex_match_cnt=2+1; // ( 2 matches in above expression: from @@..@@ and inbtween the @@, and 1 position to store the -1 for end.)
   char * subject=in;

   regmatch_t   ovector[3];
   //regmatch_t  * ovector= calloc(sizeof(regoff_t)*2,regex_match_cnt);

   /* Compile regular expression */
   regcomp(&regex, regex_str, REG_EXTENDED);
   /* Execute regular expression in loop */

   for (;;)
   {
      reti = regexec(&regex, subject+prev_end, regex_match_cnt, ovector, 0);
      if (!reti)
      {
         //puts("Match");
         int i=0;
         while (ovector[i].rm_so!=-1 && i<5)
         {
            // debug("found at %lld to %lld\n", ovector[i].rm_so+prev_end,ovector[i].rm_eo+prev_end);
            i++;
         }
         if (ovector[1].rm_eo !=-1 && ovector[0].rm_so !=-1 && (ovector[1].rm_eo-ovector[1].rm_so) == 1)
         {
            Writedynamicbuf_n((void *)(subject+ prev_end), ovector[0].rm_so, &output);

            // since we use here only 1 char as named substring, we'll have that in ovector[2]
            char stub_code=subject[prev_end+ovector[1].rm_so];
            prev_end=prev_end+ovector[0].rm_eo;
            switch (stub_code) {
               case 'u':
               {
                  Writedynamicbuf(repo_url,&output);
                  break;
               }
               case 'n':
               {
                  Writedynamicbuf(repo_name,&output);
                  break;
               }
               case 'p':
               {
                  char tmp[20];
                  sprintf(tmp,"%d", PGP_repo);
                  Writedynamicbuf(tmp,&output);
                  break;
               }
               case 'q':
               {
                  char tmp[20];
                  sprintf(tmp,"%d", PGP_pack);
                  Writedynamicbuf(tmp,&output);
                  break;
               }
               default:
                  error("svg, regex unknown stub code %c @ pos : %d",subject[prev_end+ovector[1].rm_so],prev_end+ovector[1].rm_so);break;
            }
         }

      }
      else if (reti == REG_NOMATCH) {
         // copy rest from lastmatch end to end of subject
         Writedynamicbuf_n((void *)(subject+ prev_end), strlen(subject)-prev_end, &output);
         break;
      }
      else { prev_end=0;break; }
}

   /* Free memory allocated to the pattern buffer by regcomp() */
   regfree(&regex);

   if (prev_end)
   {
      debug("success parsing script, len: %d \n",strlen(output.memory));
      return output.memory;
   }
   else
   {
      if (output.memory) {free(output.memory);}
      return NULL;
   }

}
