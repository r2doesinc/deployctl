/*
 exec.h
 Created by Danny Goossen, Gioxa Ltd on 4/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

#ifndef __deployctl__exec__
#define __deployctl__exec__

#include "common.h"

int bash_it(char * output_buf,int buff_len, const char* valuestring ,cJSON * env_vars,int unpriviledged);

int cmd_exec(void * opaque);
int cmd_write(void * opaque);
int get_gid(gid_t * gid,const char * user);
int get_uid(uid_t * uid,const char * user);
int get_uid_gid_user(uidguid_t * uidguid,const char * user);
int exec_color(void * opaque);

#endif /* defined(__deployctl__exec__) */
